package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After as After

import org.stringtemplate.v4.compiler.STParser.ifstat_return
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.lang.Integer as Integer
import java.lang.Double as Double
import java.lang.Float as Float
import com.kms.katalon.core.testdata.DBData
import forceStop.throwError
import internal.GlobalVariable


public class Validation {
	@Keyword
	def kabisatyear () {
		def kabisatcount = findTestData("Validation/Kabisat Year").getRowNumbers()

		if (kabisatcount == 0) {
			println ('Kabisat Year Validation SUCCESS !')
		}else {
			println ('Kabisat Year Validation FAILED !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def orderstatus () {
		def orderstatuscount = findTestData("Validation/Order Status Validation").getRowNumbers()

		if (orderstatuscount == 0){
			println ('Order Status Validation SUCCESS !')
		}else {
			println ('Order Status Validation FAILED !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def approvalSkipSurvey () {

		def approval = WebUI.getText(findTestObject('Web/Acquisition/Create Policy/Order Summary/LBL_APPROVAL_LIST')) as String
		println (approval)

		def countskipsurvey =  findTestData("Validation/Approval Limit Manager Skip Survey").getRowNumbers()

		def x = 1 as Integer

		while (x <= countskipsurvey){
			def approver = findTestData("Validation/Approval Limit Manager Skip Survey").getValue(1,x) as String

			print (approver)

			if (approval.contains(approver)) {
				println ('Approver ' + x + ' true !')
			}else {
				println ('Approver 	false !')
				new throwError().FORCESTOP()
			}
			x = x + 1
		}
	}

	@Keyword
	def approvalNeedSurvey () {
		def approval = WebUI.getAttribute(findTestObject('Web/Acquisition/Create Policy/Order Summary/LBL_APPROVAL_LIST'), 'text')

		def countneedsurvey = findTestData("Validation/Approval Limit Manager Need Survey").getRowNumbers()

		def x = 1 as Integer

		while (x <= countneedsurvey){
			def approver = findTestData("Validation/Approval Limit Manager Need Survey").getValue(1,x)

			if (approval.contains(approver)) {
				println ('Approver ' + x + ' true !')
			}else {
				println ('Approver false !')
				new throwError().FORCESTOP()
			}
			x = x + 1
		}
	}

	@Keyword
	def checkVAXOrder () {
		def BeyondVA = findTestData("Validation/VANumber X MST_ORDER").getValue(2,1)
		def OrderVA = findTestData("Validation/VANumber X MST_ORDER").getValue(1,1)

		if (BeyondVA != OrderVA) {
			println ('VANumber Different !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def checkVAXPolicy () {
		def BeyondVA = findTestData("Validation/VANumber X MST_ORDER").getValue(2,1)
		def OrderVA = findTestData("Validation/VANumber X MST_ORDER").getValue(1,1)
		def PolicyVA = findTestData("Validation/VANumber X MST_ORDER").getValue(6,1)
		def PolicyNoOrder = findTestData("Validation/VANumber X MST_ORDER").getValue(4,1)
		def PolicyNoPolicy = findTestData("Validation/VANumber X MST_ORDER").getValue(5,1)

		if (BeyondVA != PolicyVA) {
			println ('BeyondVA != PolicyVA !')
			new throwError().FORCESTOP()
		}

		if (OrderVA != PolicyVA) {
			println ('OrderVA != PolicyVA !')
			new throwError().FORCESTOP()
		}

		if (PolicyNoOrder != PolicyNoPolicy) {
			println ('OrderVA != PolicyVA !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def checkUserApprovalSkip (user) {
		if (user != 'AUTO SYS') {
			println ('User != AUTO SYS !')
			new throwError().FORCESTOP()
		}else if (user == 'AUTO SYS') {
			println ('User is AUTO SYS !')
		}
	}

	@Keyword
	def checkUserApprovalNeed (user) {
		if (user == 'AUTO SYS') {
			println ('User == AUTO SYS !')
			new throwError().FORCESTOP()
		}else if (user != 'AUTO SYS') {
			println ('User not AUTO SYS !')
		}
	}

	@Keyword
	def currID () {
		def CurrId = findTestData("Validation/Curr_Id").getValue(1, 1)

		if (CurrId == '') {
			println ('Currency Id FAILED !')
			new throwError().FORCESTOP()
		}else
			println ('Currency Id SUCCESS !')
	}

	@Keyword
	def accinclude () {
		def brand = findTestData("Validation/Latest PolicyNo ACC Include").getValue(1, 1)

		if (brand != 'INCLUDE TSI') {
			println ('INCLUDE TSI FAILED !')
			new throwError().FORCESTOP()
		}else if (brand == 'INCLUDE TSI')
			println ('INCLUDE TSI SUCCESS !')
	}

	@Keyword
	def checkEndorsementPolicy (column,policyid,expected) {
		DBData dbTestData = findTestData("Validation/EndorsementPolicy")
		dbTestData.query = dbTestData.query.replace("*",column).replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def x = dbTestData.getValue(1, 1)

		if (x != expected) {
			println ('CHECK ENDORSEMENT POLICY FAILED !')
			new throwError().FORCESTOP()
		}else {
			println ('CHECK ENDORSEMENT POLICY SUCCESS !')
		}
	}

	@Keyword
	def checkEndorsementAddress (column,policyid,addresstype,expected) {
		DBData dbTestData = findTestData("Validation/EndorsementAddress")
		dbTestData.query = dbTestData.query.replace("*",column).replace("_POLICYID_",policyid).replace("_ADDRESSTYPE_",addresstype)
		dbTestData.fetchedData = dbTestData.fetchData()

		def x = dbTestData.getValue(1, 1)

		if (x != expected) {
			println ('CHECK ENDORSEMENT ADDRESS FAILED !')
			new throwError().FORCESTOP()
		}else {
			println ('CHECK ENDORSEMENT ADDRESS SUCCESS !')
		}
	}

	@Keyword
	def checkEndorsementObjectClause (policyid,expectednew,expectedold) {
		DBData dbTestData = findTestData("Validation/EndorsementObjectClause")
		dbTestData.query = dbTestData.query.replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def x = dbTestData.getValue(1, 1)
		def y = dbTestData.getValue(2, 1)

		if (x != expectednew) {
			println ('CHECK ENDORSEMENT OBJECT CLAUSE FAILED !')
			new throwError().FORCESTOP()
		}else {
			println ('CHECK ENDORSEMENT OBJECT CLAUSE SUCCESS !')
		}

		if (y != expectedold) {
			println ('CHECK ENDORSEMENT OBJECT CLAUSE FAILED !')
			new throwError().FORCESTOP()
		}else {
			println ('CHECK ENDORSEMENT OBJECT CLAUSE SUCCESS !')
		}
	}

	@Keyword
	def checkEndorsementPolicyClause (policyid,expected1,expected2) {
		DBData dbTestData = findTestData("Validation/EndorsementPolicyClause")
		dbTestData.query = dbTestData.query.replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def x = dbTestData.getValue(1, 1)
		def y = dbTestData.getValue(1, 2)

		if (x != expected1) {
			println ('CHECK ENDORSEMENT POLICY CLAUSE FAILED !')
			new throwError().FORCESTOP()
		}
		else if (y != expected2) {
			println ('CHECK ENDORSEMENT POLICY CLAUSE FAILED !')
			new throwError().FORCESTOP()
		}
		else {
			println ('CHECK ENDORSEMENT POLICY CLAUSE SUCCESS !')
		}
	}

	@Keyword
	def checkEndorsementObjectInfo (policyid,colorbpkb,chassis,engine,registration) {
		DBData dbTestData = findTestData("Validation/EndorsementObjectInfo")
		dbTestData.query = dbTestData.query.replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def color = dbTestData.getValue(1, 1)
		def chassisno = dbTestData.getValue(2, 1)
		def engineno = dbTestData.getValue(3, 1)
		def registrationno = dbTestData.getValue(4, 1)

		if (color != colorbpkb) {
			println ('CHECK ENDORSEMENT OBJECT INFO FAILED !')
			new throwError().FORCESTOP()
		}
		else if (registrationno != registration) {
			println ('CHECK ENDORSEMENT OBJECT INFO FAILED !')
			new throwError().FORCESTOP()
		}
		else {
			println ('CHECK ENDORSEMENT OBJECT CLAUSE SUCCESS !')
		}

		if (chassisno.contains(chassis)){
			println ('CHECK ENDORSEMENT OBJECT INFO SUCCESS !')
		}
		else {
			println ('CHECK ENDORSEMENT OBJECT INFO FAILED !')
			new throwError().FORCESTOP()
		}

		if (engineno.contains(engine)) {
			println ('CHECK ENDORSEMENT OBJECT INFO SUCCESS !')
		}
		else {
			println ('CHECK ENDORSEMENT OBJECT INFO FAILED !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def checkEndorsementContractNoClientNo (policyid,contract,client) {
		DBData dbTestData = findTestData("Validation/EndorsementObjectInfo")
		dbTestData.query = dbTestData.query.replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def contractno = dbTestData.getValue(5, 1)
		def clientno = dbTestData.getValue(6, 1)

		if (contractno != contract) {
			println ('CHECK ENDORSEMENT CONTRACTNO FAILED !')
			new throwError().FORCESTOP()
		}
		else if (clientno != client) {
			println ('CHECK ENDORSEMENT CLIENTNO FAILED !')
			new throwError().FORCESTOP()
		}
		else {
			println ('CHECK ENDORSEMENT CONTRACTNO CLIENTNO SUCCESS !')
		}
	}

	@Keyword
	def checkEndorsementNoCover (policyid, newname, newtype, deletedname, deletedtype) {
		DBData dbTestData = findTestData("Validation/EndorsementNoCover")
		dbTestData.query = dbTestData.query.replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def name1 = dbTestData.getValue(1, 1)
		def type1 = dbTestData.getValue(2, 1)

		def name2 = dbTestData.getValue(3, 1)
		def type2 = dbTestData.getValue(4, 1)

		if (name1 != newname) {
			println ('CHECK ENDORSEMENT NOCOVER FAILED !')
			new throwError().FORCESTOP()
		}
		if (type1 != newtype) {
			println ('CHECK ENDORSEMENT NOCOVER FAILED !')
			new throwError().FORCESTOP()
		}
		else println ('CHECK ENDORSEMENT NOCOVER SUCCESS !')

		if (name2 != deletedname) {
			println ('CHECK ENDORSEMENT NOCOVER FAILED !')
			new throwError().FORCESTOP()
		}
		if (type2 != deletedtype) {
			println ('CHECK ENDORSEMENT NOCOVER FAILED !')
			new throwError().FORCESTOP()
		}
		else println ('CHECK ENDORSEMENT NOCOVER SUCCESS !')
	}

	@Keyword
	def checkEndorsementOriginalDefect (policyid,newdefect,deleted) {
		DBData dbTestData = findTestData("Validation/EndorsementOriDefect")
		dbTestData.query = dbTestData.query.replace("_POLICYID_",policyid)
		dbTestData.fetchedData = dbTestData.fetchData()

		def newname = dbTestData.getValue(1, 1)
		def deletedname = dbTestData.getValue(2, 1)

		if (newname != newdefect) {
			println ('CHECK ENDORSEMENT ORIGINAL DEFECT FAILED !')
			new throwError().FORCESTOP()
		}
		else println ('CHECK ENDORSEMENT ORIGINAL DEFECT SUCCESS !')

		if (deletedname != deleted) {
			println ('CHECK ENDORSEMENT ORIGINAL DEFECT FAILED !')
			new throwError().FORCESTOP()
		}
		else println ('CHECK ENDORSEMENT ORIGINAL DEFECT SUCCESS !')
	}

	@Keyword
	def checkEndorsementInfo2 (policyendorse,info,text) {
		DBData dbTestData = findTestData("Validation/Endorsement Info2")
		dbTestData.query = dbTestData.query.replace("_POLICYNO_",policyendorse).replace("_INFO_",info)
		dbTestData.fetchedData = dbTestData.fetchData()

		def info2 = dbTestData.getValue(2, 1)
		println (info2)
		if (info2.contains(text)) {
			println ('CHECK ENDORSEMENT INFO SUCCESS !')
		}else {
			println ('CHECK ENDORSEMENT INFO FAILED !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def getPolicyNo () {
		GlobalVariable.chassis = findTestData("Cancellation/ChassisParam").getValue(1, 1)
		WebUI.delay(1)
		DBData dbTestData = findTestData("Cancellation/Policy for Cancel")
		dbTestData.query = dbTestData.query.replace("_CHASSIS_",GlobalVariable.chassis)
		dbTestData.fetchedData = dbTestData.fetchData()
		GlobalVariable.policyid = dbTestData.getValue(2, 1)
		def orderstatus = dbTestData.getValue(5, 1)
		println (GlobalVariable.policyid)
	}

	@Keyword
	def checkCancelPolicyStatus () {
		GlobalVariable.chassis = findTestData("Cancellation/ChassisParam").getValue(1, 1)
		WebUI.delay(1)
		DBData dbTestData = findTestData("Cancellation/Policy for Cancel")
		dbTestData.query = dbTestData.query.replace("_CHASSIS_",GlobalVariable.chassis)
		dbTestData.fetchedData = dbTestData.fetchData()
		def endorseno = dbTestData.getValue(3, 1)

		def status = dbTestData.getValue(4, 1)

		if (endorseno == '1') {
			println ('CHECK CANCEL POLICY ENDORSEMENT_NO INFO SUCCESS !')
		}else {
			println ('CHECK CANCEL POLICY ENDORSEMENT_NO INFO FAILED !')
			new throwError().FORCESTOP()
		}

		if (status == 'C') {
			println ('CHECK CANCEL POLICY STATUS INFO SUCCESS !')
		}else {
			println ('CHECK CANCEL POLICY STATUS INFO FAILED !')
			new throwError().FORCESTOP()
		}
	}

	@Keyword
	def checkKendaraan () {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement Kendaraan = driver.findElement(By.xpath("//*[@id='root']/div/div/div/div/div/div/div/fieldset[1]/*/div[1]/p/b[text()='KENDARAAN']/parent::p/parent::div/parent::div[1]/div[2]"))

		List<WebElement> listkendaraan = Kendaraan.findElements(By.tagName('div'))

		def countkendaraan = listkendaraan.size()
		println (countkendaraan)
		def x = countkendaraan
		while (x != 0) {
			WebUI.delay(1)

			DBData dbkendaraan = findTestData("Otosales/Coverage - Kendaraan")
			dbkendaraan.query = dbkendaraan.query.replace("_NOSURAT_",GlobalVariable.nosurat)
			dbkendaraan.fetchedData = dbkendaraan.fetchData()

			def tsikendaraan = dbkendaraan.getValue(2, x)

			WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/Object Pertanggungan/LBL_KENDARAAN_YEAR - descendingindex',[('descendingindex'): x]), tsikendaraan)

			println (tsikendaraan)

			WebUI.delay(1)

			x--
		}
	}

	@Keyword
	def checkAccessories () {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement Accessories = driver.findElement(By.xpath("//*[@id='root']/div/div/div/div/div/div/div/fieldset[1]/*/div[1]/p/b[text()='AKSESORIS NON STANDARD']/parent::p/parent::div/parent::div[1]/div[2]"))

		List<WebElement> listaccs = Accessories.findElements(By.tagName('div'))

		def countaccs = listaccs.size()
		println (countaccs)
		def x = countaccs
		while (x != 0) {
			WebUI.delay(1)

			DBData dbaccs = findTestData("Otosales/Coverage - ACC")
			dbaccs.query = dbaccs.query.replace("_NOSURAT_",GlobalVariable.nosurat)
			dbaccs.fetchedData = dbaccs.fetchData()

			def premiaccs = dbaccs.getValue(3, x)

			WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/Object Pertanggungan/LBL_ACC_YEAR - descendingindex',[('descendingindex'): x]), premiaccs)

			println (premiaccs)

			WebUI.delay(1)

			x--
		}
	}

	@Keyword
	def cekEPolicy (productcode,source) {
		DBData dbepolicy = findTestData("isNeedHardCopy")
		dbepolicy.query = dbepolicy.query.replace("_PRODUCTCODE_",productcode).replace("_SOURCE_", source)
		dbepolicy.fetchedData = dbepolicy.fetchData()
		def isNeed = dbepolicy.getValue(1, 1)
		if (isNeed == '0') {
			//kondisi jika 0
			GlobalVariable.isNeedHardCopy = '0'
		}else {
			//kondisi jika bukan 0
			GlobalVariable.isNeedHardCopy = '1'
		}
	}

	@Keyword
	def getOrderNo (){
		DBData dborderno = findTestData("OrderNo")
		dborderno.query = dborderno.query.replace("_CHASISNO_",GlobalVariable.chassis)
		dborderno.fetchedData = dborderno.fetchData()
		def orderno = dborderno.getValue(1,1)
		println (orderno)

		GlobalVariable.globalparamSTRING1 = orderno

		//def query = "UPDATE dbo.GlobalVariable_BTe SET GlobalVariable_2 ='"+orderno+"'"
		//new executeQuery.DemoMySql().execute(query)
	}

	@Keyword
	def checkOrderSummary () {
		def info = findTestData(GlobalVariable.vmparam).getValue(58, 1) as String
		def emailDeliverValue = WebUI.getText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/LBL_ITEM',[('item'):'Email Deliver To']))

		//def orderno= findTestData("GVBTE").getValue(1, 1) as String
		//def gv = 'SELECT GLOBALVARIABLE_2 FROM LITT.DBO.GLOBALVARIABLE_BTE'

		//def orderno = new executeQuery.DemoMySql().executeQuery(gv)


		def emailDeliverCustomer = 'EHU@BEYOND.ASURANSIASTRA.COM'
		def emailDeliverPH = 'EHU@BEYOND.ASURANSI.ASTRA.CO.ID'
		def emailDeliverBoth ='EHU@BEYOND.ASURANSIASTRA.COM; EHU@BEYOND.ASURANSI.ASTRA.CO.ID'
		def emailDeliverBSame ='EHU@BEYOND.ASURANSIASTRA.COM'

		def email_deliver
		def emaideliverCustomer = 'email_deliver:Customer Email Address'
		def emaildeliverPholder = 'email_deliver:Policy Holder Email Address'
		def emaildeliverBoth = 'email_deliver:Both'
		def emaildeliverBothSame = 'email_deliver:Both Same'

		def chassis = GlobalVariable.chassis
		println ('Chasis no:  '+chassis)
		println ('EmailDeliverValue '+emailDeliverValue)
		println ('emailDeliverCustomer '+emailDeliverCustomer)

		WebUI.delay(1)

		if (info.contains(emaideliverCustomer)){
			if (emailDeliverValue != emailDeliverCustomer) {
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL TIDAK SESUAI ! """+chassis+"""',
                                     'FAILED',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}else if (emailDeliverValue == emailDeliverCustomer) {
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL SESUAI ! """+chassis+"""',
                                     'SUCCESS',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}
		}else if (info.contains(emaildeliverPholder)) {
			if (emailDeliverValue != emailDeliverPH){
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL TIDAK SESUAI ! """+chassis+"""',
                                     'FAILED',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}else if (emailDeliverValue == emailDeliverPH){
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL SESUAI ! """+chassis+"""',
                                     'SUCCESS',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}
		}else if (info.contains(emaildeliverBoth)){
			if (emailDeliverValue != emailDeliverBoth){
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL TIDAK SESUAI ! """+chassis+"""',
                                     'FAILED',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}else if (emailDeliverValue == emailDeliverBoth){
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL SESUAI ! """+chassis+"""',
                                     'SUCCESS',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}
		}else if (info.contains(emaildeliverBothSame)){
			if (emailDeliverValue != emailDeliverBSame){
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL TIDAK SESUAI ! """+chassis+"""',
                                     'FAILED',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}else if (emailDeliverValue == emailDeliverBSame){
				def query ="""
                           INSERT INTO litt.dbo.ExecutionInfo
                                   ( Platform, Info, Status, Timestamp )
                           VALUES  ( 'Retail',
                                     'EMAIL SESUAI ! """+chassis+"""',
                                     'SUCCESS',
                                     GETDATE()
                                   )
                           """
				(new executeQuery.DemoMySql().execute(query))
			}
		}
	}
}
