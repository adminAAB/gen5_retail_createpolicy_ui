package executeQuery

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import internal.GlobalVariable
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.microsoft.sqlserver.jdbc.SQLServerConnection;

public class ReadExcel {

	@Keyword
	def String readExcel(){
		FileInputStream fis = null;
		String URL = "jdbc:sqlserver://172.16.94.48:1433;databaseName=LiTT";
		String USER = "sa";
		String PASSWORD = "Password95";

		SQLServerConnection conn;
		Statement stmt;

		String sql = null;
		String str1 = null, str2 = null, str3 = null, str4 = null, str5 = null, str6 = null, str7 = null;
		int int1;

		///cek ip
		def pathxlsx = ''

		new executeQuery.GetIP().IPLocal()

		if (GlobalVariable.vmip == '172.16.94.191') {
			pathxlsx = ('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch01\\Supporting Files\\INSERT TO SQL.xlsx')
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			pathxlsx = ('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch02\\Supporting Files\\INSERT TO SQL.xlsx')
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			pathxlsx = ('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch03\\Supporting Files\\INSERT TO SQL.xlsx')
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			pathxlsx = ('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch04\\Supporting Files\\INSERT TO SQL.xlsx')
		}else {
			pathxlsx = ('D:\\katalon\\A2ISRetail\\Supporting Files\\INSERT TO SQL.xlsx')
		}

		try {
			fis = new FileInputStream(new File(pathxlsx));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// create workbook instance
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// create a sheet object to retrieve the sheet
		XSSFSheet sheet = wb.getSheetAt(0);

		FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();

		try {
			conn = (SQLServerConnection) DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = conn.createStatement();

			while (!conn.isClosed()) {

				for (Row row : sheet) {
					if (row.getRowNum() >= 2) {
						sql = "insert into dbo.dataaab2000 (Tipe, Customer, ProductCode, PolicyPeriodFrom, PolicyPeriodTo, source, VehicleCode, Year, ChasisNo, Usage, Wilayah, COMPeriodFrom, COMPeriodTo, TLOPeriodFrom, TLOPeriodTo, ETVPeriodFrom, ETVPeriodTo, FLDPeriodFrom, FLDPeriodTo, PADPeriodFrom, PADPeriodTo, PADSI, PAPPeriodFrom, PAPPeriodTo, PAPSI, PAPRate, TPLPeriodFrom, TPLPeriodTo, TPLSI, isRun, SRCCPeriodFrom, SRCCPeriodTo, SRCCSI, TSPeriodFrom, TSPeriodTo, TSSI, Marketing, Term, TLOTSPeriodFrom, TLOTSPeriodTo, Acc1, Acc1Price, Acc1Qty, Acc1SI, Acc2, Acc2Price, Acc2Qty, Acc2SI, ACCTSI, Region, AREA, CustomerType, Segment, PolicyPeriod, SurveyType, Info) values \n(";

						for (Cell cell : row) {
							if (cell.getColumnIndex() <= 55) {
								//								System.out.println("cell.getCellType()="+cell.getCellType());
								def test=cell.getCellType()
								def test2=formulaEvaluator.evaluateInCell(cell).getCellType()
								switch (formulaEvaluator.evaluateInCell(cell).getCellType()) {

									//Handling cell type NUMERIC
									case 0:
										int1 = (int) cell.getNumericCellValue();
										str1 = Integer.toString(int1);
										sql = sql + "'" + str1 + "'";
										break;

									//Handling cell type STRING
									case 1:
										str1 = cell.getStringCellValue();
										sql = sql + "'" + str1 + "'";
										break;

									//Handling cell type BLANK
									case 3:
										str1 = cell.getStringCellValue();
										sql = sql + "'" + str1 + "'";
									default:
										break;
								}

								//Meniadakan koma di kolom SurveyType
								if (cell.getColumnIndex() <= 54) {
									sql = sql + ", ";
								}
							}

							//Insert NoCover1
							else if (cell.getColumnIndex() == 60)
								str2 = cell.getStringCellValue();

							//Insert NoCover2
							else if (cell.getColumnIndex() == 61)
								str3 = cell.getStringCellValue();

							//Insert Cacat1
							else if (cell.getColumnIndex() == 59)
								str4 = cell.getStringCellValue();

							//Insert Clause1
							else if (cell.getColumnIndex() == 56)
								str5 = cell.getStringCellValue();

							//Insert Clause2
							else if (cell.getColumnIndex() == 57)
								str6 = cell.getStringCellValue();

							//Insert Clause3
							else if (cell.getColumnIndex() == 58)
								str7 = cell.getStringCellValue();

						}

						sql = sql + ")\n";
						sql = sql + "insert into litt.dbo.nocover (dataid,nocoverdescription) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'" + str2 + "')\n";
						sql = sql + "insert into litt.dbo.nocover (dataid,nocoverdescription) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'" + str3 + "')\n";
						sql = sql + "insert into litt.dbo.cacatsemula (dataid,cacatdescription) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'" + str4 + "')\n";
						sql = sql + "insert into litt.dbo.clause (dataid,clause) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'" + str5 + "')\n";
						sql = sql + "insert into litt.dbo.clause (dataid,clause) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'" + str6 + "')\n";
						sql = sql + "insert into litt.dbo.clause (dataid,clause) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'" + str7 + "')\n";
						System.out.print(sql);
						stmt.execute(sql);

					}

				}

				stmt.close();
				conn.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
