package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import java.net.Inet4Address

import internal.GlobalVariable

public class GetIP {
	@Keyword
	def IPLocal () {
		Inet4Address address = Inet4Address.getLocalHost()
		println(1)
		GlobalVariable.vmip = address.getHostAddress()
		println(GlobalVariable.vmip)
		if (GlobalVariable.vmip == '172.16.94.191') {
			GlobalVariable.vmparam = 'NewPolicyParameters3'
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			GlobalVariable.vmparam = 'NewPolicyParameters4'
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			GlobalVariable.vmparam = 'NewPolicyParameters5'
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			GlobalVariable.vmparam = 'NewPolicyParameters6'
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			GlobalVariable.vmparam = 'NewPolicyParameters'
		}else {
			GlobalVariable.vmparam = 'NewPolicyParameters'
		}
	}

	@Keyword
	def Update () {
		def runningiddata
		def updatechasis
		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))


		if (GlobalVariable.vmip == '172.16.94.191') {
			runningiddata = 'RunningID3'
			def id = findTestData(runningiddata).getValue(1, 1)
			updatechasis = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = 'QCATALON3' WHERE ID = '"+id+"'")
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			runningiddata = 'RunningID4'
			def id = findTestData(runningiddata).getValue(1, 1)
			updatechasis = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = 'QCATALON4' WHERE ID = '"+id+"'")
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			runningiddata = 'RunningID5'
			def id = findTestData(runningiddata).getValue(1, 1)
			updatechasis = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = 'QCATALON5' WHERE ID = '"+id+"'")
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			runningiddata = 'RunningID6'
			def id = findTestData(runningiddata).getValue(1, 1)
			updatechasis = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = 'QCATALON6' WHERE ID = '"+id+"'")
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			runningiddata = 'RunningID'
			def id = findTestData(runningiddata).getValue(1, 1)
			updatechasis = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = 'BTE' WHERE ID = '"+id+"'")
		}else {
			runningiddata = 'RunningID'
			def id = findTestData(runningiddata).getValue(1, 1)
			updatechasis = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = 'ACW' WHERE ID = '"+id+"'")
		}

		(new executeQuery.DemoMySql().execute(updatechasis))
	}
}
