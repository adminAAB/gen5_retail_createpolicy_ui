package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class UpdateDataFiles {
	@Keyword
	def runUpdate (){

		new executeQuery.GetIP().IPLocal()

		def runningiddata
		def increaseidquery

		if (GlobalVariable.vmip == '172.16.94.191') {
			runningiddata = 'RunningID3'
			increaseidquery = ("UPDATE masterid SET number = (SELECT number FROM dbo.MasterID where id = 'QCATALON3')+1 WHERE id = 'QCATALON3'")
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			runningiddata = 'RunningID4'
			increaseidquery = ("UPDATE masterid SET number = (SELECT number FROM dbo.MasterID where id = 'QCATALON4')+1 WHERE id = 'QCATALON4'")
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			runningiddata = 'RunningID5'
			increaseidquery = ("UPDATE masterid SET number = (SELECT number FROM dbo.MasterID where id = 'QCATALON5')+1 WHERE id = 'QCATALON5'")
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			runningiddata = 'RunningID6'
			increaseidquery = ("UPDATE masterid SET number = (SELECT number FROM dbo.MasterID where id = 'QCATALON6')+1 WHERE id = 'QCATALON6'")
		}else {
			runningiddata = 'RunningID'
			increaseidquery = ("UPDATE masterid SET number = (SELECT number FROM dbo.MasterID where id = 'ACW')+1 WHERE id = 'ACW'")
		}

		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
		def id = findTestData(runningiddata).getValue(1, 1)
		def chasisnumbercek = (findTestData(GlobalVariable.vmparam).getValue(10, 1)) as String
		def chasisnumber = (findTestData(GlobalVariable.vmparam).getValue(1, 1)) as String
		def chasisno = ''
		def nsbte = 'NSBTE'
		WebUI.delay(1)

		if (chasisnumbercek.contains(nsbte)){
			chasisno = chasisnumbercek
		}else {
			chasisno = chasisnumber
		}
		WebUI.delay(1)
		def insertChassisNo = ("UPDATE LiTT.dbo.DataAAB2000 SET ChasisNo = '"+chasisno+"' WHERE ID = '"+id+"'")
		def UpdateisRun = ("UPDATE LiTT.dbo.DataAAB2000 SET isRun = '1' WHERE ID = '"+id+"'")

		WebUI.delay(1)
		def increaseMasterID = increaseidquery

		if (chasisnumbercek.contains(nsbte)){
			WebUI.delay(1)
			(new executeQuery.DemoMySql().execute(UpdateisRun))
			WebUI.delay(1)
		}
		else {
			WebUI.delay(1)
			(new executeQuery.DemoMySql().execute(increaseMasterID))
			WebUI.delay(1)
			(new executeQuery.DemoMySql().execute(insertChassisNo))
			WebUI.delay(1)
			(new executeQuery.DemoMySql().execute(UpdateisRun))
			WebUI.delay(1)
		}
	}

	@Keyword
	def increaseNeedSurveyID() {
		def query = ("UPDATE masterid SET number = (SELECT number FROM dbo.MasterID where id = 'NSBTE')+1 WHERE id = 'NSBTE'")

		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
		Mobile.delay(1)
		(new executeQuery.DemoMySql().execute(query))
		Mobile.delay(1)
	}
}
