package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class InsertDataPolicy {

	@Keyword
	def insertdata (query) {
		WebUI.delay(1)
		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
		WebUI.delay(1)
		(new executeQuery.DemoMySql().execute(query))
		WebUI.delay(1)
	}

	@Keyword
	def otosales () {
		WebUI.delay(1)

		def query1 = """"
		INSERT INTO litt.dbo.otosalesparam
		SELECT [ISRUN]
		      ,[PROSPECT_NAME]
		      ,[PROSPECT_PHONE1]
		      ,[PROSPECT_PHONE2]
		      ,[PROSPECT_EMAIL]
		      ,[DEALER]
		      ,[SALESMAN]
		      ,[VEHICLE_CODE]
		      ,[VEHICLE]
		      ,[USAGE]
		      ,[REGION]
		      ,[REG_NO]
		      ,[CHASSIS_NO]
		      ,[PRODUCT_TYPE]
		      ,[PRODUCT_CODE]
		      ,[BASIC_COVER]
		      ,[SUM_INSURED]
		      ,[PERIOD_FROM]
		      """
		def query2 = """"
			  ,[PERIOD_TO]
			  ,[PERIHAL]
		      ,[JENIS_PERTANGGUNGAN]
		      ,[KENDARAAN]
		      ,[PLAT_NO]
		      ,[TAHUN]
		      ,[PREMI_KENDARAAN]
		      ,[COM_TS_PREMI]
		      ,[TLO_TS_PREMI]
		      ,[SRCC_PREMI]
		      ,[FLOOD_PREMI]
		      ,[ETV_PREMI]
		      ,[ACC_PREMI]
		      ,[PAD_PREMI]
		      ,[PAP_PREMI]
		      ,[TPL_PREMI]
		      ,[TOTAL_PREMI]
		  FROM [LiTT].[dbo].[Otosalesparam]
		  WHERE CHASSIS_NO = 'OTOSBTE'
		  AND PRODUCT_CODE = 'GDN52'"""

		def query3 = query1+query2
		new executeQuery.DemoMySql().connectDB('172.16.94.48', 'LITT', 'sa', 'Password95')

		new executeQuery.DemoMySql().execute(query3)
	}
}
