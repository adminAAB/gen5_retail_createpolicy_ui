package executeQuery


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import forceStop.throwError
import internal.GlobalVariable

public class CheckVANumber {
	@Keyword
	def rowcountQC4 () {
		def countvaQC4 = findTestData("VANumberQC4").getRowNumbers()
		if (countvaQC4 > 0) {
			println ('QC4 FAILED VANumber')
			new throwError().FORCESTOP()
		}else {
			println ('QC4 PASS VANumber')
		}
	}

	@Keyword
	def rowcountQC6 () {
		def countvaQC6 = findTestData("VANumberQC6").getRowNumbers()
		if (countvaQC6 > 0) {
			println ('QC6 FAILED VANumber')
			new throwError().FORCESTOP()
		}else {
			println ('QC6 PASS VANumber')
		}
	}

	@Keyword
	def rowcountNS () {
		def countvaNS = findTestData("VANumberNS").getRowNumbers()
		if (countvaNS > 0) {
			println ('NeedSurvey FAILED VANumber')
			new throwError().FORCESTOP()
		}else {
			println ('NeedSurvey PASS VANumber')
		}
	}
}
