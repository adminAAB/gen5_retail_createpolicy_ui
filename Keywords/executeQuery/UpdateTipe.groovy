package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class UpdateTipe {
	@Keyword
	def exec (String Tipe){

		new executeQuery.GetIP().IPLocal()

		def updatetipequery

		if (GlobalVariable.vmip == '172.16.94.191') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET tipe = '"+ Tipe +"' WHERE UserAlias = 'QCATALON3'"
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET tipe = '"+ Tipe +"' WHERE UserAlias = 'QCATALON4'"
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET tipe = '"+ Tipe +"' WHERE UserAlias = 'QCATALON5'"
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET tipe = '"+ Tipe +"' WHERE UserAlias = 'QCATALON6'"
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET tipe = '"+ Tipe +"' WHERE UserAlias = 'BTE'"
		}else {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET tipe = '"+ Tipe +"' WHERE UserAlias = 'ACW'"
		}


		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))

		def updateTipe = updatetipequery

		(new executeQuery.DemoMySql().execute(updateTipe))
	}

	@Keyword
	def execsegment (String segment){

		new executeQuery.GetIP().IPLocal()

		def updatetipequery
		if (GlobalVariable.vmip == '172.16.94.191') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET segment = '"+ segment +"' WHERE UserAlias = 'QCATALON3'"
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET segment = '"+ segment +"' WHERE UserAlias = 'QCATALON4'"
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET segment = '"+ segment +"' WHERE UserAlias = 'QCATALON5'"
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET segment = '"+ segment +"' WHERE UserAlias = 'QCATALON6'"
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET segment = '"+ segment +"' WHERE UserAlias = 'BTE'"
		}else {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET segment = '"+ segment +"' WHERE UserAlias = 'ACW'"
		}

		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))

		def updateTipe = updatetipequery

		(new executeQuery.DemoMySql().execute(updateTipe))
	}

	@Keyword
	def execsurveytype (String surveytype){

		new executeQuery.GetIP().IPLocal()

		def updatetipequery
		if (GlobalVariable.vmip == '172.16.94.191') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET surveytype = '"+ surveytype +"' WHERE UserAlias = 'QCATALON3'"
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET surveytype = '"+ surveytype +"' WHERE UserAlias = 'QCATALON4'"
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET surveytype = '"+ surveytype +"' WHERE UserAlias = 'QCATALON5'"
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET surveytype = '"+ surveytype +"' WHERE UserAlias = 'QCATALON6'"
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET surveytype = '"+ surveytype +"' WHERE UserAlias = 'BTE'"
		}else {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET surveytype = '"+ surveytype +"' WHERE UserAlias = 'ACW'"
		}


		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))

		def updateTipe = updatetipequery

		(new executeQuery.DemoMySql().execute(updateTipe))
	}

	@Keyword
	def execpolicyperiod (String policyperiod){

		new executeQuery.GetIP().IPLocal()

		def updatetipequery
		if (GlobalVariable.vmip == '172.16.94.191') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET policyperiod = '"+ policyperiod +"' WHERE UserAlias = 'QCATALON3'"
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET policyperiod = '"+ policyperiod +"' WHERE UserAlias = 'QCATALON4'"
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET policyperiod = '"+ policyperiod +"' WHERE UserAlias = 'QCATALON5'"
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET policyperiod = '"+ policyperiod +"' WHERE UserAlias = 'QCATALON6'"
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET policyperiod = '"+ policyperiod +"' WHERE UserAlias = 'BTE'"
		}else {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET policyperiod = '"+ policyperiod +"' WHERE UserAlias = 'ACW'"
		}

		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))

		def updateTipe = updatetipequery

		(new executeQuery.DemoMySql().execute(updateTipe))
	}

	@Keyword
	def execcustomertype (String customertype){

		new executeQuery.GetIP().IPLocal()

		def updatetipequery
		if (GlobalVariable.vmip == '172.16.94.191') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET customertype = '"+ customertype +"' WHERE UserAlias = 'QCATALON3'"
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET customertype = '"+ customertype +"' WHERE UserAlias = 'QCATALON4'"
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET customertype = '"+ customertype +"' WHERE UserAlias = 'QCATALON5'"
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET customertype = '"+ customertype +"' WHERE UserAlias = 'QCATALON6'"
		}else if (GlobalVariable.vmip == '172.16.92.22') {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET customertype = '"+ customertype +"' WHERE UserAlias = 'BTE'"
		}else {
			updatetipequery = "UPDATE LiTT.dbo.gen5tipe SET customertype = '"+ customertype +"' WHERE UserAlias = 'ACW'"
		}

		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))

		def updateTipe = updatetipequery

		(new executeQuery.DemoMySql().execute(updateTipe))
	}
}
