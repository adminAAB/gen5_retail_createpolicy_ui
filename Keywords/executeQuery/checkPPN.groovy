package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.lang.Integer as Integer
import java.lang.Double as Double
import java.lang.Float as Float
import com.kms.katalon.core.testdata.DBData
import internal.GlobalVariable

public class checkPPN {
	@Keyword
	def DCM () {

		GlobalVariable.chassis = findTestData("Cancellation/ChassisParam").getValue(1, 1)
		def pct = Integer.parseInt(findTestData("Validation/PCT_PPN_COMMISSION").getValue(1, 1))
		WebUI.delay(200)
		DBData checkDCM = findTestData("Validation/Check PPN")
		checkDCM.query = checkDCM.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DCM').replace('99','0')
		checkDCM.fetchedData = checkDCM.fetchData()
		WebUI.delay(15)
		double zero = 0
		def countDCM = checkDCM.getRowNumbers()
		println(GlobalVariable.chassis)

		WebUI.delay(1)

		DBData checkDPR = findTestData("Validation/Check PPN")
		checkDPR.query = checkDPR.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DPR').replace('99','0')
		checkDPR.fetchedData = checkDPR.fetchData()
		WebUI.delay(15)
		def countDPR = checkDPR.getRowNumbers()

		if (countDCM == 1) {
			println ('DCM 1')
			def PKPNO = checkDCM.getValue(4, 1).trim()
			def PolicyNo = checkDCM.getValue(3, 1)
			println(PKPNO)
			println(PolicyNo)

			//DCM
			double AmountDCM = Double.parseDouble(checkDCM.getValue(7, 1))
			double OrgAmountDCM = Double.parseDouble(checkDCM.getValue(8, 1))
			double NotePolicyPPNAmountDCM = Double.parseDouble(checkDCM.getValue(9, 1))
			double NoteAmountDCM = Double.parseDouble(checkDCM.getValue(10, 1))
			double FinanceNotePPNAmountDCM = Double.parseDouble(checkDCM.getValue(11, 1))
			double CommissionAmountDCM = Double.parseDouble(checkDCM.getValue(12, 1))
			double NominalDCM = Double.parseDouble(checkDCM.getValue(13 , 1))
			double AmountNoPPNDCM = Double.parseDouble(checkDCM.getValue(14, 1))

			println (AmountNoPPNDCM)

			WebUI.delay(1)

			double PPNAMOUNT = AmountNoPPNDCM * pct / 100
			println (PPNAMOUNT)
			double NETTAMOUNT = AmountNoPPNDCM + PPNAMOUNT
			println (NETTAMOUNT)

			if (PKPNO != '') {

				if (AmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == -1*PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NominalDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Nominal PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Nominal FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}
			}
			else {
				if (AmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

			}
		}
		else if (countDCM == 2) {
			println ('DCM 2')
			def PKPNO = checkDCM.getValue(4, 1).trim()
			def PolicyNo = checkDCM.getValue(3, 1)
			println(PKPNO)
			println(PolicyNo)

			//DCM
			double AmountDCM = Double.parseDouble(checkDCM.getValue(7, 1))
			double OrgAmountDCM = Double.parseDouble(checkDCM.getValue(8, 1))
			double NotePolicyPPNAmountDCM = Double.parseDouble(checkDCM.getValue(9, 1))
			double NoteAmountDCM = Double.parseDouble(checkDCM.getValue(10, 1))
			double FinanceNotePPNAmountDCM = Double.parseDouble(checkDCM.getValue(11, 1))
			double CommissionAmountDCM = Double.parseDouble(checkDCM.getValue(12, 1))
			double NominalDCM = Double.parseDouble(checkDCM.getValue(13 , 1))
			double AmountNoPPNDCM = Double.parseDouble(checkDCM.getValue(14, 1))

			def PKPNO2 = checkDCM.getValue(4, 2).trim()
			def PolicyNo2 = checkDCM.getValue(3, 2)
			println(PKPNO2)
			println(PolicyNo2)

			//DCM
			double AmountDCM2 = Double.parseDouble(checkDCM.getValue(7, 2))
			double OrgAmountDCM2 = Double.parseDouble(checkDCM.getValue(8, 2))
			double NotePolicyPPNAmountDCM2 = Double.parseDouble(checkDCM.getValue(9, 2))
			double NoteAmountDCM2 = Double.parseDouble(checkDCM.getValue(10, 2))
			double FinanceNotePPNAmountDCM2 = Double.parseDouble(checkDCM.getValue(11, 2))
			double CommissionAmountDCM2 = Double.parseDouble(checkDCM.getValue(12, 2))
			double NominalDCM2 = Double.parseDouble(checkDCM.getValue(13 , 2))
			double AmountNoPPNDCM2 = Double.parseDouble(checkDCM.getValue(14, 2))

			println('AMOUNT DCM 1 ' + AmountDCM)
			println('AMOUNT DCM 2 ' + AmountDCM2)

			WebUI.delay(1)

			double PPNAMOUNT = AmountNoPPNDCM * pct / 100
			println ('PPN AMOUNT ' + PPNAMOUNT)
			double NETTAMOUNT = AmountNoPPNDCM + PPNAMOUNT
			println ('NETT AMOUNT ' + NETTAMOUNT)

			if (PKPNO != '') {
				///// ROW 1

				if (AmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == -1*PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NominalDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Nominal PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Nominal FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				///// ROW 2
				if (AmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == -1*PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}
				println ('NominalDCM CANCEL ' + NominalDCM)
				println ('NETTAMOUNT NYA ' + NETTAMOUNT)
				if (NominalDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel Nominal PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel Nominal FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

			}
			else {
				///// ROW 1
				if (AmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				///// ROW 2
				if (AmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM2 == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM2 == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}
			}
		}

		///// DPR
		if (countDPR == 1) {
			println ('DPR 1')
			DBData DATANOTA = findTestData("Validation/Check PPN")
			DATANOTA.query = DATANOTA.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DPR').replace('99','0')
			DATANOTA.fetchedData = DATANOTA.fetchData()

			def PKPNO = DATANOTA.getValue(4, 1).trim()
			def PolicyNo = DATANOTA.getValue(3, 1)

			println(PKPNO)
			println(PolicyNo)

			//DPR
			double AmountDPR = Double.parseDouble(DATANOTA.getValue(7, 1))
			double OrgAmountDPR = Double.parseDouble(DATANOTA.getValue(8, 1))
			double NotePolicyPPNAmountDPR = Double.parseDouble(DATANOTA.getValue(9, 1))
			double NoteAmountDPR = Double.parseDouble(DATANOTA.getValue(10, 1))
			double FinanceNotePPNAmountDPR = Double.parseDouble(DATANOTA.getValue(11, 1))
			double CommissionAmountDPR = Double.parseDouble(DATANOTA.getValue(12, 1))
			double NominalDPR = Double.parseDouble(DATANOTA.getValue(13 , 1))
			def AmountNoPPNDPR = DATANOTA.getValue(14, 1)

			if (NotePolicyPPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (FinanceNotePPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (AmountNoPPNDPR == '') {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}
		}else if (countDPR == 2) {
			println ('DPR 2')
			DBData DATANOTA = findTestData("Validation/Check PPN")
			DATANOTA.query = DATANOTA.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DPR').replace('99','0')
			DATANOTA.fetchedData = DATANOTA.fetchData()

			def PKPNO = DATANOTA.getValue(4, 1).trim()
			def PolicyNo = DATANOTA.getValue(3, 1)

			println(PKPNO)
			println(PolicyNo)

			//DPR
			double AmountDPR = Double.parseDouble(DATANOTA.getValue(7, 1))
			double OrgAmountDPR = Double.parseDouble(DATANOTA.getValue(8, 1))
			double NotePolicyPPNAmountDPR = Double.parseDouble(DATANOTA.getValue(9, 1))
			double NoteAmountDPR = Double.parseDouble(DATANOTA.getValue(10, 1))
			double FinanceNotePPNAmountDPR = Double.parseDouble(DATANOTA.getValue(11, 1))
			double CommissionAmountDPR = Double.parseDouble(DATANOTA.getValue(12, 1))
			double NominalDPR = Double.parseDouble(DATANOTA.getValue(13 , 1))
			def AmountNoPPNDPR = DATANOTA.getValue(14, 1)

			double AmountDPR2 = Double.parseDouble(DATANOTA.getValue(7, 2))
			double OrgAmountDPR2 = Double.parseDouble(DATANOTA.getValue(8, 2))
			double NotePolicyPPNAmountDPR2 = Double.parseDouble(DATANOTA.getValue(9, 2))
			double NoteAmountDPR2 = Double.parseDouble(DATANOTA.getValue(10, 2))
			double FinanceNotePPNAmountDPR2 = Double.parseDouble(DATANOTA.getValue(11, 2))
			double CommissionAmountDPR2 = Double.parseDouble(DATANOTA.getValue(12, 2))
			double NominalDPR2 = Double.parseDouble(DATANOTA.getValue(13 , 2))
			def AmountNoPPNDPR2 = DATANOTA.getValue(14, 2)

			///// ROW 1
			if (NotePolicyPPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (FinanceNotePPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (AmountNoPPNDPR == '') {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			///// ROW 2
			if (NotePolicyPPNAmountDPR2 == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (FinanceNotePPNAmountDPR2 == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (AmountNoPPNDPR2 == '') {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Cancel Amount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Cancel Amount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

		}
	}

	@Keyword
	def DCMendorse () {

		GlobalVariable.chassis = findTestData("Cancellation/ChassisParam").getValue(1, 1)
		def pct = Integer.parseInt(findTestData("Validation/PCT_PPN_COMMISSION").getValue(1, 1))
		WebUI.delay(200)
		DBData checkDCM = findTestData("Validation/Check PPN")
		checkDCM.query = checkDCM.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DCM').replace('99','1')
		checkDCM.fetchedData = checkDCM.fetchData()
		WebUI.delay(15)
		double zero = 0
		def countDCM = checkDCM.getRowNumbers()
		println(GlobalVariable.chassis)

		WebUI.delay(1)

		DBData checkDPR = findTestData("Validation/Check PPN")
		checkDPR.query = checkDPR.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DPR').replace('99','1')
		checkDPR.fetchedData = checkDPR.fetchData()
		WebUI.delay(15)
		def countDPR = checkDPR.getRowNumbers()

		if (countDCM == 1) {
			println ('DCM 1')
			def PKPNO = checkDCM.getValue(4, 1).trim()
			def PolicyNo = checkDCM.getValue(3, 1)
			println(PKPNO)
			println(PolicyNo)

			//DCM
			double AmountDCM = Double.parseDouble(checkDCM.getValue(7, 1))
			double OrgAmountDCM = Double.parseDouble(checkDCM.getValue(8, 1))
			double NotePolicyPPNAmountDCM = Double.parseDouble(checkDCM.getValue(9, 1))
			double NoteAmountDCM = Double.parseDouble(checkDCM.getValue(10, 1))
			double FinanceNotePPNAmountDCM = Double.parseDouble(checkDCM.getValue(11, 1))
			double CommissionAmountDCM = Double.parseDouble(checkDCM.getValue(12, 1))
			double NominalDCM = Double.parseDouble(checkDCM.getValue(13 , 1))
			double AmountNoPPNDCM = Double.parseDouble(checkDCM.getValue(14, 1))

			println (AmountNoPPNDCM)

			WebUI.delay(1)

			double PPNAMOUNT = -1*AmountNoPPNDCM * pct / 100
			println (PPNAMOUNT)
			double NETTAMOUNT = -1*AmountNoPPNDCM + PPNAMOUNT
			println (NETTAMOUNT)

			if (PKPNO != '') {

				if (AmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NominalDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Nominal PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Nominal FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}
			}
			else {
				if (AmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note DCM NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

			}
		}
		else if (countDCM == 2) {
			println ('DCM 2')
			def PKPNO = checkDCM.getValue(4, 1).trim()
			def PolicyNo = checkDCM.getValue(3, 1)
			println(PKPNO)
			println(PolicyNo)

			//DCM
			double AmountDCM = Double.parseDouble(checkDCM.getValue(7, 1))
			double OrgAmountDCM = Double.parseDouble(checkDCM.getValue(8, 1))
			double NotePolicyPPNAmountDCM = Double.parseDouble(checkDCM.getValue(9, 1))
			double NoteAmountDCM = Double.parseDouble(checkDCM.getValue(10, 1))
			double FinanceNotePPNAmountDCM = Double.parseDouble(checkDCM.getValue(11, 1))
			double CommissionAmountDCM = Double.parseDouble(checkDCM.getValue(12, 1))
			double NominalDCM = Double.parseDouble(checkDCM.getValue(13 , 1))
			double AmountNoPPNDCM = Double.parseDouble(checkDCM.getValue(14, 1))

			def PKPNO2 = checkDCM.getValue(4, 2).trim()
			def PolicyNo2 = checkDCM.getValue(3, 2)
			println(PKPNO2)
			println(PolicyNo2)

			//DCM
			double AmountDCM2 = Double.parseDouble(checkDCM.getValue(7, 2))
			double OrgAmountDCM2 = Double.parseDouble(checkDCM.getValue(8, 2))
			double NotePolicyPPNAmountDCM2 = Double.parseDouble(checkDCM.getValue(9, 2))
			double NoteAmountDCM2 = Double.parseDouble(checkDCM.getValue(10, 2))
			double FinanceNotePPNAmountDCM2 = Double.parseDouble(checkDCM.getValue(11, 2))
			double CommissionAmountDCM2 = Double.parseDouble(checkDCM.getValue(12, 2))
			double NominalDCM2 = Double.parseDouble(checkDCM.getValue(13 , 2))
			double AmountNoPPNDCM2 = Double.parseDouble(checkDCM.getValue(14, 2))

			println('AMOUNT DCM 1 ' + AmountDCM)
			println('AMOUNT DCM 2 ' + AmountDCM2)

			WebUI.delay(1)

			double PPNAMOUNT = AmountNoPPNDCM * pct / 100
			println ('PPN AMOUNT ' + PPNAMOUNT)
			double NETTAMOUNT = AmountNoPPNDCM + PPNAMOUNT
			println ('NETT AMOUNT ' + NETTAMOUNT)

			if (PKPNO != '') {
				///// ROW 1

				if (AmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == -1*PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NominalDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Nominal PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Nominal FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				///// ROW 2
				if (AmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == -1*PPNAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}
				println ('NominalDCM CANCEL ' + NominalDCM)
				println ('NETTAMOUNT NYA ' + NETTAMOUNT)
				if (NominalDCM == NETTAMOUNT) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel Nominal PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel Nominal FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

			}
			else {
				///// ROW 1
				if (AmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM == -1*NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				///// ROW 2
				if (AmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (OrgAmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel Org_Amount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NotePolicyPPNAmountDCM2 == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (NoteAmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Invoice_Note Cancel NoteAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (FinanceNotePPNAmountDCM2 == zero) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel PPNAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}

				if (CommissionAmountDCM2 == NominalDCM) {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel CommissionAmount PASSED !','OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}else {
					def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes DCM Cancel CommissionAmount FAILED !','NOT OK',GETDATE())"
					new executeQuery.InsertDataPolicy().insertdata(query)
				}
			}
		}

		///// DPR
		if (countDPR == 1) {
			println ('DPR 1')
			DBData DATANOTA = findTestData("Validation/Check PPN")
			DATANOTA.query = DATANOTA.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DPR').replace('99','1')
			DATANOTA.fetchedData = DATANOTA.fetchData()

			def PKPNO = DATANOTA.getValue(4, 1).trim()
			def PolicyNo = DATANOTA.getValue(3, 1)

			println(PKPNO)
			println(PolicyNo)

			//DPR
			double AmountDPR = Double.parseDouble(DATANOTA.getValue(7, 1))
			double OrgAmountDPR = Double.parseDouble(DATANOTA.getValue(8, 1))
			double NotePolicyPPNAmountDPR = Double.parseDouble(DATANOTA.getValue(9, 1))
			double NoteAmountDPR = Double.parseDouble(DATANOTA.getValue(10, 1))
			double FinanceNotePPNAmountDPR = Double.parseDouble(DATANOTA.getValue(11, 1))
			double CommissionAmountDPR = Double.parseDouble(DATANOTA.getValue(12, 1))
			double NominalDPR = Double.parseDouble(DATANOTA.getValue(13 , 1))
			def AmountNoPPNDPR = DATANOTA.getValue(14, 1)

			if (NotePolicyPPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (FinanceNotePPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (AmountNoPPNDPR == '') {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}
		}else if (countDPR == 2) {
			println ('DPR 2')
			DBData DATANOTA = findTestData("Validation/Check PPN")
			DATANOTA.query = DATANOTA.query.replace('_CHASSIS_',GlobalVariable.chassis).replace('_TYPE_','DPR').replace('99','1')
			DATANOTA.fetchedData = DATANOTA.fetchData()

			def PKPNO = DATANOTA.getValue(4, 1).trim()
			def PolicyNo = DATANOTA.getValue(3, 1)

			println(PKPNO)
			println(PolicyNo)

			//DPR
			double AmountDPR = Double.parseDouble(DATANOTA.getValue(7, 1))
			double OrgAmountDPR = Double.parseDouble(DATANOTA.getValue(8, 1))
			double NotePolicyPPNAmountDPR = Double.parseDouble(DATANOTA.getValue(9, 1))
			double NoteAmountDPR = Double.parseDouble(DATANOTA.getValue(10, 1))
			double FinanceNotePPNAmountDPR = Double.parseDouble(DATANOTA.getValue(11, 1))
			double CommissionAmountDPR = Double.parseDouble(DATANOTA.getValue(12, 1))
			double NominalDPR = Double.parseDouble(DATANOTA.getValue(13 , 1))
			def AmountNoPPNDPR = DATANOTA.getValue(14, 1)

			double AmountDPR2 = Double.parseDouble(DATANOTA.getValue(7, 2))
			double OrgAmountDPR2 = Double.parseDouble(DATANOTA.getValue(8, 2))
			double NotePolicyPPNAmountDPR2 = Double.parseDouble(DATANOTA.getValue(9, 2))
			double NoteAmountDPR2 = Double.parseDouble(DATANOTA.getValue(10, 2))
			double FinanceNotePPNAmountDPR2 = Double.parseDouble(DATANOTA.getValue(11, 2))
			double CommissionAmountDPR2 = Double.parseDouble(DATANOTA.getValue(12, 2))
			double NominalDPR2 = Double.parseDouble(DATANOTA.getValue(13 , 2))
			def AmountNoPPNDPR2 = DATANOTA.getValue(14, 2)

			///// ROW 1
			if (NotePolicyPPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (FinanceNotePPNAmountDPR == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (AmountNoPPNDPR == '') {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Amount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			///// ROW 2
			if (NotePolicyPPNAmountDPR2 == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Note_Policy Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (FinanceNotePPNAmountDPR2 == zero) {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Finance_Notes Cancel PPNAmount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

			if (AmountNoPPNDPR2 == '') {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Cancel Amount PASSED !','OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}else {
				def query = "INSERT INTO litt.dbo.StatusCheckPPN (Policy_No,Info,Status,Timestamp) VALUES ('"+PolicyNo+"','Commission Cancel Amount FAILED !','NOT OK',GETDATE())"
				new executeQuery.InsertDataPolicy().insertdata(query)
			}

		}
	}







}
