package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class UpdateDB {

	def policyno = findTestData("InquiryPolicy").getValue(1,1)
	@Keyword
	def insertInquiryResult (String ActPremi){
		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
		WebUI.delay(1)
		def updateResult = ("UPDATE litt.dbo.inquirypolicy SET Result = 'ActPremi:"+ActPremi+"' WHERE PolicyNo = '"+policyno+"'")
		WebUI.delay(1)
		(new executeQuery.DemoMySql().execute(updateResult))
	}

	@Keyword
	def insertSuccess (){
		(new executeQuery.DemoMySql().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
		WebUI.delay(1)
		def ResultSuccess = ("UPDATE litt.dbo.inquirypolicy SET Result = 'Success' WHERE PolicyNo = "+policyno+"")
		WebUI.delay(1)
		(new executeQuery.DemoMySql().execute(ResultSuccess))
	}
	
	
	@Keyword
	def inserttablefnn (key,value1) {
		def query = "INSERT INTO LITT.DBO.FITRI (KEY,VALUE1,TIMESTAMP) VALUES ('"+key+"','"+value1+"',GETDATE())"
		new executeQuery.DemoMySql().connectDB('172.16.94.48','LITT','sa','Password95')
		new executeQuery.DemoMySql().execute(query)
	}
	
}
