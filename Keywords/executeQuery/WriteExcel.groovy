package executeQuery

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.lang.String
import internal.GlobalVariable
import java.nio.charset.StandardCharsets
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collector
import java.util.Collection as Collectors

public class WriteExcel {

	@Keyword
	def tulisexcel (value) {

		FileInputStream file = new FileInputStream (new File("E:\\Katalon Studio\\A2ISRetail\\Supporting Files\\Testdata.xlsx"))
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);


		String Data_fromCell=sheet.getRow(0).getCell(0).getStringCellValue();

		sheet.getRow(0).createCell(0).setCellValue(value);

		file.close();
		FileOutputStream outFile = new FileOutputStream(new File("E:\\Katalon Studio\\A2ISRetail\\Supporting Files\\Testdata.xlsx"));
		workbook.write(outFile);
		outFile.close();
	}

	@Keyword
	def tuliscsv (){

		List<String> records = new ArrayList<>();

		BufferedReader br = new BufferedReader(new FileReader("E:\\Katalon Studio\\A2ISRetail\\Supporting Files\\Testdata.csv"))
		String line;
		int index = 1;
		String data = '';
		while ((line = br.readLine()) != null) {
			if(index > 1) {
				def csvid = line.split(",")[0];
				def indexfile = csvid.split("-")[1] as Integer
				def nambah = 'bte-' + (indexfile+1) as String
				data += (line.replace(csvid,nambah)) + '\n'
			}
			else {
				//records.add(line);
				data += line + '\n'
			}
			index ++;
		}

		FileWriter writer = new FileWriter("E:\\Katalon Studio\\A2ISRetail\\Supporting Files\\Testdata2.csv");
		writer.write(data);
		writer.close();
	}
}
