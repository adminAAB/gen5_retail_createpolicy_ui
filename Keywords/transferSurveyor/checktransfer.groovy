package transferSurveyor

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import groovy.time.TimeCategory as TimeCategory

public class checktransfer {
	@Keyword
	def block1bte1coor (){
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '16/11/2018' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		///// CHECK DB ROW 1
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def block2bte1coor () {
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '16/11/2018' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		///// CHECK DB ROW 1
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != active) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def SurveyorXSurveyor1R2C () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		///// CHECK DB ROW 1
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'ADM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		//CoorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		//RegSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		//Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		//Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'ADM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		//CoorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		//RegSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		//Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		//Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		//CoorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		//RegSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		//Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		//Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 4
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		//CoorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		//RegSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		//Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		//Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}

	}

	@Keyword
	def block3bte1coor () {
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '16/11/2018' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]
		///// CHECK DB ROW 1
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 4
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def block4bte1coor () {

	}

	@Keyword
	def block1bte (){
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '16/11/2018' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		///// CHECK DB ROW 1
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000036') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000036') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AID') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000036') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 4
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'APG') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000036') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 5
		if ((findTestData('TransferHistory').getValue(2, 5)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000036') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 6
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != 'C000036') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(9, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(12, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != active) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(15, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(18, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def block2bte12 (){
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '16/11/2018' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		///// CHECK DB ROW 1
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 4
		///// TansferHistory
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 5
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 5)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 6
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 7
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 7)) != 'AEF') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 7)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 7)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 7)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 7)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 7)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 7)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 7)) != active) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 8
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 8)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 8)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 8)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 8)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 8)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 8)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 8)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 8)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 9
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 9)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 9)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 9)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 9)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 9)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 9)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 9)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 9)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 10
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 10)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 10)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 10)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 10)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 10)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 10)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 10)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 10)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 11
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 11)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 11)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 11)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 11)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 11)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 11)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 11)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 11)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 12
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 12)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 12)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 12)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

	}

	@Keyword
	def block1bte12 () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '16/11/2018' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		///// CHECK DB ROW 1
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 2
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 3
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AED') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 4
		///// TansferHistory
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 5
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 5)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		///// CHECK DB ROW 6
		///// TransferHistory
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000048') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000037') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != active) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def CoordinatorxRegional1R1C () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def SurveyorxCoordinator1R1C () {

		'ROW 1'
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 3'
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def SurveyorxRegional1R1C () {

		'ROW 1'
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'WHY') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
	}


	@Keyword
	def CoorxCoor1R2C () {

		'ROW 1'
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 3'
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'ABR') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 4'
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'ABR') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def SurveyorxCoor1R2C () {

		'ROW 1'
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'ADM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 3'
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000052') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def CoorxCoor2R () {

		'ROW 1'
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]
		'ROW 1'
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 3'
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 4'
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 5'
		if ((findTestData('TransferHistory').getValue(2, 5)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 6'
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 7'
		if ((findTestData('TransferHistory').getValue(2, 7)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 7)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 7)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 7)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 7)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 7)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 7)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 7)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 8'
		if ((findTestData('TransferHistory').getValue(2, 8)) != 'AEK') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 8)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 8)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 8)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 8)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 8)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 8)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 8)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 8)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def CoorxSurveyor2R () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]
		'ROW 1'
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'AAJ') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 3'
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'ADB') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		'ROW 4'
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'ADB') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000057') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000053') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def RegCoorToCoor1R () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]
		'ROW 1'
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'SKV') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != active) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def RegCoorXCrossCoor1R2C () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]
		'ROW 1'
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 3'
		if ((findTestData('TransferHistory').getValue(2, 3)) != 'YSE') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 4'
		if ((findTestData('TransferHistory').getValue(2, 4)) != 'YSE') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 5'
		if ((findTestData('TransferHistory').getValue(2, 5)) != 'YSE') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 6'
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'YSE') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != active) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def RegCoorToInactiveReg1R () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]
		'ROW 1'
		if ((findTestData('TransferHistory').getValue(2, 1)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'
		if ((findTestData('TransferHistory').getValue(2, 2)) != 'SKV') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != 'C000087') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != active) {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def SurveyorInactive () {

		'ROW 1'
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'AAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000056') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != 'C000051') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != '') {
			println('CHECK DB FAILED !')
		}
	}

	@Keyword
	def RegCoorxRegCoor () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		'ROW 1'

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'

		if ((findTestData('TransferHistory').getValue(2, 2)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 3'

		if ((findTestData('TransferHistory').getValue(2, 3)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 4'

		if ((findTestData('TransferHistory').getValue(2, 4)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 5'

		if ((findTestData('TransferHistory').getValue(2, 5)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 6'
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 7'
		if ((findTestData('TransferHistory').getValue(2, 7)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 7)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 7)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 7)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 7)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 7)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 7)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 7)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 7)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 8'
		if ((findTestData('TransferHistory').getValue(2, 8)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 8)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 8)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 8)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 8)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 8)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 8)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 8)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 8)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 9'
		if ((findTestData('TransferHistory').getValue(2, 9)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 9)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 9)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 9)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 9)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 9)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 9)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 10'
		if ((findTestData('TransferHistory').getValue(2, 10)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 10)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 10)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 10)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 10)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 10)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 10)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 11'
		if ((findTestData('TransferHistory').getValue(2, 11)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 11)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 11)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 11)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 11)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 11)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 11)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 12'
		if ((findTestData('TransferHistory').getValue(2, 12)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 12)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 12)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 12)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 12)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 13'
		if ((findTestData('TransferHistory').getValue(2, 13)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 13)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 13)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 13)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 13)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 13)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 13)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 13)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 13)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 13)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 13)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 13)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 13)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 13)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 14'
		if ((findTestData('TransferHistory').getValue(2, 14)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 14)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 14)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 14)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 14)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 14)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 14)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 14)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 14)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 14)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 14)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 14)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 14)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 14)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 15'
		if ((findTestData('TransferHistory').getValue(2, 15)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 15)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 15)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 15)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 15)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 15)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 15)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 15)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 15)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 15)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 15)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 15)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 15)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 15)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 16'
		if ((findTestData('TransferHistory').getValue(2, 16)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 16)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 16)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 16)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 16)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 16)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 16)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 16)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 16)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 16)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 16)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 16)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 16)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 16)) != active) {
			println('CHECK DB FAILED !')
		}
	}
	
	@Keyword
	def RegCoorxRegCoorNonSwitch () {

		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '05/09/2019' as String

		String todayconvert = today.format('dd/MM/yyyy')

		String tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = tomorrowconvert.split('/')[0]

		'ROW 1'

		if ((findTestData('TransferHistory').getValue(2, 1)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 1)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 1)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 1)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 1)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 1)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 1)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 2'

		if ((findTestData('TransferHistory').getValue(2, 2)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 2)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 2)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 2)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 2)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 2)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 2)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 2)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 3'

		if ((findTestData('TransferHistory').getValue(2, 3)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 3)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 3)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 3)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 3)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 3)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 3)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 3)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 4'

		if ((findTestData('TransferHistory').getValue(2, 4)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 4)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 4)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 4)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 4)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 4)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 4)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 4)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 5'

		if ((findTestData('TransferHistory').getValue(2, 5)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 5)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 5)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 5)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 5)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 5)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 5)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 5)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 5)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 5)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 6'
		if ((findTestData('TransferHistory').getValue(2, 6)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 6)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 6)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 6)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 6)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 6)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 6)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 6)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 6)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 6)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 7'
		if ((findTestData('TransferHistory').getValue(2, 7)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 7)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 7)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 7)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 7)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 7)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 7)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 7)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 7)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 7)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 7)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 8'
		if ((findTestData('TransferHistory').getValue(2, 8)) != 'YAM') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 8)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 8)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 8)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 8)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 8)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 8)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 8)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 8)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 8)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 8)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 9'
		if ((findTestData('TransferHistory').getValue(2, 9)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 9)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 9)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 9)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 9)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 9)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 9)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 9)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 9)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 10'
		if ((findTestData('TransferHistory').getValue(2, 10)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 10)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 10)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 10)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 10)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 10)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 10)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 10)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 10)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 11'
		if ((findTestData('TransferHistory').getValue(2, 11)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 11)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 11)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 11)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 11)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 11)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 11)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 11)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 11)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 12'
		if ((findTestData('TransferHistory').getValue(2, 12)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 12)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 12)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 12)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 12)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 12)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 12)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 12)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 13'
		if ((findTestData('TransferHistory').getValue(2, 13)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 13)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 13)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 13)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 13)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 13)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 13)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 13)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 13)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 13)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 13)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 13)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 13)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 13)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 14'
		if ((findTestData('TransferHistory').getValue(2, 14)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 14)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 14)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 14)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 14)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 14)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 14)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 14)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 14)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 14)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 14)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 14)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 14)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 14)) != active) {
			println('CHECK DB FAILED !')
		}

		'ROW 15'
		if ((findTestData('TransferHistory').getValue(2, 15)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 15)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 15)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 15)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 15)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 15)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 15)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 15)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 15)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 15)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 15)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 15)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 15)) != created) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 15)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		'ROW 16'
		if ((findTestData('TransferHistory').getValue(2, 16)) != 'HEH') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(3, 16)) != 'SR000063') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(4, 16)) != 'C000093') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(5, 16)) != 'SR000060') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(6, 16)) != 'C000086') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(7, 16)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}
		///// CoordinatorSurveyor
		if ((findTestData('TransferHistory').getValue(9, 16)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(10, 16)) != '') {
			println('CHECK DB FAILED !')
		}
		///// RegionalSurveyor
		if ((findTestData('TransferHistory').getValue(12, 16)) != '') {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(13, 16)) != '') {
			println('CHECK DB FAILED !')
		}
		///// Coordinator
		if ((findTestData('TransferHistory').getValue(15, 16)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(16, 16)) != active) {
			println('CHECK DB FAILED !')
		}
		///// Regional
		if ((findTestData('TransferHistory').getValue(18, 16)) != tomorrowconvert) {
			println('CHECK DB FAILED !')
		}

		if ((findTestData('TransferHistory').getValue(19, 16)) != active) {
			println('CHECK DB FAILED !')
		}

	}


}
