package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.google.common.util.concurrent.AbstractFuture.Cancellation
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class CancellationType {

	@Keyword
	def CType (){

		def productcode = findTestData("Cancellation/Policy for Cancel").getValue(6, 1)
		def GPN20 = 'GPN20'
		def BCLN3 = 'BCLN3'
		def GDN52 = 'GDN52'
		def GAN52 = 'GAN52'
		def GWN50 = 'GWN50'
		def GDU6S = 'GDU6S'
		def GAU6S = 'GAU6S'
		def TDN51 = 'TDN51'


		if (productcode == GPN20) {

			def canceltype = 'Customer Request'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_CANCEL_TYPE'))

			WebUI.delay (2)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE',[('canceltype'):'Customer Request']))
		}else if (productcode == GDN52){
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_CANCEL_TYPE'))

			def canceltype = 'Internal Request'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE',[('canceltype'):'Internal Request']))
		}else if (productcode == GAN52){

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_CANCEL_TYPE'))

			def canceltype = 'Intermediary Request'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE',[('canceltype'):'Intermediary Request']))
		}else if (productcode == GWN50){

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_CANCEL_TYPE'))

			def canceltype = 'Customer Request'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE',[('canceltype'):'Customer Request']))
		}else if (productcode == GDU6S){

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_CANCEL_TYPE'))

			def canceltype = 'Internal Request'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE',[('canceltype'):'Internal Request']))
		}else if (productcode == BCLN3){
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_CANCEL_TYPE'))

			def canceltype = 'Internal Request'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE',[('canceltype'):'Internal Request']))
		}
	}
}
