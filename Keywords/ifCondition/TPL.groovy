package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class TPL {
	@Keyword
	def InterestTPL () {
		def tipe = findTestData(GlobalVariable.vmparam).getValue(2, 1) as String
		def com = 'COM'
		def tpl = 'TPL'
		def lengkap = 'LENGKAP'
		def tplsi = findTestData(GlobalVariable.vmparam).getValue(30, 1)
		def productcode = (findTestData(GlobalVariable.vmparam).getValue(4, 1)).toUpperCase() as String

		if (tipe.contains(com)){
			if (tipe.contains(tpl)){
				WebUI.delay(5)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_TPL'))
				WebUI.delay(2)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))
				WebUI.delay(15)

				if (productcode == 'GNAB0') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'GFBN3') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'FLU10') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'GFBU3') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'GDHD1') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'FLU13') {
					WebUI.delay(3)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'FLU12') {
					WebUI.delay(3)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'FLN12') {
					WebUI.delay(3)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'ELU10') {
					WebUI.delay(3)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'ELN10') {
					WebUI.delay(3)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'GFBN1A01') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'GFBU1A01') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'OTGON') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'OTGOU') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}else if (productcode == 'OTGU1') {
					WebUI.delay(3)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TXT_TPL_SI'), tplsi)
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}
				else {
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/EXP_COVERAGE'))
					WebUI.delay(2)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/LST_TPL',[('tplsi'):tplsi]))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/TPL Detail/BTN_SAVE'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_CALCULATE'))
					WebUI.delay(5)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/TPL/BTN_SAVE'))
					WebUI.delay(3)
				}

				WebUI.delay(1)
			}
		}
	}
}
