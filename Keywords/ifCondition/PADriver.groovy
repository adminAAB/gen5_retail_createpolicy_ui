package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class PADriver {
	@Keyword
	def InterestPAD (){
		def tipe = findTestData(GlobalVariable.vmparam).getValue(2, 1) as String
		def pad = 'PAD'
		def lengkap = 'LENGKAP'

		if (tipe.contains(pad)){
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_PA_DRIVER'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))
			WebUI.delay(1)
			WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAD/TXT_PA_DRIVER'), findTestData(GlobalVariable.vmparam).getValue(23, 1))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAD/BTN_ADD'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAD/PAD Detail/BTN_SAVE'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAD/BTN_CALCULATE'))
			WebUI.delay(5)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAD/BTN_SAVE'))
			WebUI.delay(1)
		}
	}
}
