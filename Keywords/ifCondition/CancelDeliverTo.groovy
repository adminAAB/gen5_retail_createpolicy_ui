package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class CancelDeliverTo {

	@Keyword
	def deliverto () {

		def deliverto = findTestData("Cancellation/Policy for Cancel").getValue(6, 1)

		def GPN20 = 'GPN20'
		def BCLN3 = 'BCLN3'
		def GDN52 = 'GDN52'
		def GAN52 = 'GAN52'
		def GWN50 = 'GWN50'
		def GDU6S = 'GDU6S'
		def GAU6S = 'GAU6S'
		def TDN51 = 'TDN51'

		if (deliverto == TDN51) {

			WebUI.delay(1)

			def delivtype = 'Branch'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_DELIVER_TO'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/EXP_DELIVER_TO',[('delivtype'):'Branch']))
		}else if (deliverto == GDN52){

			def delivtype = 'Customer'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_DELIVER_TO'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/EXP_DELIVER_TO',[('delivtype'):'Customer']))
		}else if(deliverto == GPN20){

			def delivtype = 'Policy Holder'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_DELIVER_TO'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/EXP_DELIVER_TO',[('delivtype'):'Policy Holder']))
		}else if(deliverto == BCLN3){

			def delivtype = 'Branch'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_DELIVER_TO'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/EXP_DELIVER_TO',[('delivtype'):'Branch']))
		}
	}
}
