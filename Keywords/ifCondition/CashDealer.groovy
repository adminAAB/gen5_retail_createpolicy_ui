package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class CashDealer {
	@Keyword
	def variant () {
		def segment = findTestData(GlobalVariable.vmparam).getValue(55, 1).toUpperCase()
		def marketing = findTestData(GlobalVariable.vmparam).getValue(38, 1).toUpperCase()
		def CHECKPPN = findTestData(GlobalVariable.vmparam).getValue(58, 1).toUpperCase()

		if (CHECKPPN == 'PPN'){
			if (segment == 'CASHDEALER' && marketing == 'DOC') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_DEALER'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), '10195260')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

				WebUI.delay(2)
			}else if (segment == 'TITAN' && marketing == 'DOC') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_ID_AGENT'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), 'ADVI0002')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

				WebUI.delay(2)
			}else if (segment == 'TITAN' && marketing == 'SESP1') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_ID_AGENT'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/TXT_SEARCH_INPUT'), 'ADVI0002')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SELECT'))

				WebUI.delay(3)
			}
		}
		else {
			if (segment == 'CASHDEALER' && marketing == 'ESP1') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_ID_AGENT'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/TXT_SEARCH_INPUT'), '10195260')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SELECT'))

				WebUI.delay(3)
			}else if (segment == 'CASHDEALER' && marketing == 'SESP1') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_ID_AGENT'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/TXT_SEARCH_INPUT'), 'yudi0819')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SELECT'))

				WebUI.delay(3)
			}else if (segment == 'CASHDEALER' && marketing == 'DOC') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_DEALER'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), '10195260')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

				WebUI.delay(2)
			}else if (segment == 'CASHDEALER' && marketing == 'SRS01') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_DEALER'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), '10195260')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

				WebUI.delay(2)
			}else if (segment == 'CASHDEALER' && marketing == 'YUI03') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_DEALER'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), 'JAJA3686')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

				WebUI.delay(2)
			}else if (segment == 'CASHDEALER' && marketing == 'YHR01') {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_DEALER'))

				WebUI.delay(2)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), '10195260')

				WebUI.delay(2)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

				WebUI.delay(7)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

				WebUI.delay(2)
			}
		}
	}
}
