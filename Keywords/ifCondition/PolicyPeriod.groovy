package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.time.TimeCategory
import internal.GlobalVariable

public class PolicyPeriod {
	@Keyword
	def Policy (){

		def policyperiod = findTestData(GlobalVariable.vmparam).getValue(56, 1).toUpperCase()
		def y = 'Y'
		def m = 'M'

		if (policyperiod.contains(y)) {
			GlobalVariable.key1 = policyperiod.split('Y')[0]
			println (GlobalVariable.key1)
		}

		if (policyperiod.contains(m)) {
			GlobalVariable.key2 = policyperiod.split('M')[0]
			println (GlobalVariable.key2)
		}
	}

	@Keyword
	def PolicyPeriodFrom () {

		def periodfrom = findTestData(GlobalVariable.vmparam).getValue(5, 1) as String

		GlobalVariable.policyperiodfrom = periodfrom

		////////////////////////////// PolicyPeriodFrom - START
		String PolicyPeriodFrom = periodfrom

		String cmonthPPF

		String cdayPPF

		def dayPPF = PolicyPeriodFrom.split('/')[0]

		def monthPPF = PolicyPeriodFrom.split('/')[1]

		def yearPPF = PolicyPeriodFrom.split('/')[2]

		//convert month
		if (monthPPF == '01') {
			cmonthPPF = 'Jan'
		} else if (monthPPF == '02') {
			cmonthPPF = 'Feb'
		} else if (monthPPF == '03') {
			cmonthPPF = 'Mar'
		} else if (monthPPF == '04') {
			cmonthPPF = 'Apr'
		} else if (monthPPF == '05') {
			cmonthPPF = 'May'
		} else if (monthPPF == '06') {
			cmonthPPF = 'Jun'
		} else if (monthPPF == '07') {
			cmonthPPF = 'Jul'
		} else if (monthPPF == '08') {
			cmonthPPF = 'Aug'
		} else if (monthPPF == '09') {
			cmonthPPF = 'Sep'
		} else if (monthPPF == '10') {
			cmonthPPF = 'Oct'
		} else if (monthPPF == '11') {
			cmonthPPF = 'Nov'
		} else if (monthPPF == '12') {
			cmonthPPF = 'Dec'
		} else {
			println('Month not found')
		}

		//convert day
		if (dayPPF == '01') {
			cdayPPF = '1'
		} else if (dayPPF == '02') {
			cdayPPF = '2'
		} else if (dayPPF == '03') {
			cdayPPF = '3'
		} else if (dayPPF == '04') {
			cdayPPF = '4'
		} else if (dayPPF == '05') {
			cdayPPF = '5'
		} else if (dayPPF == '06') {
			cdayPPF = '6'
		} else if (dayPPF == '07') {
			cdayPPF = '7'
		} else if (dayPPF == '08') {
			cdayPPF = '8'
		} else if (dayPPF == '09') {
			cdayPPF = '9'
		} else {
			cdayPPF = dayPPF
		}

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPF_CAL_1'))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPF_CAL_2'))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPF_CHOOSE_YEAR', [('year') : yearPPF]))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPF_CHOOSE_MONTH', [('month') : cmonthPPF]))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPF_CHOOSE_DAY', [('day') : cdayPPF]))

		WebUI.delay(1)

		////////////////////////////// PolicyPeriodFrom - END
	}

	@Keyword
	def PolicyPeriodToAUTO () {

		def periodto = findTestData(GlobalVariable.vmparam).getValue(6, 1) as String

		def policyperiod = findTestData(GlobalVariable.vmparam).getValue(56, 1).toUpperCase()

		def y = 'Y'

		def m = 'M'

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CAL_1'))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CAL_2'))

		////////////////////////////// PolicyPeriodTo - START
		String PolicyPeriodTo = ''
		def gvyear = GlobalVariable.key1 as Integer
		def gvmonth = GlobalVariable.key2 as Integer

		if (policyperiod.contains(y) && policyperiod.contains(m)) {
			use (TimeCategory){
				def getdate = new Date()
				def getdate1m = getdate + gvmonth.month + gvyear.year
				PolicyPeriodTo = getdate1m.format('dd/MM/yyyy') as String
			}
		} else if (policyperiod.contains(y) && !(policyperiod.contains(m))) {
			use (TimeCategory){
				def getdate = new Date()
				def getdate1y = getdate + gvyear.year
				PolicyPeriodTo = getdate1y.format('dd/MM/yyyy') as String
			}
		} else if (policyperiod.contains(m) && !(policyperiod.contains(y))) {
			use (TimeCategory){
				def getdate = new Date()
				def getdate1m = getdate + gvmonth.month
				PolicyPeriodTo = getdate1m.format('dd/MM/yyyy') as String
				println (gvmonth)
				println (getdate1m)
				println (PolicyPeriodTo)
			}
		}

		String cmonthPPT

		String cdayPPT

		def dayPPT = PolicyPeriodTo.split('/')[0]

		def monthPPT = PolicyPeriodTo.split('/')[1]

		def yearPPT = PolicyPeriodTo.split('/')[2]

		println (PolicyPeriodTo)
		println (yearPPT)
		println (policyperiod)
		println (gvyear)
		println (gvmonth)

		//convert month
		if (monthPPT == '01') {
			cmonthPPT = 'Jan'
		} else if (monthPPT == '02') {
			cmonthPPT = 'Feb'
		} else if (monthPPT == '03') {
			cmonthPPT = 'Mar'
		} else if (monthPPT == '04') {
			cmonthPPT = 'Apr'
		} else if (monthPPT == '05') {
			cmonthPPT = 'May'
		} else if (monthPPT == '06') {
			cmonthPPT = 'Jun'
		} else if (monthPPT == '07') {
			cmonthPPT = 'Jul'
		} else if (monthPPT == '08') {
			cmonthPPT = 'Aug'
		} else if (monthPPT == '09') {
			cmonthPPT = 'Sep'
		} else if (monthPPT == '10') {
			cmonthPPT = 'Oct'
		} else if (monthPPT == '11') {
			cmonthPPT = 'Nov'
		} else if (monthPPT == '12') {
			cmonthPPT = 'Dec'
		} else {
			println('Month not found')
		}

		//convert day
		if (dayPPT == '01') {
			cdayPPT = '1'
		} else if (dayPPT == '02') {
			cdayPPT = '2'
		} else if (dayPPT == '03') {
			cdayPPT = '3'
		} else if (dayPPT == '04') {
			cdayPPT = '4'
		} else if (dayPPT == '05') {
			cdayPPT = '5'
		} else if (dayPPT == '06') {
			cdayPPT = '6'
		} else if (dayPPT == '07') {
			cdayPPT = '7'
		} else if (dayPPT == '08') {
			cdayPPT = '8'
		} else if (dayPPT == '09') {
			cdayPPT = '9'
		} else {
			cdayPPT = dayPPT
		}

		while (WebUI.verifyElementVisible(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_YEAR', [('year') : yearPPT])) == false) {
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CAL_NEXT'))
			WebUI.delay(1)
		}

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_YEAR', [('year') : yearPPT]))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_MONTH', [('month') : cmonthPPT]))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_DAY', [('day') : cdayPPT]), FailureHandling.OPTIONAL)

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_DAY - ACTIVE', [('day') : cdayPPT]), FailureHandling.OPTIONAL)

		WebUI.delay(1)

		////////////////////////////// PolicyPeriodTo - END
	}

	@Keyword
	def PolicyPeriodTo () {

		def periodto = findTestData(GlobalVariable.vmparam).getValue(6, 1) as String

		////////////////////////////// PolicyPeriodTo - START
		String PolicyPeriodTo = periodto

		String cmonthPPT

		String cdayPPT

		def dayPPT = PolicyPeriodTo.split('/')[0]

		def monthPPT = PolicyPeriodTo.split('/')[1]

		def yearPPT = PolicyPeriodTo.split('/')[2]

		//convert month
		if (monthPPT == '01') {
			cmonthPPT = 'Jan'
		} else if (monthPPT == '02') {
			cmonthPPT = 'Feb'
		} else if (monthPPT == '03') {
			cmonthPPT = 'Mar'
		} else if (monthPPT == '04') {
			cmonthPPT = 'Apr'
		} else if (monthPPT == '05') {
			cmonthPPT = 'May'
		} else if (monthPPT == '06') {
			cmonthPPT = 'Jun'
		} else if (monthPPT == '07') {
			cmonthPPT = 'Jul'
		} else if (monthPPT == '08') {
			cmonthPPT = 'Aug'
		} else if (monthPPT == '09') {
			cmonthPPT = 'Sep'
		} else if (monthPPT == '10') {
			cmonthPPT = 'Oct'
		} else if (monthPPT == '11') {
			cmonthPPT = 'Nov'
		} else if (monthPPT == '12') {
			cmonthPPT = 'Dec'
		} else {
			println('Month not found')
		}

		//convert day
		if (dayPPT == '01') {
			cdayPPT = '1'
		} else if (dayPPT == '02') {
			cdayPPT = '2'
		} else if (dayPPT == '03') {
			cdayPPT = '3'
		} else if (dayPPT == '04') {
			cdayPPT = '4'
		} else if (dayPPT == '05') {
			cdayPPT = '5'
		} else if (dayPPT == '06') {
			cdayPPT = '6'
		} else if (dayPPT == '07') {
			cdayPPT = '7'
		} else if (dayPPT == '08') {
			cdayPPT = '8'
		} else if (dayPPT == '09') {
			cdayPPT = '9'
		} else {
			cdayPPT = dayPPT
		}

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CAL_1'))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CAL_2'))

		WebUI.delay(1)

		while (WebUI.verifyElementVisible(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_YEAR', [('year') : yearPPT])) == false) {
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CAL_NEXT'))
			WebUI.delay(1)
		}

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_YEAR', [('year') : yearPPT]))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_MONTH', [('month') : cmonthPPT]))

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_DAY', [('day') : cdayPPT]), FailureHandling.OPTIONAL)

		WebUI.delay(1)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/PPT_CHOOSE_DAY - ACTIVE', [('day') : cdayPPT]), FailureHandling.OPTIONAL)

		WebUI.delay(1)

		////////////////////////////// PolicyPeriodTo - END
	}
}











