package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.stringtemplate.v4.compiler.STParser.ifstat_return

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.time.TimeCategory
import internal.GlobalVariable

public class Kendaraan {
	@Keyword
	def InterestCoverage (){
		def tipe = findTestData(GlobalVariable.vmparam).getValue(2, 1) as String
		def productcode = findTestData(GlobalVariable.vmparam).getValue(4, 1) as String
		def policyperiod = findTestData(GlobalVariable.vmparam).getValue(56, 1).toUpperCase()
		def com = 'COM'
		def tlo = 'TLO'
		def lengkap = 'LENGKAP'
		def tpl = 'TPL'
		def acc = 'ACC'
		def pad = 'PAD'
		def pap = 'PAP'
		def cla = 'CLA'
		def fes = 'FES'
		def ts = 'TS'
		def y = 'Y'
		def m = 'M'

		WebUI.delay(2)

		if (tipe.contains(com)) {
			WebUI.delay(3)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CHK_COMPREHENSIVE'))
			WebUI.delay(5)

			/////////////////////COM TLO

			def gvyear = GlobalVariable.key1 as Integer
			def gvmonth = GlobalVariable.key2 as Integer

			WebUI.delay(2)

			if (tipe.contains(tlo)){
				def COMPeriodTo = ''

				if (policyperiod.contains(y) && policyperiod.contains(m)) {
					use (TimeCategory){

						def getdate = new Date()
						def getdate1m = getdate + gvyear.year
						COMPeriodTo = getdate1m.format('dd/MM/yyyy') as String
					}
				}else if (policyperiod.contains(y) && !(policyperiod.contains(m))) {
					use (TimeCategory){
						def gvyear1 = gvyear - 1 as Integer

						if (gvyear1 == 0) {
							def gvyear0 = gvyear / 2 as Integer
							def getdate = new Date()
							def getdate1y = getdate + gvyear0.month
							COMPeriodTo = getdate1y.format('dd/MM/yyyy') as String
						}else if (gvyear1 != 0) {
							def getdate = new Date()
							def getdate1y = getdate + gvyear1.year
							COMPeriodTo = getdate1y.format('dd/MM/yyyy') as String
						}
					}
				} else if (policyperiod.contains(m) && !(policyperiod.contains(y))) {
					use (TimeCategory){
						def gvmonth1 = gvmonth / 2 as Integer
						def getdate = new Date()
						def getdate1m = getdate + gvmonth.month
						COMPeriodTo = getdate1m.format('dd/MM/yyyy') as String
					}
				}

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_COM_PERIOD_TO'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CAL_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CAL_2'))

				String cmonthCPT

				String cdayCPT

				def dayCPT = COMPeriodTo.split('/')[0]

				def monthCPT = COMPeriodTo.split('/')[1]

				def yearCPT = COMPeriodTo.split('/')[2] as String

				//convert month
				if (monthCPT == '01') {
					cmonthCPT = 'Jan'
				} else if (monthCPT == '02') {
					cmonthCPT = 'Feb'
				} else if (monthCPT == '03') {
					cmonthCPT = 'Mar'
				} else if (monthCPT == '04') {
					cmonthCPT = 'Apr'
				} else if (monthCPT == '05') {
					cmonthCPT = 'May'
				} else if (monthCPT == '06') {
					cmonthCPT = 'Jun'
				} else if (monthCPT == '07') {
					cmonthCPT = 'Jul'
				} else if (monthCPT == '08') {
					cmonthCPT = 'Aug'
				} else if (monthCPT == '09') {
					cmonthCPT = 'Sep'
				} else if (monthCPT == '10') {
					cmonthCPT = 'Oct'
				} else if (monthCPT == '11') {
					cmonthCPT = 'Nov'
				} else if (monthCPT == '12') {
					cmonthCPT = 'Dec'
				} else {
					println('Month not found')
				}

				//convert day
				if (dayCPT == '01') {
					cdayCPT = '1'
				} else if (dayCPT == '02') {
					cdayCPT = '2'
				} else if (dayCPT == '03') {
					cdayCPT = '3'
				} else if (dayCPT == '04') {
					cdayCPT = '4'
				} else if (dayCPT == '05') {
					cdayCPT = '5'
				} else if (dayCPT == '06') {
					cdayCPT = '6'
				} else if (dayCPT == '07') {
					cdayCPT = '7'
				} else if (dayCPT == '08') {
					cdayCPT = '8'
				} else if (dayCPT == '09') {
					cdayCPT = '9'
				} else {
					cdayCPT = dayCPT
				}

				WebUI.delay(1)

				while (WebUI.verifyElementVisible(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CHOOSE_YEAR',[('yearCPT') : yearCPT])) == false) {
					WebUI.delay(1)
					WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CAL_NEXT'))
					WebUI.delay(1)
				}

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CHOOSE_YEAR',[('yearCPT') : yearCPT]))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CHOOSE_MONTH',[('cmonthCPT') : cmonthCPT]))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CPT_CHOOSE_DAY',[('cdayCPT') : cdayCPT]))

				WebUI.delay(3)
			}

			/////////////////////SRCC FLD ETV
			if (tipe.contains(com) && tipe.contains(fes)) {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_ADD'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/EXP_COVERAGE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/LST_COVERAGE_EARTHQUAKE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/BTN_SAVE'))

				WebUI.delay(2)
			}

			/////////////////////TS
			if (tipe.contains(com) && tipe.contains(ts)) {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_ADD'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/EXP_COVERAGE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/LST_COVERAGE_TS'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/BTN_SAVE'))

				WebUI.delay(2)
			}

			/////////////////////COM TLO TS
			if (tipe.contains(tlo) && tipe.contains(ts)) {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_ADD'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/EXP_BSC_COVERAGE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/LST_BSC_TLO'))

				WebUI.delay(10)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/EXP_COVERAGE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/LST_COVERAGE_TSTLO'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/BTN_SAVE'))

				WebUI.delay(2)
			}

			///////////////////////////// COM END
		} else if (tipe.contains(tlo)) {
			WebUI.delay(3)

			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/CHK_TLO'))

			WebUI.delay(3)

			if (tipe.contains(tlo) && tipe.contains(ts)) {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_ADD'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/EXP_COVERAGE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/LST_COVERAGE_TSTLO'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/SRCC ETV FLD TS/BTN_SAVE'))

				WebUI.delay(2)
			}
		}

		WebUI.delay(3)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_CALCULATE'))

		WebUI.delay(12)

		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_SAVE'))

		WebUI.delay(1)
	}
}
