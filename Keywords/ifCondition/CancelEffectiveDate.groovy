package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.time.TimeCategory
import internal.GlobalVariable

public class CancelEffectiveDate {

	@Keyword
	def input () {

		def effectivedate = findTestData("Cancellation/Effective Date Latest Policy").getValue(1,1)

		println (effectivedate)
		////////////////////////////// effdate - START
		String effdate = effectivedate

		String cmonthED

		String cdayED

		def dayED = effdate.split('/')[0]

		def monthED = effdate.split('/')[1]

		def yearED = effdate.split('/')[2]

		//convert month
		if (monthED == '01') {
			cmonthED = 'Jan'
		} else if (monthED == '02') {
			cmonthED = 'Feb'
		} else if (monthED == '03') {
			cmonthED = 'Mar'
		} else if (monthED == '04') {
			cmonthED = 'Apr'
		} else if (monthED == '05') {
			cmonthED = 'May'
		} else if (monthED == '06') {
			cmonthED = 'Jun'
		} else if (monthED == '07') {
			cmonthED = 'Jul'
		} else if (monthED == '08') {
			cmonthED = 'Aug'
		} else if (monthED == '09') {
			cmonthED = 'Sep'
		} else if (monthED == '10') {
			cmonthED = 'Oct'
		} else if (monthED == '11') {
			cmonthED = 'Nov'
		} else if (monthED == '12') {
			cmonthED = 'Dec'
		} else {
			println('Month not found')
		}

		//convert day
		if (dayED == '01') {
			cdayED = '1'
		} else if (dayED == '02') {
			cdayED = '2'
		} else if (dayED == '03') {
			cdayED = '3'
		} else if (dayED == '04') {
			cdayED = '4'
		} else if (dayED == '05') {
			cdayED = '5'
		} else if (dayED == '06') {
			cdayED = '6'
		} else if (dayED == '07') {
			cdayED = '7'
		} else if (dayED == '08') {
			cdayED = '8'
		} else if (dayED == '09') {
			cdayED = '9'
		} else {
			cdayED = dayED
		}
		/*
		 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CAL_1'))
		 WebUI.delay(1)
		 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CAL_2'))
		 WebUI.delay(1)
		 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CHOOSE_YEAR', [('edyear') : yearED]))
		 WebUI.delay(1)
		 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CHOOSE_MONTH', [('edmonth') : cmonthED]))
		 */
		WebUI.delay(1)

		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CHOOSE_DAY - ACTIVE', [('edday') : cdayED]), FailureHandling.OPTIONAL)

		WebUI.delay(1)

		//		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CHOOSE_DAY', [('edday') : cdayED]), FailureHandling.OPTIONAL)

		//		WebUI.delay(1)

		////////////////////////////// effdate - END
	}
}
