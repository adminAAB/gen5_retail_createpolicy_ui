package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class FinInstitution {

	@Keyword
	def check () {

		def productcode = (findTestData(GlobalVariable.vmparam).getValue(4, 1)) as String

		if (productcode == 'MFGM3') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'GMMG1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'TALN4') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'TALN5') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'OTGN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'OTGON') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'OTGOU') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'OTGU1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'LNLN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'LULN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'LULN2') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'VULN2') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'VULN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'VNLN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'VULN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'VNLN1') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'VULN2') {
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'ACCG0001')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}else if (productcode == 'BCLN3') {
		
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_FINANCIAL_INSTITUTION'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/TXT_SEARCH_BY_INPUT'), 'BCA')

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SEARCH'))

			WebUI.delay(8)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/Financial Institution/BTN_SELECT'))

			WebUI.delay(1)
		}
	}
}
