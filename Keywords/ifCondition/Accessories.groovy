package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Accessories {
	@Keyword
	def InterestAccessories (){
		def tipe = findTestData(GlobalVariable.vmparam).getValue(2, 1) as String
		def acc = 'ACC'
		if (tipe.contains(acc)) {
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_ACCESSORIES'))

			WebUI.delay(3)

			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))

			WebUI.delay(3)

			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/BTN_ADD'))

			WebUI.delay(2)

			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_ACCESSORIES'))

			WebUI.delay(2)

			/*
			 Category 1 : Kaca Film Bersertifikat
			 Quantity : Cover Jok Kulit
			 Velg Racing / Ban : pilih 4
			 */
			def acc1 = ((findTestData(GlobalVariable.vmparam).getValue(42, 1)) as String).toUpperCase()

			def acc1si = ((findTestData(GlobalVariable.vmparam).getValue(45, 1)) as String)

			def acc1qty = ((findTestData(GlobalVariable.vmparam).getValue(44, 1)) as String)

			def acc2 = ((findTestData(GlobalVariable.vmparam).getValue(46, 1)) as String).toUpperCase()

			def acc2si = ((findTestData(GlobalVariable.vmparam).getValue(49, 1)) as String)

			def acc2qty = ((findTestData(GlobalVariable.vmparam).getValue(48, 1)) as String)

			//// ACC 1
			if (acc1 == 'KACA FILM BERSERTIFIKAT' || acc1 == 'KACA FILM STANDAR') {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
						acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_CATEGORY'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_CATEGORY_1'))

				WebUI.delay(1)

				if (acc1si != 'INCLUDE' && acc1si != '') {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),acc1si)

					WebUI.delay(1)
				}
				else if (acc1si == 'INCLUDE') {
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_YES_INCLUDE'))
				}

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)

			} else if (acc1 == 'COVER JOK KULIT') {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
						acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_QUANTITY'),
						acc1qty)

				WebUI.delay(1)

				if (acc1si != 'INCLUDE' && acc1si != '') {
					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),acc1si)
				}
				else if (acc1si == 'INCLUDE') {
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_YES_INCLUDE'))
				}

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)

			} else if ((acc1 == 'VELG RACING') || (acc1 == 'VELG')) {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
						acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)

			}

			else if (acc1 == 'BAN') {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
						acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

				WebUI.delay(1)

				WebUI.doubleClick(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_FR_RH_INSURED'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_FR_RH_INSURED'), '1600000')

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_FR_RH_SUBMIT'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)

			}

			else if (acc1 == 'NON AUDIO SYSTEM') {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH')
						, acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_PART_ACCESSORIES'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_PART_SEARCH')
						, 'chrome spion')

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_PART_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/CHK_FR'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/CHK_RR'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)

			} else if (acc1 == 'AUDIO SYSTEM') {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH')
						, acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_PART_ACCESSORIES'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_PART_SEARCH')
						, 'cd player')

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_PART_RESULT_1'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),
						acc1si)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)

			}
			else if (acc1 == 'TV' || acc1 == 'CD CHANGER') {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
						acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_QUANTITY'),
						acc1qty)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_YES_INCLUDE'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)
			}
			else {
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
						acc1)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_QUANTITY'),
						acc1qty)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

				WebUI.delay(1)

				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),
						acc1si)

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

				WebUI.delay(1)
			}

			if (acc2 != '') {
				//// ACC 2
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/BTN_ADD'))

				WebUI.delay(1)

				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_ACCESSORIES'))

				WebUI.delay(1)

				if (acc2 == 'KACA FILM BERSERTIFIKAT' || acc2 == 'KACA FILM STANDAR') {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
							acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_CATEGORY'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_CATEGORY_1'))

					WebUI.delay(1)

					if (acc2si != 'INCLUDE' && acc2si != '') {
						WebUI.delay(1)

						WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),acc2si)

						WebUI.delay(1)
					}
					else if (acc2si == 'INCLUDE') {
						WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

						WebUI.delay(1)

						WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_YES_INCLUDE'))
					}

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)

				} else if (acc2 == 'COVER JOK KULIT') {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
							acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_QUANTITY'),
							acc2qty)

					WebUI.delay(1)

					if (acc2si != 'INCLUDE' && acc2si != '') {
						WebUI.delay(1)

						WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),acc2si)
					}
					else if (acc2si == 'INCLUDE') {
						WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

						WebUI.delay(1)

						WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_YES_INCLUDE'))
					}

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)

				} else if ((acc2 == 'VELG RACING') || (acc2 == 'VELG')) {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
							acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)

				}

				else if (acc2 == 'BAN') {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
							acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

					WebUI.delay(1)

					WebUI.doubleClick(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_FR_RH_INSURED'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_FR_RH_INSURED'), '1600000')

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_FR_RH_SUBMIT'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)

				}

				else if ((acc2 == 'NON AUDIO SYSTEM')) {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH')
							, acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_PART_ACCESSORIES'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_PART_SEARCH'), 'chrome spion')

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_PART_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/CHK_FR'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/CHK_RR'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)

				} else if (acc2 == 'AUDIO SYSTEM') {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH')
							, acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_PART_ACCESSORIES'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_PART_SEARCH')
							, 'cd player')

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_PART_RESULT_1'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),
							acc2si)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)

				}
				else if (acc2 == 'TV' || acc2 == 'CD CHANGER') {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
							acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_QUANTITY'),
							acc2qty)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_YES_INCLUDE'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)
				}

				else {
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_ACCESSORIES_SEARCH'),
							acc2)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_SEARCH_RESULT_1'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_QUANTITY'),
							acc2qty)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/EXP_INCLUDE_TSI'))

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/LST_NO_INCLUDE'))

					WebUI.delay(1)

					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/TXT_SUM_INSURED'),
							acc2si)

					WebUI.delay(1)

					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/ACCESORIES Detail/BTN_SAVE'))

					WebUI.delay(1)
				}
			}
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/BTN_X'))
		}

	}
}
