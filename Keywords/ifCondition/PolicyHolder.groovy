package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword

import com.kms.katalon.core.checkpoint.Checkpoint

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import com.kms.katalon.core.model.FailureHandling

import com.kms.katalon.core.testcase.TestCase

import com.kms.katalon.core.testdata.TestData

import com.kms.katalon.core.testobject.TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import internal.GlobalVariable

public class PolicyHolder {
	@Keyword
	def PHolder () {
		def productcode = findTestData(GlobalVariable.vmparam).getValue(4, 1)

		def BCLN3 = 'BCLN3'
		def GWN50 = 'GWN50'
		def GPN20 = 'GPN20'
		def GAN52 = 'GAN52'
		def GAU6S = 'GAU6S'

		if (productcode == GWN50) {
			/*WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_NEW'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/EXP_UPLOAD_TYPE'))

			WebUI.delay(1)

			def defineidtype= 'KTP'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_ID_IMAGE',[('idtype') : 'KTP']))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_CHOOSE_IMAGE'))

			WebUI.delay(1)

			new runEXE.UploadFile().Upload()

			WebUI.delay(20)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_CUSTOMER_NAME'),'New Otomate')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/EXP_ID_TYPE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/LST_TYPE_KTP'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_ID_NUMBER'),'1234567890098765')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_BIRTHDATE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/EXP_GENDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/LST_GENDER_MALE'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_HOME_ADDRESS'),'jl. alamat PH automate')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_POSTAL_CODE'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_POSTAL_CODE'),'12840')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SEARCH_POSTAL_CODE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/LST_POSTAL_CODE_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SELECT_POSTAL_CODE'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_HOME_NUMBER'),'87654321' )

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_MOBILE_NUMBER_1'),'08123456789')

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_EMAIL'),'JUH@BEYOND.ASURANSIASTRA.COM')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SAVE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SAVE_OK'))

			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))
			
			WebUI.delay(1)*/
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))
			
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))
			
			WebUI.delay(1)
			
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'101680583')
			
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SEARCH'))
			
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))
			
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))
			

			//Jika product BCLN3 dan Policy Holder NEW

		} else if (productcode == BCLN3) {

			//println ('MASUK BCLN3')

			/*WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_NEW'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/EXP_UPLOAD_TYPE'))

			WebUI.delay(1)

			def defineidtype= 'KTP'

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_ID_IMAGE',[('idtype') : 'KTP']))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_CHOOSE_IMAGE'))

			WebUI.delay(1)

			new runEXE.UploadFile().Upload()

			WebUI.delay(20)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_CUSTOMER_NAME'),'New Otomate')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/EXP_ID_TYPE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/LST_TYPE_KTP'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_ID_NUMBER'),'1234567890098765')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_BIRTHDATE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/EXP_GENDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/LST_GENDER_MALE'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_HOME_ADDRESS'),'jl. alamat PH automate')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_POSTAL_CODE'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_POSTAL_CODE'),'12840')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SEARCH_POSTAL_CODE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/LST_POSTAL_CODE_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SELECT_POSTAL_CODE'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_HOME_NUMBER'),'87654321' )

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_MOBILE_NUMBER_1'),'08123456789')

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/TXT_EMAIL'),'JUH@BEYOND.ASURANSIASTRA.COM')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SAVE'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/NEW/BTN_SAVE_OK'))

			WebUI.delay(1)
			*/
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))
		
		WebUI.delay(1)
		
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'101680583')
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SEARCH'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))
		

			//PRODUCT BCLN3 DAN POLICY HOLDER EXISTING

			/*           } else if (productcode == BCLN3){
			 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))
			 WebUI.delay(1)
			 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))
			 WebUI.delay(1)
			 WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'101680583')
			 WebUI.delay(1)
			 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_POLICY_HOLDER'))
			 WebUI.delay(1)
			 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))
			 WebUI.delay(1)
			 WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))
			 */         

		} else if (productcode == GPN20){

			//JALANIN YANG EXISTINg

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'101680583')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SEARCH'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))

		} else if (productcode == GAN52){

			//JALANIN YANG EXISTINg

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'163546163')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SEARCH'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))

			WebUI.delay(1)

		} else if (productcode == GAU6S){

			//JALANIN YANG EXISTINg

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))

			WebUI.delay(1)

			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'101680583')

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SEARCH'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))

			WebUI.delay(1)

		} else {

			WebUI.delay(3)
		}
	}
}