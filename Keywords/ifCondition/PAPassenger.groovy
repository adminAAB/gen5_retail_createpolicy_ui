package ifCondition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class PAPassenger {
	@Keyword
	def InterestPAP () {
		def tipe = findTestData(GlobalVariable.vmparam).getValue(2, 1) as String
		def pap = 'PAP'
		def lengkap = 'LENGKAP'
		def productcode = findTestData(GlobalVariable.vmparam).getValue(4, 1) as String
		if (tipe.contains(pap)) {
			if (productcode == 'LDN61'){
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_PA24'))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))
				WebUI.delay(1)
				//WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/TXT_PA_PASSENGER'), findTestData('NewPolicyParameters').getValue(26, 1))
				//WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/BTN_ADD'))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/PAP Detail/BTN_SAVE'))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/BTN_CALCULATE'))
				WebUI.delay(5)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/BTN_SAVE'))
				WebUI.delay(1)
			}else {
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_PA_PASSENGER'))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))
				WebUI.delay(1)
				WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/TXT_PA_PASSENGER'), findTestData(GlobalVariable.vmparam).getValue(26, 1))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/BTN_ADD'))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/PAP Detail/BTN_SAVE'))
				WebUI.delay(1)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/BTN_CALCULATE'))
				WebUI.delay(3)
				WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/PAP/BTN_SAVE'))
				WebUI.delay(1)
			}
		}
	}
}
