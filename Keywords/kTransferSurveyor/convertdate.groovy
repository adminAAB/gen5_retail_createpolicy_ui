package kTransferSurveyor

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.time.TimeCategory as TimeCategory
import internal.GlobalVariable

public class convertdate {
	@Keyword

	def convertday () {
		def today = new Date()

		def tomorrow = new Date() + 1

		def created = '05/09/2019' as String

		def active = '31/12/9999' as String

		def regiondate = '30/10/2018' as String

		String todayconvert = today.format('dd/MM/yyyy')

		GlobalVariable.tomorrowconvert = tomorrow.format('dd/MM/yyyy')

		String tomorrowday = GlobalVariable.tomorrowconvert.split('/')[0]

		//convert day
		if (tomorrowday == '01') {
			GlobalVariable.ctomorrowday = '1'
		} else if (tomorrowday == '02') {
			GlobalVariable.ctomorrowday = '2'
		} else if (tomorrowday == '03') {
			GlobalVariable.ctomorrowday = '3'
		} else if (tomorrowday == '04') {
			GlobalVariable.ctomorrowday = '4'
		} else if (tomorrowday == '05') {
			GlobalVariable.ctomorrowday = '5'
		} else if (tomorrowday == '06') {
			GlobalVariable.ctomorrowday = '6'
		} else if (tomorrowday == '07') {
			GlobalVariable.ctomorrowday = '7'
		} else if (tomorrowday == '08') {
			GlobalVariable.ctomorrowday = '8'
		} else if (tomorrowday == '09') {
			GlobalVariable.ctomorrowday = '9'
		} else {
			GlobalVariable.ctomorrowday = tomorrowday
		}
	}
}
