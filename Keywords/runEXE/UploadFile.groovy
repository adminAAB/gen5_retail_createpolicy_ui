package runEXE

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class UploadFile {
	@Keyword
	def Upload() {

		new executeQuery.GetIP().IPLocal()
		WebUI.delay(2)
		if (GlobalVariable.vmip == '172.16.94.191') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch01\\Supporting Files\\Open File Upload.exe');
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch02\\Supporting Files\\Open File Upload.exe');
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch03\\Supporting Files\\Open File Upload.exe');
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch04\\Supporting Files\\Open File Upload.exe');
		}else {
			Runtime.getRuntime().exec('D:\\katalon\\A2ISRetail\\Supporting Files\\Open File Upload.exe');
		}
	}

	@Keyword
	def UploadPath() {

		new executeQuery.GetIP().IPLocal()
		WebUI.delay(2)
		if (GlobalVariable.vmip == '172.16.94.191') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch01\\Supporting Files\\Open File Path Upload.exe');
		}else if (GlobalVariable.vmip == '172.16.94.192') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch02\\Supporting Files\\Open File Path Upload.exe');
		}else if (GlobalVariable.vmip == '172.16.94.193') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch03\\Supporting Files\\Open File Path Upload.exe');
		}else if (GlobalVariable.vmip == '172.16.94.194') {
			Runtime.getRuntime().exec('C:\\Users\\Administrator\\.jenkins\\workspace\\A2ISRetail_Testing_Branch04\\Supporting Files\\Open File Path Upload.exe');
		}else {
			Runtime.getRuntime().exec('D:\\katalon\\A2ISRetail\\Supporting Files\\Open File Upload.exe');
		}
	}
}
