<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_BIRTHDATE</name>
   <tag></tag>
   <elementGuidId>a56860cc-870e-4db6-bd3e-b0377484a2e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PGCreatePers1DPBirth&quot;]/a2is-datepicker-wide-dc/div[2]/div[1]/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/FRAME']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PGCreatePers1DPBirth&quot;]/a2is-datepicker-wide-dc/div[2]/div[1]/div/span</value>
   </webElementXpaths>
</WebElementEntity>
