<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_EXISTING_CUST_COMPANY</name>
   <tag></tag>
   <elementGuidId>ed60e792-17aa-4a9f-9d4d-8ebfe97215a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelCorporateInfo-0&quot;]/div[1]/div[2]/a2is-buttons/div/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*/a2is-buttons[contains(@params,&quot;name: 'PGCreateCorp1ExistingCustomer'&quot;)]</value>
   </webElementXpaths>
</WebElementEntity>
