<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_CLEAR_CUSTOMER</name>
   <tag></tag>
   <elementGuidId>0e619384-63d0-4a43-a939-9a1ecf769366</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/div[2]/a2is-buttons/div/div/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/div[2]/a2is-buttons/div/div/button[2]</value>
   </webElementXpaths>
</WebElementEntity>
