<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_MARITAL</name>
   <tag></tag>
   <elementGuidId>2e0ef5c7-a6a8-49b0-8d96-32b72a9da807</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/div/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/div/button[contains(text(),&quot;${marital}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/div/button[contains(text(),&quot;${marital}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/div/button[2]</value>
   </webElementXpaths>
</WebElementEntity>
