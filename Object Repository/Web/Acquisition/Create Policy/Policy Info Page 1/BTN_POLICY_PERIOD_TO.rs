<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_POLICY_PERIOD_TO</name>
   <tag></tag>
   <elementGuidId>521103c5-73a8-46a9-b769-49e93fe3ea1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;DivPeriodTo&quot;]/a2is-datepicker-wide-dc/div[2]/div[1]/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;DivPeriodTo&quot;]/a2is-datepicker-wide-dc/div[2]/div[1]/div/span</value>
   </webElementXpaths>
</WebElementEntity>
