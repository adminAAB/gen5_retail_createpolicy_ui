<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_POLICY_HOLDER</name>
   <tag></tag>
   <elementGuidId>27868338-7116-4887-8a3c-03f7d6ff89ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[1]/div/div[1]/a2is-sections/div[2]/div[5]/div/div[1]/div/div[2]/a2is-panels/div[2]/div[2]/div[1]/div[2]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button[count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[1]/div/div[1]/a2is-sections/div[2]/div[5]/div/div[1]/div/div[2]/a2is-panels/div[2]/div[2]/div[1]/div[2]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
