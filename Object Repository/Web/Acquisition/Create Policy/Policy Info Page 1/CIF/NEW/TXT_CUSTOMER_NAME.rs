<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_CUSTOMER_NAME</name>
   <tag></tag>
   <elementGuidId>a6117dde-0c05-4527-8f26-c9c147e2facb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CustPersonal&quot;]/a2is-textbox-wide-dc[1]/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;CustPersonal&quot;]/a2is-textbox-wide-dc[1]/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CustPersonal&quot;]/a2is-textbox-wide-dc[1]/div[2]/div[1]/div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;CustPersonal&quot;]/a2is-textbox-wide-dc[1]/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
