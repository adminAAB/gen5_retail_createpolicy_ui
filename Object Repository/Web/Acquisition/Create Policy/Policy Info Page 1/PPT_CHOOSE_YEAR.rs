<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PPT_CHOOSE_YEAR</name>
   <tag></tag>
   <elementGuidId>4f0b99ec-797d-45ee-9da6-be032d91586b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[6]/div/div[3]/table/tbody/tr/td/span[11]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[7]/div/div[3]/table/tbody/tr/td/span[contains(text(),&quot;${year}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[7]/div/div[3]/table/tbody/tr/td/span[contains(text(),&quot;${year}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>/html/body/div[6]/div/div[3]/table/tbody/tr/td/span[11]</value>
   </webElementXpaths>
</WebElementEntity>
