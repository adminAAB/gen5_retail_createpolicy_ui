<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_CHOOSE_IMAGE</name>
   <tag></tag>
   <elementGuidId>7c332197-c1fa-4d58-89b7-f37c0edbceef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelPolicyImageInfo-0&quot;]/div[1]/div[3]/a2is-textbox-wide-nc/div[2]/div[1]/div/span/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelPolicyImageInfo-0&quot;]/div[1]/div[4]/a2is-textbox-wide-nc/div[2]/div[1]/div/span/button[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelPolicyImageInfo-0&quot;]/div[1]/div[4]/a2is-textbox-wide-nc/div[2]/div[1]/div/span/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelPolicyImageInfo-0&quot;]/div[1]/div[3]/a2is-textbox-wide-nc/div[2]/div[1]/div/span/button</value>
   </webElementXpaths>
</WebElementEntity>
