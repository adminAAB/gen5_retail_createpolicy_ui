<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_PREORDER_VA</name>
   <tag></tag>
   <elementGuidId>630e0ecc-d200-48df-97d4-6d786f22d2f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-textbox-wide-dc[18]/div[2]/div[1]/div/span/input[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-textbox-wide-dc[7]/div[2]/div[1]/div/span/input[3][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-textbox-wide-dc[7]/div[2]/div[1]/div/span/input[3]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-textbox-wide-dc[13]/div[2]/div[1]/div/span/input[3]</value>
   </webElementXpaths>
</WebElementEntity>
