<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_BILLING_ADDRESS</name>
   <tag></tag>
   <elementGuidId>17f5c036-114b-4836-80eb-9e58c1a40f22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[7]/div[2]/div[1]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[1]/div/div[1]/a2is-sections/div[2]/div[5]/div/div[1]/div/div[2]/a2is-panels/div[2]/div[2]/div[1]/a2is-combo-wide-dc[6]/div[2]/div[1]/div/button/span[1][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[1]/div/div[1]/a2is-sections/div[2]/div[5]/div/div[1]/div/div[2]/a2is-panels/div[2]/div[2]/div[1]/a2is-combo-wide-dc[6]/div[2]/div[1]/div/button/span[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[8]/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
