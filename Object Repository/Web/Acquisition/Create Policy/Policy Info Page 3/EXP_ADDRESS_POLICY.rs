<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_ADDRESS_POLICY</name>
   <tag></tag>
   <elementGuidId>2176f21d-fc2c-4ffc-a891-a4baae6ef6d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/button[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
