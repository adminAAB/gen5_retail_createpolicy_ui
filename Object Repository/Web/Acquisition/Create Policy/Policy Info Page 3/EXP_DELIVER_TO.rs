<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_DELIVER_TO</name>
   <tag></tag>
   <elementGuidId>71ffa6c5-260c-4218-b31a-ec232a186b08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[6]/div[2]/div[1]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGCreatePolicy3ShowHideIsNeedHardCopy&quot;]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/button[count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGCreatePolicy3ShowHideIsNeedHardCopy&quot;]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[6]/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
