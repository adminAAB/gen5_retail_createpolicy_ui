<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_SEARCH_POSTAL_CODE</name>
   <tag></tag>
   <elementGuidId>f3522dcb-3043-43a4-9960-f97326e2ab7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGCreatePolicy3TBPostalCodeDeliveryAddressDesc&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGCreatePolicy3TBPostalCodeDeliveryAddressDesc&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
