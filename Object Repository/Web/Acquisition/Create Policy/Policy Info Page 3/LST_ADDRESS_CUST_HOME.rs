<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_ADDRESS_CUST_HOME</name>
   <tag></tag>
   <elementGuidId>ad0dcc1e-aab4-4af4-9500-e6999192a7e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/div/button[1][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[4]/div[2]/div[1]/div/div/button[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelPolicyInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/div/button[1]</value>
   </webElementXpaths>
</WebElementEntity>
