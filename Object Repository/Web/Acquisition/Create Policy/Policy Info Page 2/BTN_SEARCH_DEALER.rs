<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_SEARCH_DEALER</name>
   <tag></tag>
   <elementGuidId>73a5f5c3-7db2-4547-ab9d-d32f7298e854</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGCreatePolicy2TBDealer&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGCreatePolicy2TBDealer&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
