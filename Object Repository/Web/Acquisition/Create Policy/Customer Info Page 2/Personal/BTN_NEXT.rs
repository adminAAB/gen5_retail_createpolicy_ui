<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_NEXT</name>
   <tag></tag>
   <elementGuidId>71efee05-829a-418f-8e14-92020cfd04fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PGCreatePolicyChild-0&quot;]/div[1]/div/div[2]/a2is-buttons/div/div/button[4]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGCreatePolicyChild-0&quot;]/div[1]/div/a2is-buttons/div/div/button[4][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGCreatePolicyChild-0&quot;]/div[1]/div/a2is-buttons/div/div/button[4]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PGCreatePolicyChild-0&quot;]/div[1]/div/a2is-buttons/div/div/button[4]</value>
   </webElementXpaths>
</WebElementEntity>
