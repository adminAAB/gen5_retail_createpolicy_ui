<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_SEARCH_RESULT_1</name>
   <tag></tag>
   <elementGuidId>daaed3a6-64c2-418d-8023-ccf394977b42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/li[@class = &quot;select2-results__option select2-results__option--highlighted&quot;][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/li[@class = &quot;select2-results__option select2-results__option--highlighted&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
