<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_ENGINE_NO</name>
   <tag></tag>
   <elementGuidId>c701a0f1-9e60-4ee1-b228-894261682767</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;EngineNumber&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;EngineNumber&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;EngineNumber&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;EngineNumber&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
