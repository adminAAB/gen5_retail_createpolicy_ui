<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_TPL</name>
   <tag></tag>
   <elementGuidId>32d61298-212b-4e6c-b85f-ba7d5050b8ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[contains(text(),'${TPL}')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[contains(text(),&quot;${tplsi}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[contains(text(),&quot;${tplsi}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[contains(text(),'${TPL}')]</value>
   </webElementXpaths>
</WebElementEntity>
