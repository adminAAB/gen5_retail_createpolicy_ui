<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CPT_CHOOSE_DAY</name>
   <tag></tag>
   <elementGuidId>5dad20f7-07a9-4930-a095-36323aba47ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[17]/div/div[1]/table/tbody/*/td[@class = &quot;day&quot; and contains(text(),&quot;${cdayCPT}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[17]/div/div[1]/table/tbody/*/td[@class = &quot;day&quot; and contains(text(),&quot;${cdayCPT}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
