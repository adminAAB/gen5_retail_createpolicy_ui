<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_COVERAGE_TSTLO</name>
   <tag></tag>
   <elementGuidId>aeeb7f68-cddb-4a51-afb4-a39051eb84e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[text()='TERRORISM TLO']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[text()='TERRORISM TLO']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value></value>
   </webElementXpaths>
</WebElementEntity>
