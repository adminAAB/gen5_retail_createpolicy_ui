<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_BSC_COMPREHENSIVE</name>
   <tag></tag>
   <elementGuidId>027cd2d7-c24a-4321-bf36-178c3f471b06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='Comprehensive'][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='Comprehensive']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value></value>
   </webElementXpaths>
</WebElementEntity>
