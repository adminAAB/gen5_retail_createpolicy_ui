<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_PERIOD_TO</name>
   <tag></tag>
   <elementGuidId>c375bb81-9c52-4844-b15c-4b02a054919b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-datepicker-wide-dc[2]/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PUCoverage-0&quot;]/div[1]/div/div/a2is-datepicker-wide-dc[2]/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
