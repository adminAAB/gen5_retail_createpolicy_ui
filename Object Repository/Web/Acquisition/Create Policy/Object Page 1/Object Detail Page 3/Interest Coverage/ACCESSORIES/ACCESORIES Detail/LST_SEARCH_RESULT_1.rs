<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_SEARCH_RESULT_1</name>
   <tag></tag>
   <elementGuidId>b85a2054-4d98-4515-8170-4fe9f3246845</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*/li[@class=&quot;select2-results__option select2-results__option--highlighted&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*/li[@class=&quot;select2-results__option select2-results__option--highlighted&quot;]</value>
   </webElementXpaths>
</WebElementEntity>
