<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_ACCESSORIES</name>
   <tag></tag>
   <elementGuidId>8457b9e8-772d-4827-a251-d92bc35ce209</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-searchable-wide-dc/div[2]/div[1]/div/span/span[1]/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-searchable-wide-dc/div[2]/div[1]/div/span/span[1]/span</value>
   </webElementXpaths>
</WebElementEntity>
