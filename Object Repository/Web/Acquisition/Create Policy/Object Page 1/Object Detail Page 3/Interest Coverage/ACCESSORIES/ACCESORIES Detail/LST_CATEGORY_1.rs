<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_CATEGORY_1</name>
   <tag></tag>
   <elementGuidId>c5af5333-b186-477a-a944-87b23b4d0f2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*/li[text()='Category 1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/li[text()='Category 1']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[2]</value>
   </webElementXpaths>
</WebElementEntity>
