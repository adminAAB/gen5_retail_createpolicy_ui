<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_CATEGORY</name>
   <tag></tag>
   <elementGuidId>dca2d12e-897d-4d05-a996-21fab6e8b2c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-searchable-wide-dc[2]/div[2]/div[1]/div/span/span[1]/span[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-searchable-wide-dc[2]/div[2]/div[1]/div/span/span[1]/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PUAccessoriesAddEdit-0&quot;]/div[1]/div/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
