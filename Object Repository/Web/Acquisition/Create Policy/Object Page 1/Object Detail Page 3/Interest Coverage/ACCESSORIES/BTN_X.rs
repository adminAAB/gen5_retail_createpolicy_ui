<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_X</name>
   <tag></tag>
   <elementGuidId>345b4346-2ad4-45cd-bfab-f76fab40b65c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;D_a2is_ParentContainer&quot;]/a2is-popup[10]/div/div[2]/div/div[1]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;D_a2is_ParentContainer&quot;]/div[2]/a2is-popup[11]/div/div[2]/div/div[1]/button[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;D_a2is_ParentContainer&quot;]/div[2]/a2is-popup[11]/div/div[2]/div/div[1]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;D_a2is_ParentContainer&quot;]/a2is-popup[10]/div/div[2]/div/div[1]/button</value>
   </webElementXpaths>
</WebElementEntity>
