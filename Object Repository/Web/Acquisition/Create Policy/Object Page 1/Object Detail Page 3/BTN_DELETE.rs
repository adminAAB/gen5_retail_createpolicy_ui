<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_DELETE</name>
   <tag></tag>
   <elementGuidId>df6f45bd-5f4c-4860-b7e6-ddc8a0cebac6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[2]/div/div[1]/div/div[2]/a2is-panels/div[2]/div[2]/div[1]/div[1]/div/a2is-datatable/div[2]/div/div/button[2][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[2]/div/div[1]/div/div[2]/a2is-panels/div[2]/div[2]/div[1]/div[1]/div/a2is-datatable/div[2]/div/div/button[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
