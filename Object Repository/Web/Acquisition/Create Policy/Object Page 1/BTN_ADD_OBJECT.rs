<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_ADD_OBJECT</name>
   <tag></tag>
   <elementGuidId>86f1c3c2-a8d4-489e-a3f2-6bdf94751de4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelObjectPolicyInfo-0&quot;]/div[1]/div[1]/a2is-datatable/div[2]/div/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelObjectPolicyInfo-0&quot;]/div[1]/div[1]/a2is-datatable/div[2]/div/div/button[1][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelObjectPolicyInfo-0&quot;]/div[1]/div[1]/a2is-datatable/div[2]/div/div/button[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelObjectPolicyInfo-0&quot;]/div[1]/div[1]/a2is-datatable/div[2]/div/div/button[1]</value>
   </webElementXpaths>
</WebElementEntity>
