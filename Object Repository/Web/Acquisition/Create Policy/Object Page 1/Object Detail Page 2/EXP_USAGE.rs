<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_USAGE</name>
   <tag></tag>
   <elementGuidId>4c8e1f68-b8de-4320-b15c-7837b0ac9df9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelAddEditObjectPolicyInfo-0&quot;]/div[1]/a2is-combo-searchable-wide-dc[2]/div[2]/div[1]/div/span/span[1]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.container-fluid.body-content:nth-child(5) div.a2is-content-for-PGCreatePolicyParent-0 div.a2is-content-for-PGCreatePolicyChild-6 div.row div.col-md-6:nth-child(2) div.panel.panel-default.agreyborder:nth-child(2) div.panel-body.collapse.in div.a2is-content-for-PanelAddEditObjectPolicyInfo-0 div.form-group.form-group-sm.col-sm-12 div.col-sm-8 div:nth-child(1) span.select2.select2-container.select2-container--default.select2-container--focus span.selection > span.select2-selection.select2-selection--single</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelAddEditObjectPolicyInfo-0&quot;]/div[1]/a2is-combo-searchable-wide-dc[2]/div[2]/div[1]/div/span/span[1]/span[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelAddEditObjectPolicyInfo-0&quot;]/div[1]/a2is-combo-searchable-wide-dc[2]/div[2]/div[1]/div/span/span[1]/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelAddEditObjectPolicyInfo-0&quot;]/div[1]/a2is-combo-searchable-wide-dc[2]/div[2]/div[1]/div/span/span[1]/span</value>
   </webElementXpaths>
</WebElementEntity>
