<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_REGION_RESULT</name>
   <tag></tag>
   <elementGuidId>ca1f329d-fd05-48a8-a0fb-64ce1a890a17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;select2-7b2t-results&quot;]/li</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*/li[@class = &quot;select2-results__option select2-results__option--highlighted&quot;][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/li[@class = &quot;select2-results__option select2-results__option--highlighted&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*/li[contains(text(),&quot;${region}&quot;)]</value>
   </webElementXpaths>
</WebElementEntity>
