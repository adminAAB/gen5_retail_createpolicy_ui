<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_BUMPER_DEPAN</name>
   <tag></tag>
   <elementGuidId>ae491fb9-33fe-4db6-9b44-59811f142600</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/li[text() = 'BUMPER DEPAN']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/li[text() = 'BUMPER DEPAN']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
