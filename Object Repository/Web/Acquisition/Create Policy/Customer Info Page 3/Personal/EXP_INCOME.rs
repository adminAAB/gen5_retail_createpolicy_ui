<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_INCOME</name>
   <tag></tag>
   <elementGuidId>91e9305c-cf1d-445c-a370-3266ae528322</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/button[count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PanelCustomerInfo-0&quot;]/div[1]/a2is-combo-wide-dc[5]/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
