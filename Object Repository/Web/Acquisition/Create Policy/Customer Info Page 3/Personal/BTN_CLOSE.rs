<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_CLOSE</name>
   <tag></tag>
   <elementGuidId>ea57d24a-8609-4263-a60c-87143854a2f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PGCreatePolicyChild-0&quot;]/div[1]/div/div[2]/a2is-buttons/div/div/button[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;PGCreatePolicyChild-0&quot;]/div[1]/div/div[2]/a2is-buttons/div/div/button[3]</value>
   </webElementXpaths>
</WebElementEntity>
