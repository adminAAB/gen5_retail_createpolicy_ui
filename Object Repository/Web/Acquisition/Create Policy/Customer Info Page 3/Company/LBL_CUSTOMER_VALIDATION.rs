<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_CUSTOMER_VALIDATION</name>
   <tag></tag>
   <elementGuidId>4b2adf69-694b-4e33-81a2-13d165898f0b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGCreatePolicyParent-0&quot;]/div[1]/a2is-sections/div[2]/div[4]/h3[contains(text(),&quot;Company Customer Validation&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGCreatePolicyParent-0&quot;]/div[1]/a2is-sections/div[2]/div[4]/h3[contains(text(),&quot;Company Customer Validation&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
