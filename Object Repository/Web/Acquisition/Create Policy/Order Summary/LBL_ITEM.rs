<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_ITEM</name>
   <tag></tag>
   <elementGuidId>939850ce-9279-47d9-b6a9-03b70d179e46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;SummaryText&quot;]/table/tbody/*/td[text()='${item}']/following-sibling::td[2][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;SummaryText&quot;]/table/tbody/*/td[text()='${item}']/following-sibling::td[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
