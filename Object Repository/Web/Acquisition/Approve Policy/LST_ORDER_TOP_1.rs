<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_ORDER_TOP_1</name>
   <tag></tag>
   <elementGuidId>6897e46a-f5eb-4736-9dea-277a4d5025f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGApprovePolicy-0&quot;]/div[1]/div/div[2]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${orderno}&quot;)][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGApprovePolicy-0&quot;]/div[1]/div/div[2]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${orderno}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
