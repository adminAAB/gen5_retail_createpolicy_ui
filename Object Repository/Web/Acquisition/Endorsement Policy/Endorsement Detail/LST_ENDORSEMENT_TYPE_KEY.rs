<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_ENDORSEMENT_TYPE_KEY</name>
   <tag></tag>
   <elementGuidId>4454c923-8638-45eb-b771-5e45fbc83db6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelEndorsementPolicyDetailInfo1-0&quot;]/div[1]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text() = '${endorsetype}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelEndorsementPolicyDetailInfo1-0&quot;]/div[1]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text() = '${endorsetype}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
