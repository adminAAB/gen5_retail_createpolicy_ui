<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_ADDRESS_ON_POLICY_KEY</name>
   <tag></tag>
   <elementGuidId>0697ca1b-f06c-4e8d-8680-c1761872a3e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGEndorsementPolicy3&quot;]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='${addressonpolicy}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGEndorsementPolicy3&quot;]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='${addressonpolicy}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
