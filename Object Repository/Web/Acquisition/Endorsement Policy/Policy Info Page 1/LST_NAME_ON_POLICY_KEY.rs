<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_NAME_ON_POLICY_KEY</name>
   <tag></tag>
   <elementGuidId>98c6fc73-d699-42dc-9572-9d4d5434fda7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGEndorsementPolicy1CBNameOnPolicy&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text() = '${nameonpolicy}'][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGEndorsementPolicy1CBNameOnPolicy&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text() = '${nameonpolicy}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
