<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_NEXT</name>
   <tag></tag>
   <elementGuidId>10f47430-38c5-46ad-a53f-d13151749d34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGEndorsementPolicy-4&quot;]/div[1]/div/div[2]/div/div/a2is-buttons/div/div/button[text()='Next'][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGEndorsementPolicy-4&quot;]/div[1]/div/div[2]/div/div/a2is-buttons/div/div/button[text()='Next']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
