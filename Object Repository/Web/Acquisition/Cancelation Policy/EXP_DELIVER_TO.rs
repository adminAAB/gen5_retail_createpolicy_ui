<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_DELIVER_TO</name>
   <tag></tag>
   <elementGuidId>fb28edab-b01f-4640-8e8c-47da370a4571</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUDeliveryAddress-0&quot;]/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='${delivtype}'][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUDeliveryAddress-0&quot;]/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='${delivtype}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
