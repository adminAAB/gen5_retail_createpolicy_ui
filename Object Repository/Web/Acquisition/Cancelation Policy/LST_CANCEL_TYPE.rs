<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_CANCEL_TYPE</name>
   <tag></tag>
   <elementGuidId>7d8047a4-a253-48ac-9ea3-5296013fe451</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[2]/div/div[1]/div[1]/div[2]/a2is-panels/div[2]/div[2]/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/button[count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[4]/div[1]/a2is-sections/div[2]/div[2]/div/div[1]/div[1]/div[2]/a2is-panels/div[2]/div[2]/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
