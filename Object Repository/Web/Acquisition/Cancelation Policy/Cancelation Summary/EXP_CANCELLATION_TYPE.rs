<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_CANCELLATION_TYPE</name>
   <tag></tag>
   <elementGuidId>6dd1670b-ad10-4273-a0ab-a333a0e9c747</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelSummaryDetailInfo-0&quot;]/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='${canceltype}'][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelSummaryDetailInfo-0&quot;]/div[1]/a2is-combo-wide-dc[1]/div[2]/div[1]/div/div/button[text()='${canceltype}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
