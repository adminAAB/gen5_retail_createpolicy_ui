<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_CUSTOMER_REQUEST</name>
   <tag></tag>
   <elementGuidId>2c69a7b0-c560-4908-a306-5db8af9ca5b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelSummaryDetailInfo-0&quot;]/div[1]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text() = 'Customer Request']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelSummaryDetailInfo-0&quot;]/div[1]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text() = 'Customer Request']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
