<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LNK_TASK_LIST</name>
   <tag></tag>
   <elementGuidId>b72b1a79-2fe0-4a48-a415-572ff5cae6c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a/span[text()='Task List']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a/span[text()='Task List']</value>
   </webElementProperties>
</WebElementEntity>
