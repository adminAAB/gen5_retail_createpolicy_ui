<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_TPL - ischeckTPL</name>
   <tag></tag>
   <elementGuidId>bb1a5aca-8918-4333-b658-83d679b8267e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsTPLChecked&quot; and @value = &quot;${ischeckTPL}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsTPLChecked&quot; and @value = &quot;${ischeckTPL}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
