<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_ETV - ischeckETV</name>
   <tag></tag>
   <elementGuidId>7540235f-4933-48ec-9c1d-27e415722b64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsETVChecked&quot; and @value = &quot;${ischeckETV}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsETVChecked&quot; and @value = &quot;${ischeckETV}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
