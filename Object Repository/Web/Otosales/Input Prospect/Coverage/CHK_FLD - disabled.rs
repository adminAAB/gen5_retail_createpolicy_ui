<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_FLD - disabled</name>
   <tag></tag>
   <elementGuidId>889b8766-508a-4eda-a444-e2f2e77ad204</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsFLDChecked&quot; and @disabled]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsFLDChecked&quot; and @disabled]</value>
   </webElementProperties>
</WebElementEntity>
