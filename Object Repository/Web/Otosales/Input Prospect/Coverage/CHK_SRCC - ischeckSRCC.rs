<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_SRCC - ischeckSRCC</name>
   <tag></tag>
   <elementGuidId>3a288425-4631-4301-a26d-3f9003021915</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsSRCCChecked&quot; and @value = &quot;${ischeckSRCC}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsSRCCChecked&quot; and @value = &quot;${ischeckSRCC}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
