<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_ACC</name>
   <tag></tag>
   <elementGuidId>f755c45c-21c1-4696-945e-8706507b5ac1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsACCESSChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsACCESSChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
