<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_PRODUCT_TYPE</name>
   <tag></tag>
   <elementGuidId>a2c704ba-e98b-4e10-99d7-207dd943e8a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;PRODUCT TYPE&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;PRODUCT TYPE&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
