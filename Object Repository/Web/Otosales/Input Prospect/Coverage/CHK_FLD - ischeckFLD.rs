<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_FLD - ischeckFLD</name>
   <tag></tag>
   <elementGuidId>52c9a8a2-f44e-481b-8fce-6ba577c1963f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsFLDChecked&quot; and @value = &quot;${ischeckFLD}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsFLDChecked&quot; and @value = &quot;${ischeckFLD}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
