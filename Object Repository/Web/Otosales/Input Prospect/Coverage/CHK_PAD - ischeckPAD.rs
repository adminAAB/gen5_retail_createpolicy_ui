<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_PAD - ischeckPAD</name>
   <tag></tag>
   <elementGuidId>7f4b2e1c-2ee8-443e-ade5-9eaf781a1f5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsPADRVRChecked&quot; and @value = &quot;${ischeckPAD}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsPADRVRChecked&quot; and @value = &quot;${ischeckPAD}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
