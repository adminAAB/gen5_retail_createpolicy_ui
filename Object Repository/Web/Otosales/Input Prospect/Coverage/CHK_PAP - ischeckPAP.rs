<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_PAP - ischeckPAP</name>
   <tag></tag>
   <elementGuidId>f6ebe09a-7767-4d0e-a693-a97fbb725ccb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsPAPASSChecked&quot; and @value = &quot;${ischeckPAP}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsPAPASSChecked&quot; and @value = &quot;${ischeckPAP}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
