<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_BASIC_COVER</name>
   <tag></tag>
   <elementGuidId>3fd3e204-7f75-4c2a-9e21-55731a3c6205</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;BASIC COVER&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;BASIC COVER&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
