<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_TS - ischeckTS</name>
   <tag></tag>
   <elementGuidId>b2e4a3b9-16b5-4394-ab53-5f63668f2ba3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsTSChecked&quot; and @value = &quot;${ischeckTS}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsTSChecked&quot; and @value = &quot;${ischeckTS}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
