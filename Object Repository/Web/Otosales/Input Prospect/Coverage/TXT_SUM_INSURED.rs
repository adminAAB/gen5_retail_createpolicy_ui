<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_SUM_INSURED</name>
   <tag></tag>
   <elementGuidId>034cceaa-b60f-4584-b486-46bfd640c35a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;SUM INSURED&quot;)]/following-sibling::div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;SUM INSURED&quot;)]/following-sibling::div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
