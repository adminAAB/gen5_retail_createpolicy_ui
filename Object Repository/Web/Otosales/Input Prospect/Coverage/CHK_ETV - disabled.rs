<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CHK_ETV - disabled</name>
   <tag></tag>
   <elementGuidId>83563bdb-ae86-4ab0-b365-4abfcce3c974</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = &quot;IsETVChecked&quot; and @disabled]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name = &quot;IsETVChecked&quot; and @disabled]</value>
   </webElementProperties>
</WebElementEntity>
