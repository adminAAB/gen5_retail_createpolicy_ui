<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_NEXT</name>
   <tag></tag>
   <elementGuidId>dc1da6d7-5b6f-4c0e-ae55-736125b5b26c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class=&quot;btn btn-success pull-right&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot;btn btn-success pull-right&quot;]</value>
   </webElementProperties>
</WebElementEntity>
