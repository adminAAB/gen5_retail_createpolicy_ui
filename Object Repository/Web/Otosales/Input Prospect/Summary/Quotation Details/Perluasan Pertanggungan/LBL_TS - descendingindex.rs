<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_TS - descendingindex</name>
   <tag></tag>
   <elementGuidId>3d820107-f823-4adf-ab6d-b48b35de54f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[2]/div[3]/div[2]/div[${descendingindex}]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[2]/div[3]/div[2]/div[${descendingindex}]/p</value>
   </webElementProperties>
</WebElementEntity>
