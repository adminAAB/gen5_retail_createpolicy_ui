<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_TPL - descendingindex</name>
   <tag></tag>
   <elementGuidId>69c20782-19b5-4aa8-b835-dfd73931a7f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[2]/div[4]/div[2]/div[${descendingindex}]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[2]/div[4]/div[2]/div[${descendingindex}]/p</value>
   </webElementProperties>
</WebElementEntity>
