<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_SRCC - descendingindex</name>
   <tag></tag>
   <elementGuidId>6da0d47a-8d62-4080-ba30-e14ed319e302</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[2]/div[2]/div[2]/div[${descendingindex}]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[2]/div[2]/div[2]/div[${descendingindex}]/p</value>
   </webElementProperties>
</WebElementEntity>
