<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_PREMI_DASAR - descendingindex</name>
   <tag></tag>
   <elementGuidId>be9cb825-120d-4f94-8a70-cb76acdabd29</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[1]/div[8]/div[2]/div[${descendingindex}]/p/b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[1]/div[8]/div[2]/div[${descendingindex}]/p/b</value>
   </webElementProperties>
</WebElementEntity>
