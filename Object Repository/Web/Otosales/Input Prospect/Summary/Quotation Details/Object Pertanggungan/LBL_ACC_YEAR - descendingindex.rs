<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_ACC_YEAR - descendingindex</name>
   <tag></tag>
   <elementGuidId>3f9bd07b-7aad-42eb-ac54-6ae81a8b4ca5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[1]/div[4]/div[2]/div[${descendingindex}]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div/div/div/div/fieldset[1]/div[4]/div[2]/div[${descendingindex}]/p</value>
   </webElementProperties>
</WebElementEntity>
