<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_ENGINE_NO</name>
   <tag></tag>
   <elementGuidId>9f06270b-9073-4ad8-9cd9-1bc386353799</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;ENGINE NUMBER&quot;)]/following-sibling::div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;ENGINE NUMBER&quot;)]/following-sibling::div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
