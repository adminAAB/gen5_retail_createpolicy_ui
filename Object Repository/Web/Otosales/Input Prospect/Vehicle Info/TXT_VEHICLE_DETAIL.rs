<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_VEHICLE_DETAIL</name>
   <tag></tag>
   <elementGuidId>9214244f-2f2e-481b-b306-83a3b8378330</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;Vehicle Details&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;Vehicle Details&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
