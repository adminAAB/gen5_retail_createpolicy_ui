<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_REGISTRATION_NO</name>
   <tag></tag>
   <elementGuidId>72a6531b-ba32-49b6-8fdd-7f39ca59c995</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;REGISTRATION NUMBER&quot;)]/following-sibling::div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;REGISTRATION NUMBER&quot;)]/following-sibling::div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
