<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_PROSPECT_NO_1</name>
   <tag></tag>
   <elementGuidId>7a9d6517-950b-4117-9f79-2e09ab3edef6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;PHONE NUMBER 1&quot;)]/following-sibling::div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;PHONE NUMBER 1&quot;)]/following-sibling::div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
