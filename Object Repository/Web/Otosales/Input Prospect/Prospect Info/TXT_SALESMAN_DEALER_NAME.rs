<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_SALESMAN_DEALER_NAME</name>
   <tag></tag>
   <elementGuidId>dccac6b1-9e2a-461a-ae90-554c04758a53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;SALESMAN DEALER NAME&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Datalist&quot;]/div/div/form/div[*]/label[contains(text(),&quot;SALESMAN DEALER NAME&quot;)]/following-sibling::div[1]/div[1]/div[1]/input[1]</value>
   </webElementProperties>
</WebElementEntity>
