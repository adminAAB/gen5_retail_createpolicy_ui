<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_JENIS_KELAMIN</name>
   <tag></tag>
   <elementGuidId>bb237d59-de29-4c09-901d-b35849019bf4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Jenis Kelamin']/following-sibling::div[1]/div[1]/div[1]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Jenis Kelamin']/following-sibling::div[1]/div[1]/div[1]/input</value>
   </webElementProperties>
</WebElementEntity>
