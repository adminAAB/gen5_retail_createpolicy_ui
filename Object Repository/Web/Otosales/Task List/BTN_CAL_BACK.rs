<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_CAL_BACK</name>
   <tag></tag>
   <elementGuidId>7aef8df5-a21e-4ab4-9804-a86b9b3476e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/button[@class=&quot;react-datepicker__navigation react-datepicker__navigation--previous&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/button[@class=&quot;react-datepicker__navigation react-datepicker__navigation--previous&quot;]</value>
   </webElementProperties>
</WebElementEntity>
