<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_GROSS_PREMIUM</name>
   <tag></tag>
   <elementGuidId>d5741219-b255-4f5e-b006-a5b33aa83b97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Gross Premium']/following-sibling::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Gross Premium']/following-sibling::span[1]</value>
   </webElementProperties>
</WebElementEntity>
