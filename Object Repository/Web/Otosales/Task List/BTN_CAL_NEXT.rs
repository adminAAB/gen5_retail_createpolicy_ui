<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_CAL_NEXT</name>
   <tag></tag>
   <elementGuidId>409517a6-eb83-472a-a5fc-4b3d7b8f2874</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/button[@class=&quot;react-datepicker__navigation react-datepicker__navigation--next&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/button[@class=&quot;react-datepicker__navigation react-datepicker__navigation--next&quot;]</value>
   </webElementProperties>
</WebElementEntity>
