<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_BIRTH_MONTH - month</name>
   <tag></tag>
   <elementGuidId>e0c148b5-0340-4697-aa6b-fbcea5e29c58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;personaldata&quot;]/div[2]/div[1]/div/div/div/div/div/div[2]/div[1]/div[@class=&quot;react-datepicker__current-month react-datepicker__current-month--hasYearDropdown&quot; and text()='${month}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;personaldata&quot;]/div[2]/div[1]/div/div/div/div/div/div[2]/div[1]/div[@class=&quot;react-datepicker__current-month react-datepicker__current-month--hasYearDropdown&quot; and text()='${month}']</value>
   </webElementProperties>
</WebElementEntity>
