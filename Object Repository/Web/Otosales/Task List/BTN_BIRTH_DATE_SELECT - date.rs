<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_BIRTH_DATE_SELECT - date</name>
   <tag></tag>
   <elementGuidId>7e488a0a-21f5-4367-ba9f-90c134232b58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,&quot;react-datepicker__day react-datepicker__day--${date}&quot;) and not(contains(@class,&quot;outside-month&quot;))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,&quot;react-datepicker__day react-datepicker__day--${date}&quot;) and not(contains(@class,&quot;outside-month&quot;))]</value>
   </webElementProperties>
</WebElementEntity>
