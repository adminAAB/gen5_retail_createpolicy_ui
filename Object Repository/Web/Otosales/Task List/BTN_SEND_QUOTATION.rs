<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_SEND_QUOTATION</name>
   <tag></tag>
   <elementGuidId>d2487f93-9a7b-4c4a-b2da-96cc42110105</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Send Quotation']/parent::div[1]/img[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Send Quotation']/parent::div[1]/img[1]</value>
   </webElementProperties>
</WebElementEntity>
