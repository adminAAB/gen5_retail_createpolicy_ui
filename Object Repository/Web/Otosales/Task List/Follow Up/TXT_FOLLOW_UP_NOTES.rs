<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_FOLLOW_UP_NOTES</name>
   <tag></tag>
   <elementGuidId>14c026ea-c4f5-4bb9-a600-353fd7c15b8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;FOLLOW UP NOTES&quot;)]/following-sibling::div[1]/div[1]/div[1]/div[1]/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;FOLLOW UP NOTES&quot;)]/following-sibling::div[1]/div[1]/div[1]/div[1]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
