<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_NAMA_CUSTOMER</name>
   <tag></tag>
   <elementGuidId>5d28d892-3564-42ab-889d-756cb23fe663</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Nama Customer']/following-sibling::div[1]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Nama Customer']/following-sibling::div[1]/input</value>
   </webElementProperties>
</WebElementEntity>
