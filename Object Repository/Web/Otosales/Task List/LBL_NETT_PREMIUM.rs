<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_NETT_PREMIUM</name>
   <tag></tag>
   <elementGuidId>32c1625a-55b5-49cb-8e1f-02224aaef21b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Net Premi']/following-sibling::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Net Premi']/following-sibling::span[1]</value>
   </webElementProperties>
</WebElementEntity>
