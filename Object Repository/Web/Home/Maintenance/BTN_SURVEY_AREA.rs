<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_SURVEY_AREA</name>
   <tag></tag>
   <elementGuidId>d56993dd-34b9-4f20-a451-e287e033852a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/button[text()='Survey Area'][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/button[text()='Survey Area']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
