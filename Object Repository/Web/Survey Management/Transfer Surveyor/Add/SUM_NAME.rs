<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SUM_NAME</name>
   <tag></tag>
   <elementGuidId>fa26e3fc-ffd3-4d6e-8a9b-e827577bc149</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;TransferSummaryDT&quot;]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${sumname}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;TransferSummaryDT&quot;]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${sumname}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
