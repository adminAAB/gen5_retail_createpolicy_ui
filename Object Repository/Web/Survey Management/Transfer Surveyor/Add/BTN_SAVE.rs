<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_SAVE</name>
   <tag></tag>
   <elementGuidId>6b1acbdd-7ca9-4f76-89f6-65e575440bec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[4]/div/a2is-buttons/div/div/button[1][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[4]/div/a2is-buttons/div/div/button[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
