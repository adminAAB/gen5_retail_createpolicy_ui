<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_DAY - NextMonth</name>
   <tag></tag>
   <elementGuidId>f0e7233b-a1b6-4be8-bb4e-375168d555ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[5]/div/div[1]/table/tbody/*/td[@class = &quot;day new&quot; and text() = &quot;${day}&quot;][count(. | //*[@ref_element = 'Object Repository/Web/FRAME']) = count(//*[@ref_element = 'Object Repository/Web/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[5]/div/div[1]/table/tbody/*/td[@class = &quot;day new&quot; and text() = &quot;${day}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
