<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_DEST_COORDINATOR</name>
   <tag></tag>
   <elementGuidId>5a505076-6733-486e-93ed-1bf953849468</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUTransferContent-0&quot;]/div[1]/div/a2is-combo-wide-dc[3]/div[2]/div[1]/div/div/button[text() = &quot;${destcoor}&quot;][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUTransferContent-0&quot;]/div[1]/div/a2is-combo-wide-dc[3]/div[2]/div[1]/div/div/button[text() = &quot;${destcoor}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
