<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_DEST_REGIONAL</name>
   <tag></tag>
   <elementGuidId>17d3c7d4-c615-43c3-b550-3a2a6020fc1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PUTransferContent-0&quot;]/div[1]/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[text() = &quot;${destreg}&quot;][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PUTransferContent-0&quot;]/div[1]/div/a2is-combo-wide-dc[2]/div[2]/div[1]/div/div/button[text() = &quot;${destreg}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
