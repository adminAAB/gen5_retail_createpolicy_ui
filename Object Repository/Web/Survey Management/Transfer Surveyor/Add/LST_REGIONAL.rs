<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_REGIONAL</name>
   <tag></tag>
   <elementGuidId>b14685e1-64ad-456c-8ceb-fe4d7fcc3984</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;TransferSurveyorDetailSearch-0&quot;]/div[1]/div/a2is-multi-wide-dc[1]/div[2]/div[1]/div/div/button[text() = &quot;${regional}&quot;][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;TransferSurveyorDetailSearch-0&quot;]/div[1]/div/a2is-multi-wide-dc[1]/div[2]/div[1]/div/div/button[text() = &quot;${regional}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
