<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_REGIONAL</name>
   <tag></tag>
   <elementGuidId>667215c5-32a5-4036-b294-b01966838552</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;MaintenanceKorwil-0&quot;]/div[1]/div/div[1]/div/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${region}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;MaintenanceKorwil-0&quot;]/div[1]/div/div[1]/div/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${region}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
