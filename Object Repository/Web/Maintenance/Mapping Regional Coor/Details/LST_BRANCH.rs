<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_BRANCH</name>
   <tag></tag>
   <elementGuidId>068e8204-c4e5-4550-97fc-7259e560114a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PanelListMaintenanceKorwilDetailBranch-0&quot;]/div[1]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${branch}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PanelListMaintenanceKorwilDetailBranch-0&quot;]/div[1]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${branch}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
