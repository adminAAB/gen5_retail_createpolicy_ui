<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_COOR_1</name>
   <tag></tag>
   <elementGuidId>469e09b8-62f9-4be8-8058-1f8c8931b76b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;MaintenanceCoordinator-0&quot;]/div[1]/div/div[1]/div[3]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;MaintenanceCoordinator-0&quot;]/div[1]/div/div[1]/div[3]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
