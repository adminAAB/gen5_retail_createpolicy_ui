<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_SURVEY_TIME</name>
   <tag></tag>
   <elementGuidId>2774d9a0-db0e-448c-b3f8-6edc267e2f54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;P1-0&quot;]/div[1]/div/div[3]/div/a2is-datatable/div[2]/div/table/tbody/*/td[6]/span[contains(text(),&quot;${surveytime}&quot;)][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;P1-0&quot;]/div[1]/div/div[3]/div/a2is-datatable/div[2]/div/table/tbody/*/td[6]/span[contains(text(),&quot;${surveytime}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
