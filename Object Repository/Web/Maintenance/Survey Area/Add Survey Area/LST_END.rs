<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_END</name>
   <tag></tag>
   <elementGuidId>b3b37cf1-3194-4d15-a711-6c46da9bfef1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;leftPanel&quot;]/div/div/div[4]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${end}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;leftPanel&quot;]/div/div/div[4]/a2is-datatable/div[2]/div/table/tbody/*/td[2]/span[contains(text(),&quot;${end}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
