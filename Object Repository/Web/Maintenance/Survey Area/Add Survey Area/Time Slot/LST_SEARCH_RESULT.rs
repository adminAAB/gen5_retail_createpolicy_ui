<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_SEARCH_RESULT</name>
   <tag></tag>
   <elementGuidId>43409565-ef87-4693-a471-30822d6c63a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/li[@class = &quot;select2-results__option select2-results__option--highlighted&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/li[@class = &quot;select2-results__option select2-results__option--highlighted&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
