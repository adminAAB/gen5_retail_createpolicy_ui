<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_COMPANY_TYPE_RESULT</name>
   <tag></tag>
   <elementGuidId>c54fcaf2-eadd-42f3-8ba9-cea6bcd532ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*/li[@class=&quot;select2-results__option select2-results__option--highlighted&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/li[@class=&quot;select2-results__option select2-results__option--highlighted&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
