<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EXP_COMPANY_GROUP</name>
   <tag></tag>
   <elementGuidId>b54739cf-4506-49e8-841e-8cf28e43e6dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CustCompany&quot;]/a2is-combo-searchable-wide-dc[1]/div[2]/div[1]/div/span/span[1]/span/span[1][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CustCompany&quot;]/a2is-combo-searchable-wide-dc[1]/div[2]/div[1]/div/span/span[1]/span/span[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
