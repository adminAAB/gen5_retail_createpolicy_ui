<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LST_SEARCH_BY_CHASSIS_NO</name>
   <tag></tag>
   <elementGuidId>13b8be6e-4e17-44d3-9f3e-fada4d1a68e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PGTaskList-0&quot;]/div[1]/div/div[1]/div[2]/a2is-combo/div/div/div/div/button[contains(text(),&quot;Chasis No&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PGTaskList-0&quot;]/div[1]/div/div[1]/div[2]/a2is-combo/div/div/div/div/button[contains(text(),&quot;Chasis No&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FRAME</value>
   </webElementProperties>
</WebElementEntity>
