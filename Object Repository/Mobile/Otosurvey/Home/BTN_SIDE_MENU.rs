<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_SIDE_MENU</name>
   <tag></tag>
   <elementGuidId>cca906af-0f7a-49c6-bd9f-a53f1454f4e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//android.widget.ImageView[@resource-id='android:id/up']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//android.widget.ImageView[@resource-id='android:id/up']</value>
   </webElementProperties>
</WebElementEntity>
