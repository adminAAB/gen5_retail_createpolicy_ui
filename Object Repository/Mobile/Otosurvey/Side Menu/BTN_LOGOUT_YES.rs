<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_LOGOUT_YES</name>
   <tag></tag>
   <elementGuidId>646b4329-20fb-4bd6-b978-1568b48ddf8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//android.widget.Button[@text='Yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//android.widget.Button[@text='Yes']</value>
   </webElementProperties>
</WebElementEntity>
