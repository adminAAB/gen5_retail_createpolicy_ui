<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_NAME</name>
   <tag></tag>
   <elementGuidId>8911aaec-7647-4aef-b70b-b2748ab9f6ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//android.widget.EditText[@resource-id='com.asuransiastra.mobilesurvey:id/etName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//android.widget.EditText[@resource-id='com.asuransiastra.mobilesurvey:id/etName']</value>
   </webElementProperties>
</WebElementEntity>
