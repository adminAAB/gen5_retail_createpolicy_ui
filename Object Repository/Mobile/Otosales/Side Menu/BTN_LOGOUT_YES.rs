<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_LOGOUT_YES</name>
   <tag></tag>
   <elementGuidId>50b3e8c4-4ed4-4a7b-854a-84d67e234f0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//android.widget.Button[@text='Yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//android.widget.Button[@text='Yes']</value>
   </webElementProperties>
</WebElementEntity>
