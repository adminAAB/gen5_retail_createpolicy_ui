<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_APPROVE</name>
   <tag></tag>
   <elementGuidId>e769006a-1fc9-4c5f-a38d-18d2603ae02a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//android.widget.LinearLayout[@resource-id='com.aab.mobilesalesnative:id/btnApprove']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//android.widget.LinearLayout[@resource-id='com.aab.mobilesalesnative:id/btnApprove']</value>
   </webElementProperties>
</WebElementEntity>
