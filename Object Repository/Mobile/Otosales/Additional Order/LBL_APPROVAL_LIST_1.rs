<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_APPROVAL_LIST_1</name>
   <tag></tag>
   <elementGuidId>bc7d756d-4d89-4f5c-8c4f-9b776cc60260</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//android.widget.TextView[@resource-id='com.aab.mobilesalesnative:id/tvCustomerName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//android.widget.TextView[@resource-id='com.aab.mobilesalesnative:id/tvCustomerName']</value>
   </webElementProperties>
</WebElementEntity>
