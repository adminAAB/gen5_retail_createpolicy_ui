<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cancel Policy</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e1956d0b-27b4-4315-8540-1a824034e57b</testSuiteGuid>
   <testCaseLink>
      <guid>108c0839-423b-4335-88de-9153983c5e0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Login/Login DOC Konvensional</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a232874-7395-49e2-b26c-c8bbb6de3ed4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Home/Select Print Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fa97f8f-025f-41d2-8640-1b08b90b12ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Policy/Print Policy For Cancellation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebc1f22d-c125-4f32-a914-67dda4cb09ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Home/Select Cancellation Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>055d243b-982f-406d-bd8f-e8da108345ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Cancelation Policy/Cancel Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a1fe41f-598f-4a2a-8482-9ad59663dc91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Approve Policy/Approve Cancel FAR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
