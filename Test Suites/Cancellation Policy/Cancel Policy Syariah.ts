<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cancel Policy Syariah</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>828e87be-3580-4039-a713-10e8761d9e7e</testSuiteGuid>
   <testCaseLink>
      <guid>48b6a61c-34f3-4193-b077-869e1316a57a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Login/Login DOC Konvensional</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c23f2fb-af90-4c76-9090-e1177724ce83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Home/Select Print Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76447411-4b4f-4fca-beba-a8f865df64fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Policy/Print Policy For Cancellation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0535f558-0033-46d3-bc8d-fa6122b1931a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Home/Select Cancellation Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bd23f74-36b4-43ca-8b0b-48a72461b95d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Cancelation Policy/Cancel Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1a6b09e-5387-4bab-bf9e-a053475987cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Approve Policy/Approve Cancel FAR syariah</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
