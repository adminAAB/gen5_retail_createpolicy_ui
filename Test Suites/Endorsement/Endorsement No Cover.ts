<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Endorsement No Cover</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7350a55e-f9f1-40fa-bf2f-8fc3769bfeb1</testSuiteGuid>
   <testCaseLink>
      <guid>d605ce74-8bcd-4852-a9e8-8de2f4b156bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Endorsement/Endorse No Cover</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71e14225-81d9-41c2-ad12-a91cfe218c38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Endorse Customer/Check NoCover</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f769025-cbb9-47e0-8450-e27c6ef225e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Endorsement Policy/Print Endorsement Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e9758d0-d652-4b60-b56e-c6ea15358fdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Check RPT Info NoCover</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
