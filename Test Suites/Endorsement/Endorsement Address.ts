<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Endorsement Address</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a350a2b6-5add-4ddd-81aa-5b0100cd2be1</testSuiteGuid>
   <testCaseLink>
      <guid>b1e0a022-eae0-40ea-905d-28ff9faa805a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Endorsement/Endorse Address</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5c75530-110b-4b09-9e16-d95b7a2a835f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Endorsement Policy/Print Endorsement Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dede5ad7-8205-4467-8e8c-008a5d13d710</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Endorse Customer/Check Address</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
