<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Endorsement Clause</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>76aa736d-df65-40a4-ad8b-9533c56af2d7</testSuiteGuid>
   <testCaseLink>
      <guid>1d7b0e3e-707a-4dad-91a8-534c5f835ec8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Endorsement/Endorse Clause</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c283258c-0b7a-473d-9285-58439f2c0a98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Endorse Customer/Check Object Clause</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adc198f3-12a1-4c53-a5c2-b7b3b68290cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Endorse Customer/Check Policy Clause</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94cba9d0-ddbd-42d8-afdd-fe21c9191554</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Endorsement Policy/Print Endorsement Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c0b34a1-d4fc-4d50-9096-b303e10f5b66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Check RPT Info Clause</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
