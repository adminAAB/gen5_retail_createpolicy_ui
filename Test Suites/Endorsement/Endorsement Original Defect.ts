<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Endorsement Original Defect</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5976b4be-3193-4049-bfa4-911ad70d130f</testSuiteGuid>
   <testCaseLink>
      <guid>a4b51394-6169-4dcf-9e98-19bfc265cd1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Endorsement/Endorse Original Defect</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06492f5e-c83a-4b06-8d5d-1852412bfd1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Endorse Customer/Check Original Defect</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c44b6f62-7d4e-48ee-a0d7-538a3d412d41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Endorsement Policy/Print Endorsement Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7410911-e5ee-4150-9cfb-d284b2bbbdae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Check RPT Info Original Defect</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
