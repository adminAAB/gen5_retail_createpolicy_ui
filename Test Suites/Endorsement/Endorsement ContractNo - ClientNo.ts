<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Endorsement ContractNo - ClientNo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>08cb1be6-05f6-4f9e-8893-9fe5329c892d</testSuiteGuid>
   <testCaseLink>
      <guid>a7743a72-00b3-4bde-a288-91dbbc1902c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Endorsement/Endorse ContractNo - ClientNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0a1a901-cf02-4da0-ae52-854b0dffa108</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Endorse Customer/Check ContractNo ClientNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a2767a5-0e17-40df-8b50-2852799e1b50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Screen/Web/Acquisition/Print Endorsement Policy/Print Endorsement Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c3764f5-9ef0-4479-b6ab-394d7c85a7af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Validation/Endorsement/Check RPT Info Contract Client</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
