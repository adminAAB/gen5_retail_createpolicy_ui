<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1 Region 1 Coordiantor</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2db04fe8-788d-44ed-9dc7-e553960f77d1</testSuiteGuid>
   <testCaseLink>
      <guid>b72cfb17-62c7-4b5e-a93f-bf8055202cba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 1 Coordinator/Coordinator x Regional Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>261dd5ab-84cf-4be2-a1ba-0787ddec7c6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 1 Coordinator/Surveyor x Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aacaa371-99b4-4cad-ac96-3a788b4377cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 1 Coordinator/Surveyor x Inactive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c459a78c-3550-41b1-8256-d148df447460</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 1 Coordinator/Surveyor x Regional Coordinator</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
