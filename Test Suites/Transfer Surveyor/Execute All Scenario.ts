<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Execute All Scenario</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d4c1f722-560d-485c-bf2e-7f36dea134b7</testSuiteGuid>
   <testCaseLink>
      <guid>b1decfa7-c833-4f74-9dbe-f0da2b098953</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/Regional BTE - Coordinator 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94d51bf6-f5bc-4f91-b3f4-4f80ed36f5e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/Regional BTE - Coordinator 1 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d12012fe-a676-43a9-9bf0-e36c1956d806</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/Regional BTE 1 2 - Coordinator 1 2</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
