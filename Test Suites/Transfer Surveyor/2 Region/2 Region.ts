<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2 Region</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b938d585-f992-4339-a58a-b0ec8d352615</testSuiteGuid>
   <testCaseLink>
      <guid>3b52233e-1421-4e5b-9e8b-191ea3d2b53e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Coordinator x Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>229d9e7e-5ac1-4581-ae21-7ae13764281d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Coordinator x Surveyor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b80145bb-b052-49e8-9103-ad829b1d8a78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Reg Coor x Reg Coor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da40d295-ddce-4a5e-b054-e96683299daa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Regional x Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54fd390d-e428-4325-9693-2e3ff5859d00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Regional x Regional</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea8d7d55-ea20-431d-8640-dee97b828293</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Regional x Surveyor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdf3f41a-f545-4eae-94b9-13d1b609ea1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/2 Region/Surveyor x Surveyor</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
