<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1 Region 2 Coordinator</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>21f16b14-df26-456c-b6c2-f82455797b78</testSuiteGuid>
   <testCaseLink>
      <guid>354942e5-d852-49a7-a1f2-870cf93a889e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 2 Coordinator/Coordinator x Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>173c8085-4356-417c-84b7-75ccb136cf93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 2 Coordinator/RegCoor to Coor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df45f492-e6a5-4022-8ea5-c4d9f0653273</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 2 Coordinator/RegCoor to Cross Coor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc73989d-406a-4d07-84d1-06c48bf6bb0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 2 Coordinator/RegCoor to Reg</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a2f87e9-0c46-4fca-9528-abc0b4e3ec61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 2 Coordinator/Surveyor x Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87e22d0f-76c7-4718-b282-946ebba74d2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Survey Management/Transfer Surveyor/1 Region 2 Coordinator/Surveyor x Surveyor</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
