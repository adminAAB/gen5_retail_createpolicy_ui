<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Execute All Scenario</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f1edca38-40bb-4967-9a2d-546a3a62da8d</testSuiteGuid>
   <testCaseLink>
      <guid>73f137f3-b12e-41f2-a0e0-663388ad34fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance/Customer Category</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca973975-d5bf-48f5-a0dd-63a28f204f57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance/Customer Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00b984ac-42e1-438e-bb1a-cb0123e8ff2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance/Mapping Branch Area</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5466269-9c15-42b8-bc0d-63227a06e336</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance/Mapping Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1236cab-e182-4b5f-b74b-75a27ae80681</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance/Mapping Regional Coordinator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ddce1e3-3ba4-4905-afbc-0ab61c90ae2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance/Survey Area</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
