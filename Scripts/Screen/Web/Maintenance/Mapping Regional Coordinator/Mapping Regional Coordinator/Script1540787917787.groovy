import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.exception.StepErrorException as StepErrorException

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/BTN_ADD'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/TXT_REGIONAL'), 'REGION TEST')

WebUI.delay(1)

def regional = WebUI.getAttribute(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/TXT_REGIONAL'), 'value')

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SEARCH_REG_COOR'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Reg Coor/TXT_SEARCH_INPUT'), 'kkk')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Reg Coor/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Reg Coor/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Reg Coor/BTN_SELECT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_CREATE_BRANCH'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/TXT_SEARCH_INPUT'), '888')

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/BTN_SELECT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_CREATE_SURVEYOR'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/TXT_SEARCH_INPUT'), 'ZUL')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/BTN_SEARCH'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/BTN_SELECT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SAVE_YES'))

WebUI.delay(7)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SAVE_OK'))

WebUI.delay(3)

///// CHECK CREATE
WebDriver driver = DriverFactory.getWebDriver()

WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)

WebElement Table = driver.findElement(By.xpath('//*[@id="MaintenanceKorwil-0"]/div[1]/div/div[1]/div/a2is-datatable/div[2]/div/table/tbody'))

List<WebElement> rows = Table.findElements(By.tagName('tr'))

WebUI.delay(1)

def rowcount = rows.size()
def regcoor = ''
def branch = ''
def surveyor = ''

for (int x = 0; x < rowcount; x++) {
	List<WebElement> Cols = rows.get(x).findElements(By.tagName('td'))

	if (Cols.get(1).getText().trim().equalsIgnoreCase(regional)) {
		regcoor = Cols.get(2).getText().trim()
		branch = Cols.get(3).getText().trim()
		surveyor = Cols.get(4).getText().trim()
		
		if (regcoor != 'KKK - Kelly Kharisman Kaprawi'){
			CustomKeywords.'forceStop.throwError.STOP'()
		}
		
		if (branch != 'CALL CENTER AREA 1'){
			CustomKeywords.'forceStop.throwError.STOP'()
		}
		
		if (surveyor != 'ZUL'){
			CustomKeywords.'forceStop.throwError.STOP'()
		}
	}
}

WebUI.delay(3)

WebUI.switchToDefaultContent()

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/LST_REGIONAL',[('region') : 'REGION TEST']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/BTN_EDIT'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/TXT_REGIONAL'), 'REGION EDIT')

WebUI.delay(1)

def regionalx = WebUI.getAttribute(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/TXT_REGIONAL'), 'value')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/LST_BRANCH',[('branch'):'888']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_DELETE_BRANCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_DELETE_YES'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_CREATE_BRANCH'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/TXT_SEARCH_INPUT'), '900')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Branch/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_CREATE_SURVEYOR'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/TXT_SEARCH_INPUT'), 'ZUT')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/Search Surveyor/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SAVE_YES'))

WebUI.delay(7)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/Details/BTN_SAVE_OK'))

WebUI.delay(3)

///// CHECK EDIT
WebDriver driverx = DriverFactory.getWebDriver()

WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)

WebElement Tablex = driverx.findElement(By.xpath('//*[@id="MaintenanceKorwil-0"]/div[1]/div/div[1]/div/a2is-datatable/div[2]/div/table/tbody'))

List<WebElement> rowsx = Tablex.findElements(By.tagName('tr'))

WebUI.delay(1)

def rowcountx = rowsx.size()
def regcoorx = ''
def branchx = ''
def surveyorx = ''

for (int y = 0; y < rowcountx; y++) {
	List<WebElement> Colsx = rowsx.get(y).findElements(By.tagName('td'))

	if (Colsx.get(1).getText().trim().equalsIgnoreCase(regionalx)) {
		regcoorx = Colsx.get(2).getText().trim()
		branchx = Colsx.get(3).getText().trim()
		surveyorx = Colsx.get(4).getText().trim()
		
		if (regcoorx != 'KKK - Kelly Kharisman Kaprawi'){
			CustomKeywords.'forceStop.throwError.STOP'()
		}
		
		if (branchx != 'INVESTMENT'){
			CustomKeywords.'forceStop.throwError.STOP'()
		}
		
		if (surveyorx != 'ZUL, ZUT'){
			CustomKeywords.'forceStop.throwError.STOP'()
		}
	}
}

WebUI.delay(3)

WebUI.switchToDefaultContent()

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Mapping Regional Coor/LST_REGIONAL',[('region') : 'REGION EDIT']), 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/BTN_DELETE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Regional Coor/BTN_DELETE_OK'))

WebUI.delay(3)
















