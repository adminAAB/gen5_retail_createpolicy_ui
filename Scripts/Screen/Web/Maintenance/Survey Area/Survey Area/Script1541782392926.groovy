import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/EXP_SEARCH_BY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/LST_SEARCH_BY_BRANCH'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/TXT_SEARCH_BY'), 'puri')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/BTN_ADD'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/TXT_AREA_NAME'), 'areaname')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/TXT_ALIAS'), 'alias')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/EXP_BRANCH'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/TXT_SEARCH_BRANCH'), 'puri')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_ADD_TIME'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/EXP_START'))

WebUI.delay(1)

///// ADD TIME
WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_SEARCH_TIME'), '04.00')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/LST_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/EXP_END'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_SEARCH_TIME'), '06.00')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/LST_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_QUOTA'), '10')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_ADD_TIME'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/EXP_START'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_SEARCH_TIME'), '06.00')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/LST_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/EXP_END'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_SEARCH_TIME'), '08.00')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/LST_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_QUOTA'), '11')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/BTN_ADD'))

WebUI.delay(3)

///// CHECK TIME
WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_START', [('start') : '04.00']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_START', [('start') : '06.00']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_END', [('end') : '06.00']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_END', [('end') : '08.00']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_QUOTA', [('quota') : '10']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_QUOTA', [('quota') : '11']), 
    3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_QUOTA', [('quota') : '11']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_DELETE_TIME'))

WebUI.delay(1)

WebUI.verifyElementNotPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_QUOTA', [('quota') : '11']), 
    3)

WebUI.delay(1)

///// ADD POSTAL CODE
WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_ADD_POSTAL'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/TXT_SEARCH_INPUT'), 
    '35214')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_ADD_POSTAL'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/TXT_SEARCH_INPUT'), 
    '35116')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/BTN_SELECT'))

WebUI.delay(1)

///// CHECK POSTAL CODE
WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_POSTAL_CODE', [
            ('postalcode') : '35214']), 3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_POSTAL_CODE', [
            ('postalcode') : '35116']), 3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_DESCRIPTION', [
            ('description') : '35214']), 3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_DESCRIPTION', [
            ('description') : '35116']), 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_DESCRIPTION', [('description') : '35116']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_DELETE_POSTAL'))

WebUI.delay(1)

WebUI.verifyElementNotPresent(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_DESCRIPTION', 
        [('description') : '35116']), 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/CHK_IS_ACTIVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_SAVE'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_SAVE_X'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_AREA_NAME', [('areaname') : 'areaname']), 
    3)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Web/Maintenance/Survey Area/LST_AREA_NAME', [('areaname') : 'areaname']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_AREA_ALIAS', [('areaalias') : 'alias']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_POSTAL_CODE', [('postalcode') : '35214']), 
    3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/LST_POSTAL_CODE', [('postalcode') : '35214']))

WebUI.delay(1)

///// EDIT SURVEY AREA
WebUI.click(findTestObject('Web/Maintenance/Survey Area/BTN_EDIT'))

WebUI.delay(5)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/TXT_AREA_NAME'), 'areanamexx')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/TXT_ALIAS'), 'aliasxx')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_ADD_TIME'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/EXP_START'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_SEARCH_TIME'), '07.00')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/LST_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/EXP_END'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_SEARCH_TIME'), '08.00')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/LST_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/TXT_QUOTA'), '5')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Time Slot/BTN_ADD'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/LST_QUOTA',[('quota'):'10']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_DELETE_TIME'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_ADD_POSTAL'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/TXT_SEARCH_INPUT'), 
    '35116')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/Postal Code/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_SAVE'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/Add Survey Area/BTN_SAVE_X'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Web/Maintenance/Survey Area/LST_AREA_NAME', [('areaname') : 'areanamexx']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_AREA_NAME', [('areaname') : 'areanamexx']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_AREA_ALIAS', [('areaalias') : 'aliasxx']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_POSTAL_CODE', [('postalcode') : '35116']), 
    3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Survey Area/LST_SURVEY_TIME', [('surveytime') : '07:00']), 
    3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/LST_AREA_ALIAS', [('areaalias') : 'aliasxx']))

WebUI.delay(1)

///// DELETE SURVEY AREA
WebUI.click(findTestObject('Web/Maintenance/Survey Area/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Survey Area/BTN_DELETE_X'))

WebUI.delay(1)

WebUI.verifyElementNotPresent(findTestObject('Web/Maintenance/Survey Area/LST_AREA_ALIAS', [('areaalias') : 'aliasxx']), 
    3)

WebUI.delay(1)

