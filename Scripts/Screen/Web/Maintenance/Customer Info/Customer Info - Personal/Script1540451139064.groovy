import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def personalcode = '163546163'

///// PERSONAL CUSTOMER
WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/EXP_CUSTOMER_TYPE'))

//WebUI.selectOptionByValue(findTestObject('Object Repository/Web/Maintenance/Customer Info/EXP_CUSTOMER_TYPE'), 'personal', true)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_PERSONAL'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/TXT_CUSTOMER_CODE'), personalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/BTN_SEARCH'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_CODE_RESULT'), personalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_CODE_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/BTN_EDIT'))

WebUI.delay(5)

///// GET EXISTING PERSONAL INFO
def customername = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_CUSTOMER_NAME'), 
    'value')

def idtype = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_ID_TYPE'), 'title')

def idnumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ID_NUMBER'), 'value')

def birthdate = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_BIRTHDATE'), 'value')

def gender = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_GENDER'), 'title')

def religion = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_RELIGION'), 'title')

def marital = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_MARITAL'), 'title')

def address = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ADDRESS'), 'value')

def postal = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_POSTAL_CODE'), 'value')

//def homenumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_HOME_NUMBER'), 'value')

def mobilenumber1 = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_1'), 
    'value')

def mobilenumber2 = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_2'), 
    'value')

def mobilenumber3 = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_3'), 
    'value')

def mobilenumber4 = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_4'), 
    'value')

def email = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EMAIL'), 'value').toUpperCase()

def facebook = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_FACEBOOK'), 'value')

def twitter = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_TWITTER'), 'value')

def instagram = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_INSTAGRAM'), 'value')

def job = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_JOB_TITLE'), 'title')

def companyname = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_COMPANY_NAME'), 
    'value')

def officeaddress = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_ADDRESS'), 
    'value')

def officepostal = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_POSTAL_CODE'), 
    'value')

def officenumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_NUMBER'), 
    'value')

def ext = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EXTENSION'), 'value')

def income = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_INCOME'), 'title')

def bday = birthdate.split('/')[0]

def postalcode = postal.split(' ')[0]

def officepostalcode = officepostal.split(' ')[0]

///// EDIT PERSONAL INFO
WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_CUSTOMER_NAME'), 'pak tarno katalon')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_ID_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_ID_TYPE', [('idtype') : 'KITAS']))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ID_NUMBER'), '98888888888888')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_BIRTHDATE'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_DAY', [('day') : '15']))

WebUI.delay(1)

def birthdatex = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_BIRTHDATE'), 'value')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_GENDER'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_GENDER', [('gender') : 'FEMALE']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_RELIGION'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_RELIGION', [('religion') : 'ISLAM']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_MARITAL'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_MARITAL', [('marital') : 'SINGLE']))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ADDRESS'), 'addressku')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/TXT_SEARCH_INPUT'), '10620')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/BTN_SELECT'))

WebUI.delay(1)

def postalx = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_POSTAL_CODE'), 'value')

WebUI.delay(1)

//WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_HOME_NUMBER'), '021021021')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_1'), '08180808081')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_2'), '08180808082')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_3'), '08180808083')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_4'), '08180808084')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EMAIL'), '')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_FACEBOOK'), 'facebook@mail.com')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_TWITTER'), 'twitter@mail.com')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_INSTAGRAM'), 'instagram@mail.com')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_JOB_TITLE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_JOB_SEARCH'), 'pelajar')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_JOB_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_COMPANY_NAME'), 'companyxxx')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_ADDRESS'), 'officeaddrxxx')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_OFFICE_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/TXT_SEARCH_INPUT'), '10620')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/BTN_SELECT'))

WebUI.delay(1)

def officepostalx = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_POSTAL_CODE'), 
    'value')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_NUMBER'), '0218989898989')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EXTENSION'), '333')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_INCOME'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_INCOME', [('income') : 'RP 15 JUTA']))

WebUI.delay(1)

def incomex = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_INCOME'), 'title')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_SAVE'))

WebUI.delay(15)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Maintenance/Customer Info/Personal Detail/BTN_PopUp_Validation'), 
	'Invalid value in one (or more) field')

WebUI.delay (2)

WebUI.click(findTestObject('Object Repository/Web/Maintenance/Customer Info/Personal Detail/BTN_PopUp_Validation_OK'))

WebUI.delay (2)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EMAIL'), 'EHU@BEYOND.ASURANSI.ASTRA.CO.ID')

WebUI.delay (2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_SAVE'))

WebUI.delay (2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_SAVE_OK'))

WebUI.delay(7)



///// CHECK AFTER EDIT CUSTOMER & ROLLBACK CUSTOMER INFO
WebUI.click(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_CODE_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/BTN_EDIT'))

WebUI.delay(5)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_CUSTOMER_NAME'), 'value', 
    'PAK TARNO KATALON', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_CUSTOMER_NAME'), customername)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_ID_TYPE'), 'title', 
    'KITAS/KITAP', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_ID_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_ID_TYPE', [('idtype') : idtype]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ID_NUMBER'), 'value', 
    '98888888888888', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ID_NUMBER'), idnumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_BIRTHDATE'), 'value', 
    birthdatex, 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_BIRTHDATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_DAY', [('day') : bday]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_GENDER'), 'title', 
    'FEMALE', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_GENDER'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_GENDER', [('gender') : gender]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_RELIGION'), 'title', 
    'ISLAM', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_RELIGION'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_RELIGION', [('religion') : religion]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_MARITAL'), 'title', 
    'SINGLE', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_MARITAL'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_MARITAL', [('marital') : marital]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ADDRESS'), 'value', 
    'ADDRESSKU', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_ADDRESS'), address)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_POSTAL_CODE'), 'value', 
    postalx, 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/TXT_SEARCH_INPUT'), postalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Postal Code/BTN_SELECT'))

WebUI.delay(1)

//WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_HOME_NUMBER'), 'value', 
    //'021021021', 3)

WebUI.delay(1)

//WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_HOME_NUMBER'), homenumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_1'), 
    'value', '08180808081', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_1'), mobilenumber1)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_2'), 
    'value', '08180808082', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_2'), mobilenumber2)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_3'), 
    'value', '08180808083', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_3'), mobilenumber3)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_4'), 
    'value', '08180808084', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_MOBILE_NUMBER_4'), mobilenumber4)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EMAIL'), 'value', 'EHU@BEYOND.ASURANSI.ASTRA.CO.ID', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EMAIL'), email)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_FACEBOOK'), 'value', 
    'FACEBOOK@MAIL.COM', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_FACEBOOK'), facebook)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_TWITTER'), 'value', 
    'TWITTER@MAIL.COM', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_TWITTER'), twitter)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_INSTAGRAM'), 'value', 
    'INSTAGRAM@MAIL.COM', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_INSTAGRAM'), instagram)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_JOB_TITLE'), 'title', 
    'PELAJAR', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_JOB_TITLE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_JOB_SEARCH'), job)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_JOB_SEARCH_RESULT'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_COMPANY_NAME'), 'value', 
    'COMPANYXXX', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_COMPANY_NAME'), companyname)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_ADDRESS'), 
    'value', 'OFFICEADDRXXX', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_ADDRESS'), officeaddress)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_POSTAL_CODE'), 
    'value', officepostalx, 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_OFFICE_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/TXT_SEARCH_INPUT'), officepostalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/Office Postal Code/BTN_SELECT'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_NUMBER'), 'value', 
    '0218989898989', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_OFFICE_NUMBER'), officenumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EXTENSION'), 'value', 
    '333', 3)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Personal Detail/TXT_EXTENSION'), ext)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_INCOME'), 'title', 
    incomex, 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/EXP_INCOME'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/LST_INCOME', [('income') : income]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_SAVE'))

WebUI.delay(15)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Personal Detail/BTN_SAVE_OK'))

WebUI.delay(7)

