import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def companycode = '163499659'

///// COMPANY CUSTOMER
WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/EXP_CUSTOMER_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_COMPANY'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/TXT_CUSTOMER_CODE'), companycode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/BTN_SEARCH'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_CODE_RESULT'), companycode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_CODE_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/BTN_EDIT'))

WebUI.delay(5)

///// GET EXISTING COMPANY INFO
def companyname = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_NAME'), "value")
def companygroup = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_GROUP'), "title")
def companytype = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_TYPE'), "title")
def npwpname = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NAME'), "value")
def npwpnumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NUMBER'), "value")
def npwpdate = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_DATE'), "value")
def npwpaddress = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_ADDRESS'), "value")
def siupnumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_SIUP_NUMBER'), "value")
def pkpnumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_NUMBER'), "value")
def pkpdate = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_DATE'), "value")
def officeaddress = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_ADDRESS'), "value")
def officepostal = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_POSTAL_CODE'), "value")
def officenumber = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_NUMBER'), "value")
def ext = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EXTENSION'), "value")
def picname = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PIC_NAME'), "value")
def mobilenumber1 = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_1'), "value")
def mobilenumber2 = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_2'), "value")
def email = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EMAIL'), "value")

def officepostalcode = officepostal.split(' ')[0]
def npwpday = npwpdate.split('/')[0]
def pkpday = pkpdate.split('/')[0]

println(companygroup)
println(companytype)

WebUI.delay(3)

///// EDIT COMPANY INFO
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_NAME'), 'PT.MENCARI KATALON')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_GROUP'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_GROUP_SEARCH'), 'FIF')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/LST_COMPANY_GROUP_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_TYPE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_TYPE_SEARCH'), 'BROKER')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/LST_COMPANY_TYPE_RESULT'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NAME'), 'UDIN KATALON')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NUMBER'), '123123123123123')

WebUI.delay(1)

def npwpnumberx = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NUMBER'), "value")

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_NPWP_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_NPWP_DAY',[('npwpday'):'15']))

WebUI.delay(1)

def npwpdatex = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_DATE'), "value")

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_ADDRESS'), 'NPWP ADDRESS XXX')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_SIUP_NUMBER'), '12345678910')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_NUMBER'), '10987654321')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_PKP_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_PKP_DAY',[('pkpday'):'15']))

WebUI.delay(1)

def pkpdatex = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_DATE'), "value")

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_ADDRESS'), 'OFFICE ADDRESS XXX')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_OFFICE_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/TXT_SEARCH_INPUT'), '10620')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/BTN_SELECT'))

WebUI.delay(1)

def companypostalx = WebUI.getAttribute(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_POSTAL_CODE'), "value")

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_NUMBER'), '02188877733')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EXTENSION'), '339')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PIC_NAME'), 'TEJO')

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_1'), '081231231231')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_2'), '081231231232')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EMAIL'), '')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_SAVE'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Maintenance/Customer Info/Personal Detail/BTN_PopUp_Validation'),
	'Invalid value in one (or more) field')

WebUI.delay (2)

WebUI.click(findTestObject('Object Repository/Web/Maintenance/Customer Info/Personal Detail/BTN_PopUp_Validation_OK'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EMAIL'), 'EHU@BEYOND.ASURANSIASTRA.COM')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Maintenance/Customer Info/Company Detail/BTN_SAVE'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_SAVE_OK'))

WebUI.delay(15)

///// CHECK AFTER EDIT COMPANY INFO & ROLLBACK

WebUI.click(findTestObject('Web/Maintenance/Customer Info/LST_CUSTOMER_CODE_RESULT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/BTN_EDIT'))

WebUI.delay(5)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_NAME'), 'value', 
	'PT.MENCARI KATALON', 3)

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_NAME'), companyname)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_GROUP'), "title", 'FIF', 3)
WebUI.delay(1)
WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_GROUP'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_GROUP_SEARCH'), companygroup)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/LST_COMPANY_GROUP_RESULT'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_TYPE'), "title", 'BROKER', 3)
WebUI.delay(1)
WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/EXP_COMPANY_TYPE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_COMPANY_TYPE_SEARCH'), companytype)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/LST_COMPANY_TYPE_RESULT'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NAME'), 'value', 'PT.MENCARI KATALON', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NAME'), npwpname)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NUMBER'), 'value', npwpnumberx, 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_NUMBER'), npwpnumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_DATE'), 'value', npwpdatex, 3)
WebUI.delay(1)
WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_NPWP_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_NPWP_DAY',[('npwpday'):npwpday]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_ADDRESS'), 'value', 'NPWP ADDRESS XXX', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_NPWP_ADDRESS'), npwpaddress)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_SIUP_NUMBER'), 'value', '12345678910', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_SIUP_NUMBER'), siupnumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_NUMBER'), 'value', '10987654321', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_NUMBER'), pkpnumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PKP_DATE'), 'value', pkpdatex, 3)
WebUI.delay(1)
WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_PKP_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_PKP_DAY',[('pkpday'):pkpday]))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_ADDRESS'), 'value', 'NPWP ADDRESS XXX', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_ADDRESS'), officeaddress)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_POSTAL_CODE'), 'value', companypostalx, 3)
WebUI.delay(1)
WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_OFFICE_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/TXT_SEARCH_INPUT'), officepostalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/BTN_SEARCH'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/Office Postal Code/BTN_SELECT'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_NUMBER'), 'value', '02188877733', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_OFFICE_NUMBER'), officenumber)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EXTENSION'), 'value', '339', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EXTENSION'), ext)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_1'), 'value', '081231231231', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_1'), mobilenumber1)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_2'), 'value', '081231231232', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_MOBILE_NUMBER_2'), mobilenumber2)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EMAIL'), 'value', 'EHU@BEYOND.ASURANSIASTRA.COM', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_EMAIL'), email)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PIC_NAME'), 'value', 'TEJO', 3)
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Maintenance/Customer Info/Company Detail/TXT_PIC_NAME'), 'PICXXX')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_SAVE'))

WebUI.delay(15)

WebUI.click(findTestObject('Web/Maintenance/Customer Info/Company Detail/BTN_SAVE_OK'))

WebUI.delay(5)










