import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def regional = 'Kelapa Gading'
def branch = 'Kelapa Gading'
def areatype = 'Garda Center'
def areaname = 'Astra Biz Center BSD'
def alias = 'alias'
def editalias = 'editalias'

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/EXP_SEARCH_BY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/LST_SEARCH_BY',[('regional'):regional]))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/LST_BRANCH',[('branch'):branch]))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/BTN_ADD'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/EXP_AREA_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/LST_AREA_TYPE',[('areatype'):areatype]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/BTN_AREA_NAME'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/Search Area/TXT_SEARCH_INPUT'), areaname)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/Search Area/BTN_SEARCH'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/Search Area/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/Search Area/BTN_SELECT'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/TXT_ALIAS'), alias)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Add Area/BTN_SAVE_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_TYPE',[('areatype'):areatype]), 3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_NAME',[('areaname'):areaname]), 3)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_ALIAS',[('alias'):alias]), 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_NAME',[('areaname'):areaname]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/BTN_EDIT'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Branch Area/Edit Area/TXT_ALIAS'), editalias)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Edit Area/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Edit Area/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/Edit Area/BTN_SAVE_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_ALIAS',[('alias'):editalias]), 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_NAME',[('areaname'):areaname]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/BTN_DELETE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Branch Area/BTN_DELETE_OK'))

WebUI.delay(3)

WebUI.verifyElementNotPresent(findTestObject('Web/Maintenance/Mapping Branch Area/LST_AREA_ALIAS',[('alias'):editalias]), 3)

WebUI.delay(3)