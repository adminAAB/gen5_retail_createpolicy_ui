import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/EXP_SEARCH_BY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/LST_SEARCH_BY',[('searchby'):'MAPPING']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/BTN_X'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/BTN_ADD'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Coordinator/Details/TXT_COORDINATOR'), 'ADD COORDINATOR')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SEARCH_PIC'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search PIC Coor/TXT_SEARCH_INPUT'), 'AYI')

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search PIC Coor/BTN_SEARCH'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search PIC Coor/LST_SEARCH_RESULT_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search PIC Coor/BTN_SELECT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_ADD_BRANCH'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Branch/TXT_SEARCH_INPUT'), '009')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Branch/BTN_SEARCH'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Branch/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Branch/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_ADD_SURVEYOR'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Surveyor/TXT_SEARCH_INPUT'), 'BAA')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Surveyor/BTN_SEARCH'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Surveyor/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Surveyor/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SAVE'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SAVE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SAVE_OK'))

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_COOR_1'),'ADD COORDINATOR')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_PIC_NAME_1'), 'AYI - Adi Yulianto')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_BRANCH_1'), 'JAKARTA BARAT')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_SURVEYOR_1'), 'Boy Amardin')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/LST_ROW_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/BTN_EDIT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_ADD_BRANCH'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Branch/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Branch/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_ADD_SURVEYOR'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Surveyor/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/Search Surveyor/BTN_SELECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SAVE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/Details/BTN_SAVE_OK'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_COOR_1'), 'ADD COORDINATOR')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_PIC_NAME_1'), 'AYI - Adi Yulianto')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_BRANCH_1'), 'JAKARTA BARAT, JAKARTA (011)')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Mapping Coordinator/LST_SURVEYOR_1'), 'Boy Amardin, Bahtiar Adi Cahyadi')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/BTN_DELETE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Maintenance/Mapping Coordinator/BTN_X'))

WebUI.delay(3)


