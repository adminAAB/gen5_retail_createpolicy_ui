import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def companycode = '155624335'

def personalcode = '02638324'


///// COMPANY
WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/EXP_CUSTOMER_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LST_CUSTOMER_COMPANY'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Category/TXT_CUSTOMER_CODE'), companycode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SEARCH'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Category/LBL_CUSTOMER_CODE_RESULT'), companycode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LBL_CUSTOMER_CODE_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_UPDATE_CUSTOMER'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/LST_CATEGORY_BLACKLISTED'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), 'blacklist')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/BTN_SAVE'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SAVE_X'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Category/LBL_COMPANY_CATEGORY_RESULT'), 'Blacklisted')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LBL_COMPANY_CATEGORY_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_UPDATE_CUSTOMER'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), "value", 'blacklist', 2)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/LST_CATEGORY_VIP'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), 'VIP')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SAVE_X'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Category/LBL_COMPANY_CATEGORY_RESULT'), 'VIP')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LBL_COMPANY_CATEGORY_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_UPDATE_CUSTOMER'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), "value", 'VIP', 2)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/LST_CATEGORY_REGULAR'))

WebUI.delay(1)

WebUI.clearText(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SAVE_X'))


///// PERSONAL
WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/EXP_CUSTOMER_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LST_CUSTOMER_PERSONAL'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Category/TXT_CUSTOMER_CODE'), personalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SEARCH'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Category/LBL_CUSTOMER_CODE_RESULT'), personalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LBL_CUSTOMER_CODE_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_UPDATE_CUSTOMER'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/LST_CATEGORY_BLACKLISTED'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), 'blacklist')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SAVE_X'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Category/LBL_PERSONAL_CATEGORY_RESULT'), 'Blacklisted')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LBL_COMPANY_CATEGORY_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_UPDATE_CUSTOMER'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), "value", 'blacklist', 2)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/LST_CATEGORY_VIP'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), 'VIP')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SAVE_X'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Maintenance/Customer Category/LBL_PERSONAL_CATEGORY_RESULT'), 'VIP')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/LBL_COMPANY_CATEGORY_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_UPDATE_CUSTOMER'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'), "value", 'VIP', 2)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/LST_CATEGORY_REGULAR'))

WebUI.delay(1)

WebUI.clearText(findTestObject('Web/Maintenance/Customer Category/Update Customer/TXT_REMARKS'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/Update Customer/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_SAVE_X'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_CLOSE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Maintenance/Customer Category/BTN_CLOSE_YES'))

WebUI.delay(1)


