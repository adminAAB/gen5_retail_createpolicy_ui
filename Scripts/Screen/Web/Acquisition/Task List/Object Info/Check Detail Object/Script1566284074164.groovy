import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def vehiclecode = findTestData("Otosales/OtosalesParam").getValue(9, 1)
def usage = findTestData("Otosales/OtosalesParam").getValue(11, 1)
def region = findTestData("Otosales/OtosalesParam").getValue(12, 1)
def chassisno = findTestData("Otosales/OtosalesParam").getValue(14, 1)
def producttype = findTestData("Otosales/OtosalesParam").getValue(15, 1)
def productcode = findTestData("Otosales/OtosalesParam").getValue(16, 1)

def totalpremi = findTestData("Otosales/OtosalesParam").getValue(36, 1).replace(',', '.')+',00'
def nettpremi = findTestData("Otosales/OtosalesParam").getValue(92, 1)
def adminfee = '50.000,00'

//KENDARAAN
def compre1 = findTestData("Otosales/OtosalesParam").getValue(39, 1)
def compre2 = findTestData("Otosales/OtosalesParam").getValue(40, 1)
def compre3 = findTestData("Otosales/OtosalesParam").getValue(41, 1)
def tlo1 = findTestData("Otosales/OtosalesParam").getValue(42, 1)
def tlo2 = findTestData("Otosales/OtosalesParam").getValue(43, 1)
def etv1 = findTestData("Otosales/OtosalesParam").getValue(44, 1)
def etv2 = findTestData("Otosales/OtosalesParam").getValue(45, 1)
def etv3 = findTestData("Otosales/OtosalesParam").getValue(46, 1)
def etv4 = findTestData("Otosales/OtosalesParam").getValue(47, 1)
def etv5 = findTestData("Otosales/OtosalesParam").getValue(48, 1)
def fld1 = findTestData("Otosales/OtosalesParam").getValue(49, 1)
def fld2 = findTestData("Otosales/OtosalesParam").getValue(50, 1)
def fld3 = findTestData("Otosales/OtosalesParam").getValue(51, 1)
def fld4 = findTestData("Otosales/OtosalesParam").getValue(52, 1)
def fld5 = findTestData("Otosales/OtosalesParam").getValue(53, 1)
def srcc1 = findTestData("Otosales/OtosalesParam").getValue(54, 1)
def srcc2 = findTestData("Otosales/OtosalesParam").getValue(55, 1)
def srcc3 = findTestData("Otosales/OtosalesParam").getValue(56, 1)
def srcc4 = findTestData("Otosales/OtosalesParam").getValue(57, 1)
def srcc5 = findTestData("Otosales/OtosalesParam").getValue(58, 1)
def ts1 = findTestData("Otosales/OtosalesParam").getValue(59, 1)
def ts2 = findTestData("Otosales/OtosalesParam").getValue(60, 1)
def ts3 = findTestData("Otosales/OtosalesParam").getValue(61, 1)
def ts4 = findTestData("Otosales/OtosalesParam").getValue(62, 1)
def ts5 = findTestData("Otosales/OtosalesParam").getValue(63, 1)

def padpremi = findTestData("Otosales/OtosalesParam").getValue(64, 1)
def pappremi = findTestData("Otosales/OtosalesParam").getValue(65, 1)
def tplpremi = findTestData("Otosales/OtosalesParam").getValue(66, 1)

//ACCESSORIES
def acccompre1 = findTestData("Otosales/OtosalesParam").getValue(67, 1)
def acccompre2 = findTestData("Otosales/OtosalesParam").getValue(68, 1)
def acccompre3 = findTestData("Otosales/OtosalesParam").getValue(69, 1)
def acctlo1 = findTestData("Otosales/OtosalesParam").getValue(70, 1)
def acctlo2 = findTestData("Otosales/OtosalesParam").getValue(71, 1)
def accetv1 = findTestData("Otosales/OtosalesParam").getValue(72, 1)
def accetv2 = findTestData("Otosales/OtosalesParam").getValue(73, 1)
def accetv3 = findTestData("Otosales/OtosalesParam").getValue(74, 1)
def accetv4 = findTestData("Otosales/OtosalesParam").getValue(75, 1)
def accetv5 = findTestData("Otosales/OtosalesParam").getValue(76, 1)
def accfld1 = findTestData("Otosales/OtosalesParam").getValue(77, 1)
def accfld2 = findTestData("Otosales/OtosalesParam").getValue(78, 1)
def accfld3 = findTestData("Otosales/OtosalesParam").getValue(79, 1)
def accfld4 = findTestData("Otosales/OtosalesParam").getValue(80, 1)
def accfld5 = findTestData("Otosales/OtosalesParam").getValue(81, 1)
def accsrcc1 = findTestData("Otosales/OtosalesParam").getValue(82, 1)
def accsrcc2 = findTestData("Otosales/OtosalesParam").getValue(83, 1)
def accsrcc3 = findTestData("Otosales/OtosalesParam").getValue(84, 1)
def accsrcc4 = findTestData("Otosales/OtosalesParam").getValue(85, 1)
def accsrcc5 = findTestData("Otosales/OtosalesParam").getValue(86, 1)
def accts1 = findTestData("Otosales/OtosalesParam").getValue(87, 1)
def accts2 = findTestData("Otosales/OtosalesParam").getValue(88, 1)
def accts3 = findTestData("Otosales/OtosalesParam").getValue(89, 1)
def accts4 = findTestData("Otosales/OtosalesParam").getValue(90, 1)
def accts5 = findTestData("Otosales/OtosalesParam").getValue(91, 1)

WebUI.delay(30)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/TXT_GROSS_PREMIUM'), 'value', totalpremi, 3)

println (totalpremi)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/TXT_TOTAL_PREMIUM'), 'value', nettpremi, 3)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/LST_OBJECT_1'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/BTN_EDIT_OBJECT'))

WebUI.delay(8)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_VEHICLE_CODE'), 'value', vehiclecode, 3)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_REG_NO'), 'value', 'B1234GG', 3)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_ENGINE_NO'), 'value', chassisno, 3)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_CHASSIS_NO'), 'value', chassisno, 3)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_COLOR_STNK'), 'value', 'black', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/BTN_NEXT'))

WebUI.delay(3)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/TXT_USAGE'), 'title', usage, 3)

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/TXT_REGION'), 'title', 'TARIF WILAYAH 2 (DKI JAKARTA, JAWA BARAT, BANTEN)', 3)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/BTN_NEXT'))

WebUI.delay(3)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/TXT_PREMIUM'), 'value', totalpremi, 3)

WebUI.delay(1)

//KENDARAAN
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_KENDARAAN'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))

WebUI.delay(3)

//TAHUN 1
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_CASCO_1'), compre1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_ETV_1'), etv1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_FLD_1'), fld1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_SRCC_1'), srcc1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_TS_1'), ts1)

WebUI.delay(1)

//TAHUN 2
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_CASCO_2'), compre2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_ETV_2'), etv2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_FLD_2'), fld2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_SRCC_2'), srcc2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_TS_2'), ts2)

WebUI.delay(1)

//TAHUN 3
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_CASCO_3'), compre3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_ETV_3'), etv3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_FLD_3'), fld3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_SRCC_3'), srcc3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_TS_3'), ts3)

WebUI.delay(1)

//TAHUN 4
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_CASCO_4'), tlo1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_ETV_4'), etv4)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_FLD_4'), fld4)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_SRCC_4'), srcc4)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_TS_4'), ts4)

WebUI.delay(1)

//TAHUN 5
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_CASCO_5'), tlo2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_ETV_5'), etv5)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_FLD_5'), fld5)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_SRCC_5'), srcc5)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/NETT_PREMI_TS_5'), ts5)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_X'))

WebUI.delay(1)

//ACCESSORIES
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_ACCESSORIES'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))

WebUI.delay(3)

//TAHUN 1
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_CASCO_1'), acccompre1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_ETV_1'), accetv1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_FLD_1'), accfld1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_SRCC_1'), accsrcc1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_TS_1'), accts1)

WebUI.delay(1)

//TAHUN 2
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_CASCO_2'), acccompre2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_ETV_2'), accetv2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_FLD_2'), accfld2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_SRCC_2'), accsrcc2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_TS_2'), accts2)

WebUI.delay(1)

//TAHUN 3
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_CASCO_3'), acccompre3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_ETV_3'), accetv3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_FLD_3'), accfld3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_SRCC_3'), accsrcc3)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_TS_3'), accts3)

WebUI.delay(1)

//TAHUN 4
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_CASCO_4'), acctlo1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_ETV_4'), accetv4)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_FLD_4'), accfld4)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_SRCC_4'), accsrcc4)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_TS_4'), accts4)

WebUI.delay(1)

//TAHUN 5
WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_CASCO_5'), acctlo2)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_ETV_5'), accetv5)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_FLD_5'), accfld5)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_SRCC_5'), accsrcc5)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/ACCESSORIES/NETT_PREMI_TS_5'), accts5)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_X'))

WebUI.delay(1)

//PADRIVER
WebUI.verifyElementText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/TXT_PAD_PREMI'), padpremi)

WebUI.delay(1)

//PAPASSENGER
WebUI.verifyElementText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/TXT_PAP_PREMI'), pappremi)

WebUI.delay(1)

//TPL
WebUI.verifyElementText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/TXT_TPL_PREMI'), tplpremi)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE_YES'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/BTN_NEXT'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT_YES'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT_OK'))

WebUI.delay(10)



























//