import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def chassisno = findTestData('Otosales/OtosalesParam').getValue(14, 1)

WebUI.delay(30)

WebUI.click(findTestObject('Object Repository/Web/Task List SA/EXP_SEARCH_BY'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Task List SA/LST_SEARCH_BY_CHASSIS_NO'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Task List SA/TXT_PARAMETER'), chassisno)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Task List SA/BTN_SEARCH'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Task List SA/LST_TASK_LIST'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Task List SA/BTN_SELECT'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Task List SA/BTN_NEXT'))

WebUI.delay(15)

