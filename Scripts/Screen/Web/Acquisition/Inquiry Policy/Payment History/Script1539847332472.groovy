import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

def policyno = ((findTestData('InquiryPolicy').getValue(1, 1)) as String)

def referenceno = ((policyno + '-0') as String)

def premi = ((findTestData('InquiryPolicy').getValue(2, 1)) as String)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Inquiry Policy/TXT_POLICY_NO'), policyno)

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Inquiry Policy/BTN_SEARCH'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Inquiry Policy/LST_POLICY_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Inquiry Policy/BTN_SUMMARY'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Inquiry Policy/TAB_PAYMENT_HISTORY'))

WebUI.delay(1)

WebDriver driver = DriverFactory.getWebDriver()

String ReferenceNo = policyno + '-0'

String NoteType = 'DPR'

WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)

WebElement Table = driver.findElement(By.xpath('//*[@id="TabPolicySummary-3"]/div[1]/div/div/a2is-datatable/div[2]/div/table/tbody'))

List<WebElement> rows = Table.findElements(By.tagName('tr'))

WebUI.delay(1)

def rowcount = rows.size()

def NoteAmount = ''

for (int x = 0; x < rowcount; x++) {
    List<WebElement> Cols = rows.get(x).findElements(By.tagName('td'))

    if (Cols.get(1).getText().trim().equalsIgnoreCase(ReferenceNo) && Cols.get(2).getText().trim().equalsIgnoreCase(NoteType)) {
        NoteAmount = Cols.get(5).getText().trim()
    }
}

WebUI.delay(1)

if (premi != NoteAmount) {
    CustomKeywords.'executeQuery.UpdateDB.insertInquiryResult'(NoteAmount)
} else {
    CustomKeywords.'executeQuery.UpdateDB.insertSuccess'()
}

WebUI.delay(10)

