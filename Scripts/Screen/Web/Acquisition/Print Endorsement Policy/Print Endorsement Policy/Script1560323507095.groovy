import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login DOC Konvensional'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Home/Select Print Endorsement Policy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Print Endorsement Policy/TXT_POLICY_NUMBER'), GlobalVariable.policyendorse)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Endorsement Policy/BTN_SEARCH'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Endorsement Policy/LST_POLICY_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Endorsement Policy/BTN_PRINT'))

WebUI.delay(8)

WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)

WebUI.delay(5)

