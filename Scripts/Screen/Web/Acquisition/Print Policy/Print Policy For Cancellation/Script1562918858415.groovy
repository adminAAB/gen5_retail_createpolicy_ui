import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.google.common.util.concurrent.AbstractFuture.Cancellation
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

//CustomKeywords.'executeQuery.Validation.getPolicyNo'()

//CustomKeywords.'executeQuery.GetIP.IPLocal'()

WebUI.delay(30)

def policyno = findTestData("Cancellation/Policy for Cancel").getValue(2, 1)

println(policyno)

def orderstatus = findTestData("Cancellation/Policy for Cancel").getValue(5, 1)
println (orderstatus)
if (orderstatus == '11') {
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_CLOSE'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_CLOSE_YES'))
	
	WebUI.delay(3)
	
}else if (orderstatus == '9'){
	
	WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Print Policy/TXT_POLICY_NO'), policyno)

	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_SEARCH'))
	
	WebUI.delay(5)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/LST_SEARCH_RESULT_1'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_PRINT'))
	
	WebUI.delay(20)
	
	WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)
	
	WebUI.delay(1)
	
	WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)
	
	WebUI.delay(1)
	
	WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)
	
	WebUI.delay(5)
	
	WebUI.switchToWindowIndex(0)
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_CLOSE'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_CLOSE_YES'))
	
	WebUI.delay(3)
}

/*WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Print Policy/TXT_POLICY_NO'), policyno)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_SEARCH'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_PRINT'))

WebUI.delay(20)

WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.closeWindowIndex(1, FailureHandling.OPTIONAL)

WebUI.delay(5)

WebUI.switchToWindowIndex(0)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_CLOSE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Print Policy/BTN_CLOSE_YES'))

WebUI.delay(3)*/












