import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def nameonpolicy = 'ANTONY QQ JOHNNY SATRIAWAN'

def customerid = 'SPTAF0443'

def policyholder = 'SPTAF0981'

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/BTN_CUSTOMER_NAME'))

WebUI.delay(1)

'CIF'
WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/CIF/TXT_SEARCH_INPUT'), 
    customerid)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/CIF/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/CIF/LST_SEARCH_RESULT_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/CIF/BTN_SELECT'))

WebUI.delay(8)

'Policy Holder'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/Policy Holder/Existing/TXT_SEARCH_INPUT'), 
    policyholder)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/Policy Holder/Existing/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/Policy Holder/Existing/LST_SEARCH_RESULT_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/Policy Holder/Existing/BTN_SELECT'))

WebUI.delay(8)

'Name on Policy'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/EXP_NAME_ON_POLICY'))

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/LST_NAME_ON_POLICY_KEY', 
        [('nameonpolicy') : nameonpolicy]))

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Policy Info Page 1/BTN_NEXT'))

WebUI.delay(3)

