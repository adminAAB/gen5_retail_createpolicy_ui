import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/BTN_ADD'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/EXP_ADD_DEFECT_NAME'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/TXT_ADD_DEFECT_NAME'), 'ACCU')

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/LST_ADD_DEFECT_NAME_RESULT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/EXP_ADD_DAMAGE_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/LST_ADD_DAMAGE_CATEGORY_1'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/TXT_ADD_DESCRIPTION'), 'add defect description')

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/Original Defect/BTN_ADD_SAVE'))

WebUI.delay(3)























