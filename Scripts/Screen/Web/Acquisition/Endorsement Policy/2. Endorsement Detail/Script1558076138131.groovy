import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/EXP_ENDORSEMENT_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/LST_ENDORSEMENT_TYPE_KEY',[('endorsetype'):endorsetype]))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/TXT_REMARKS'), 'remarks')

if (detailinfo == 'Address On Policy') {
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/CHK_DETAIL_INFO_KEY',[('detailinfo'):'Customer']))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/CHK_DETAIL_INFO_KEY',[('detailinfo'):detailinfo]))

}else {
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/CHK_DETAIL_INFO_KEY',[('detailinfo'):detailinfo]))
}

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Endorsement Detail/BTN_NEXT'))

WebUI.delay(5)