import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/BTN_NO_COVER'))

WebUI.delay(1)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/4.C.1. Add No Cover Part'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/No Cover/TAB_ACCESSORIES'))

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/4.C.2. Delete No Cover Accs'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/No Cover/Accessories/BTN_X'))

WebUI.delay(2)

