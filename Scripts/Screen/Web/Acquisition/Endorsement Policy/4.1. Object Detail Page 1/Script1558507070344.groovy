import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/TXT_REGISTRATION_NUMBER'), 'B1234GG')

def chassisold = WebUI.getAttribute(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/TXT_CHASSIS_NO'), 'value')

WebUI.delay(1)

def chassisnew = chassisold + 'endorse'

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/TXT_CHASSIS_NO'), chassisnew)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/TXT_ENGINE_NO'), chassisnew)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/TXT_COLOR_STNK'), 'pink')

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/BTN_NEXT'))

WebUI.delay(2)
