import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_KENDARAAN'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_EDIT'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/LST_COVERAGE_TS'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_DELETE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Interest Coverage/KENDARAAN/BTN_SAVE'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_ACCESSORIES'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE_YES'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_PA_DRIVER'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE_YES'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_PA_PASSENGER'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE_YES'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_TPL'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_DELETE_YES'))

WebUI.delay(10)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/Endorse/BTN_SAVE'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE_YES'))

WebUI.delay(10)

