import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/BTN_CLAUSE'))

WebUI.delay(1)

'add policy clause'
WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Clause/BTN_ADD_P_CLAUSE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Clause/EXP_P_CLAUSE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Clause/TXT_SEARCH_INPUT_P_CLAUSE'), '401022')

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Clause/LST_P_CLAUSE_RESULT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Clause/BTN_SAVE_P_CLAUSE'))

WebUI.delay(8)

'add object clause'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/BTN_ADD_O_CLAUSE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/EXP_O_CLAUSE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/TXT_SEARCH_INPUT_O_CLAUSE'), 
    'OMV10')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/LST_O_CLAUSE_RESULT'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/EXP_OBJECT'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/LST_OBJECT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/BTN_SAVE_O_CLAUSE'))

WebUI.delay(8)

'delete object clause'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/LST_O_CLAUSE_KEY', [
            ('oclause') : 'OMV00']))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/BTN_DELETE_O_CLAUSE'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Endorsement Policy/Object Info/Clause/BTN_X'))

WebUI.delay(3)

