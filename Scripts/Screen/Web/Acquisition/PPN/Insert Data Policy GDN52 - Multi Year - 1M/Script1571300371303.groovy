import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)
def insertdata = ("insert into litt.dbo.dataaab2000 (Tipe,Customer,ProductCode,PolicyPeriodFrom,PolicyPeriodTo,source,VehicleCode,Year,ChasisNo,Usage,Wilayah,COMPeriodFrom,COMPeriodTo,TLOPeriodFrom,TLOPeriodTo,ETVPeriodFrom,ETVPeriodTo,FLDPeriodFrom,FLDPeriodTo,PADPeriodFrom,PADPeriodTo,PADSI,PAPPeriodFrom,PAPPeriodTo,PAPSI,PAPRate,TPLPeriodFrom,TPLPeriodTo,TPLSI,isRun,SRCCPeriodFrom,SRCCPeriodTo,SRCCSI,TSPeriodFrom,TSPeriodTo,TSSI,Marketing,Term,TLOTSPeriodFrom,TLOTSPeriodTo,Acc1,Acc1Price,Acc1Qty,Acc1SI,Acc2,Acc2Price,Acc2Qty,Acc2SI,ACCTSI,Region,AREA,CustomerType,Segment,PolicyPeriod,SurveyType,Info) values ('COM.BUNDLING.TS.PAD.PAP.TPL.ACC','163484533','GDN52','','','P','V08326','2018','BTE','Pribadi','Jakarta','','','','','','','','','','','5000000','','','5000000','','','','5,000,000','0','','','','','','','doc','Full Payment','','','Cover Jok Kulit','500000','4','2000000','Non Audio System','2000000','1','2000000','','','','EXISTING PERSONAL','CASHDEALER','3Y','SKIP SURVEY','PPN')	insert into litt.dbo.nocover (dataid,nocoverdescription) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'')	insert into litt.dbo.nocover (dataid,nocoverdescription) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'')	insert into litt.dbo.cacatsemula (dataid,cacatdescription) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'')	insert into litt.dbo.clause (dataid,clause) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'')	insert into litt.dbo.clause (dataid,clause) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'OMV00')	insert into litt.dbo.clause (dataid,clause) values ((SELECT TOP 1 ID FROM LITT.DBO.DataAAB2000 ORDER BY ID DESC),'')")
WebUI.delay(2)
CustomKeywords.'executeQuery.InsertDataPolicy.insertdata'(insertdata)
WebUI.delay(2)