import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

def VANumber = findTestData('Validation/VANumber X MST_ORDER').getValue(1, 1)

def OrderNo = findTestData('Validation/VANumber X MST_ORDER').getValue(3, 1)

WebUI.click(findTestObject('Object Repository/Web/Dashboard/Task List/EXP_SEARCH_BY'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Dashboard/Task List/LST_SEARCH_BY_ORDERNO'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Dashboard/Task List/TXT_PARAMETER'), OrderNo)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Dashboard/Task List/BTN_SEARCH'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Dashboard/Task List/LST_SEARCH_RESULT_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Dashboard/Task List/BTN_SELECT'))

WebUI.delay(13)

'CUSTOMER INFO PAGE'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Customer Info Page 2/Personal/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Customer Info Page 3/Personal/Confirmation/BTN_NO'))

WebUI.delay(3)

'POLICY INFO PAGE'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 2/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 4/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_CONFIRM_UPDATE_YES'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_CONFIRM_OK'))

WebUI.delay(3)

'OBJECT INFO PAGE'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/BTN_NEXT'))

WebUI.delay(3)

'ORDER SUMMARY PAGE'
WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT_YES'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT_OK'))

WebUI.delay(3)

