import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/EXP_POLICY_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/LST_POLICY_TYPE_NEW'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/BTN_SEARCH_ACCOUNT_MANAGER'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/Search Account Manager/EXP_CATEGORY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/Search Account Manager/LST_CATEGORY_ID'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Virtual Account Number/Search Account Manager/TXT_KEYWORD'), 'doc')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/Search Account Manager/BTN_SEARCH'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/Search Account Manager/LST_SEARCH_RESULT_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/Search Account Manager/BTN_SELECT'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Acquisition/Virtual Account Number/TXT_PIC_MOBILE_NO'), '081234567890')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Virtual Account Number/TXT_REMARKS'), 'remarks')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Virtual Account Number/TXT_CUSTOMER_NAME'), 'BTE CUSTOMER NAME')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/EXP_ID_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/LST_ID_TYPE_KTP'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Virtual Account Number/TXT_ID_NUMBER'), '1234567890')

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/BTN_GENERATE_VA_NUMBER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/BTN_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Virtual Account Number/BTN_X'))

WebUI.delay(3)

