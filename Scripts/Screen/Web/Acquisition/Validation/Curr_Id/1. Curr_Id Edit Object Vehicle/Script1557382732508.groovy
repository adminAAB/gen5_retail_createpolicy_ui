import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

CustomKeywords.'executeQuery.UpdateTipe.exec'('COM')

WebUI.delay(1)

def insertdatapolicy = 'insert into litt.dbo.dataaab2000 (Tipe,Customer,ProductCode,PolicyPeriodFrom,PolicyPeriodTo,source,VehicleCode,Year,ChasisNo,Usage,Wilayah,COMPeriodFrom,COMPeriodTo,TLOPeriodFrom,TLOPeriodTo,ETVPeriodFrom,ETVPeriodTo,FLDPeriodFrom,FLDPeriodTo,PADPeriodFrom,PADPeriodTo,PADSI,PAPPeriodFrom,PAPPeriodTo,PAPSI,PAPRate,TPLPeriodFrom,TPLPeriodTo,TPLSI,isRun,SRCCPeriodFrom,SRCCPeriodTo,SRCCSI,TSPeriodFrom,TSPeriodTo,TSSI,Marketing,Term,TLOTSPeriodFrom,TLOTSPeriodTo,Acc1,Acc1Price,Acc1Qty,Acc1SI,Acc2,Acc2Price,Acc2Qty,Acc2SI,ACCTSI,Region,AREA,CustomerType,Segment,PolicyPeriod,SurveyType) values (\'COM\',\'163484533\',\'GWN50\',\'01/05/2019\',\'01/11/2019\',\'P3C100\',\'V02808\',\'2007\',\'BTE\',\'Pribadi\',\'Jakarta\',\'01/05/2019\',\'01/11/2019\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'0\',\'\',\'\',\'\',\'\',\'\',\'\',\'doc\',\'Full Payment\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'EXISTING PERSONAL\',\'WIC\',\'6M\',\'SKIP SURVEY\')'

WebUI.delay(1)

new executeQuery.DemoMySql().execute(insertdatapolicy)

WebUI.delay(1)

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login LHE Konvensional'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Home/Select Create Policy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/1. Choose Customer'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/2. Policy info'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/3. Object'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/BTN_BACK'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/LST_OBJECT_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/BTN_EDIT_OBJECT'))

WebUI.delay(11)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE_YES'))

WebUI.delay(11)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/BTN_NEXT'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/4. Order Summary'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Login/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

CustomKeywords.'executeQuery.UpdateDataFiles.runUpdate'()

WebUI.delay(3)

WebUI.closeBrowser()

