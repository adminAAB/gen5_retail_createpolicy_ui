import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import forceStop.throwError as throwError

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login LHE Konvensional'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Home/Select Inquiry Policy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

def policyno = findTestData('Validation/Latest PolicyNo Workflow History').getValue(1, 1)

def surveycategory = findTestData('Validation/Latest PolicyNo Workflow History').getValue(2, 1)

WebUI.setText(findTestObject('Web/Acquisition/Inquiry Policy/TXT_POLICY_NO'), policyno)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Inquiry Policy/BTN_SEARCH'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Inquiry Policy/LST_POLICY_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Inquiry Policy/BTN_SUMMARY'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Inquiry Policy/TAB_WORKFLOW_HISTORY'))

WebUI.delay(8)

if (surveycategory == '2') {
	WebDriver driver = DriverFactory.getWebDriver()
	
	String action = 'Approval Level 1 CO'
	
	WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)
	
	WebElement Table = driver.findElement(By.xpath('//*[@id="TabPolicySummary-1"]/div[1]/div/div/a2is-datatable/div[2]/div/table/tbody'))
	
	List<WebElement> rows = Table.findElements(By.tagName('tr'))
	
	WebUI.delay(1)
	
	def rowcount = rows.size()
	
	def user = ''
	
	for (int x = 0; x < rowcount; x++) {
	    List<WebElement> Cols = rows.get(x).findElements(By.tagName('td'))
	
	    if (Cols.get(2).getText().trim().equalsIgnoreCase(action)) {
	        user = Cols.get(3).getText().trim()
			println (user)
	    }
	}
	
	WebUI.delay(1)
	
	println (user)
	
	CustomKeywords.'executeQuery.Validation.checkUserApprovalSkip'(user)
	
}else if (surveycategory == '1') {
	WebDriver driver = DriverFactory.getWebDriver()
	
	String action = 'Approval Level 1 CO'
	
	WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)
	
	WebElement Table = driver.findElement(By.xpath('//*[@id="TabPolicySummary-1"]/div[1]/div/div/a2is-datatable/div[2]/div/table/tbody'))
	
	List<WebElement> rows = Table.findElements(By.tagName('tr'))
	
	WebUI.delay(1)
	
	def rowcount = rows.size()
	
	def user = ''
	
	for (int x = 0; x < rowcount; x++) {
		List<WebElement> Cols = rows.get(x).findElements(By.tagName('td'))
	
		if (Cols.get(2).getText().trim().equalsIgnoreCase(action)) {
			user = Cols.get(3).getText().trim()
			println (user)
		}
	}
	
	WebUI.delay(1)
	
	println (user)
	
	CustomKeywords.'executeQuery.Validation.checkUserApprovalNeed'(user)
}

WebUI.delay(2)

WebUI.closeBrowser()

