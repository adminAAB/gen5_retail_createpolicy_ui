import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

def chassisno = findTestData('ChassisNeedSurvey/MasterID_NSBTE').getValue(1, 1)

WebUI.delay(2)

def insertdatapolicy = ("insert into litt.dbo.dataaab2000 (Tipe,Customer,ProductCode,PolicyPeriodFrom,PolicyPeriodTo,source,VehicleCode,Year,ChasisNo,Usage,Wilayah,COMPeriodFrom,COMPeriodTo,TLOPeriodFrom,TLOPeriodTo,ETVPeriodFrom,ETVPeriodTo,FLDPeriodFrom,FLDPeriodTo,PADPeriodFrom,PADPeriodTo,PADSI,PAPPeriodFrom,PAPPeriodTo,PAPSI,PAPRate,TPLPeriodFrom,TPLPeriodTo,TPLSI,isRun,SRCCPeriodFrom,SRCCPeriodTo,SRCCSI,TSPeriodFrom,TSPeriodTo,TSSI,Marketing,Term,TLOTSPeriodFrom,TLOTSPeriodTo,Acc1,Acc1Price,Acc1Qty,Acc1SI,Acc2,Acc2Price,Acc2Qty,Acc2SI,ACCTSI,Region,AREA,CustomerType,Segment,PolicyPeriod,SurveyType) values ('COM','163484533','GPN20','01/12/2018','01/05/2019','P3A100','v02808','2007','"+chassisno+"','Pribadi','Jakarta','01/12/2018','01/05/2019','','','','','','','','','','','','','','','','','0','','','','','','','DOC','Full Payment','','','','','','','','','','','','','','EXISTING PERSONAL','PROKHUS','6M','NEED SURVEY')")

WebUI.callTestCase(findTestCase('Scenario/New Policy/Config/Existing Personal Customer/PROKHUS/6M/Need Survey/Update Parameter'),
	[:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

new executeQuery.DemoMySql().execute(insertdatapolicy)

WebUI.callTestCase(findTestCase('Scenario/New Policy/Flow/Comprehensive/COM'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)