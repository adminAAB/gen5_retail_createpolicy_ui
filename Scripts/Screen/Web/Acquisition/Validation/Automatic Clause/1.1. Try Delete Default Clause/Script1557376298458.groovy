import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

'TRY DELETE CLAUSE'
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/BTN_CLAUSE'))

WebUI.delay(2)

'DELETE ALL DEFAULT POLICY CLAUSE COUNTED BY ROW'
WebDriver driver = DriverFactory.getWebDriver()

WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)

WebElement TableP = driver.findElement(By.xpath('//*[@id="PanelPUClause-0"]/div[1]/a2is-datatable/div[2]/div/table/tbody'))

List<WebElement> rowsP = TableP.findElements(By.tagName('tr'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

def rowcountP = rowsP.size() as Integer
def x = 1 as Integer
while (x <= rowcountP) {
	WebUI.delay(1)
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/LST_P_CLAUSE',[('index'):x]))
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_DELETE_P_CLAUSE'))
	WebUI.delay(2)
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/LBL_INFO_CANNOT_DELETE'), 3)
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_CLOSE_MESSAGE'))
	WebUI.delay(2)
	x = (x+1)
}

'DELETE ALL DEFAULT OBJECT CLAUSE COUNTED BY ROW'
WebUI.switchToFrame(findTestObject('Web/FRAME'), 5)

WebElement TableO = driver.findElement(By.xpath('//*[@id="PanelPUObjectClause-0"]/div[1]/a2is-datatable/div[2]/div/table/tbody'))

List<WebElement> rowsO = TableO.findElements(By.tagName('tr'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

def rowcountO = rowsO.size() as Integer
def y = 1 as Integer
while (y <= rowcountO) {
	WebUI.delay(1)
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/LST_O_CLAUSE',[('index'):y]))
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_DELETE_O_CLAUSE'))
	WebUI.delay(2)
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/LBL_INFO_CANNOT_DELETE'), 3)
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_CLOSE_MESSAGE'))
	WebUI.delay(2)
	y = (y+1)
}

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_CLOSE'))

WebUI.delay(3)