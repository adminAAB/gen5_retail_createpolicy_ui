import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.maximizeWindow()

//CustomKeywords.'executeQuery.Validation.getPolicyNo'()

WebUI.delay(2)

def policyno = findTestData("Cancellation/Policy for Cancel").getValue(2, 1)
def biztype = findTestData("Cancellation/Policy for Cancel").getValue(12, 1)
println(policyno)
println(biztype)

if (biztype == '1'){
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/TXT_INPUT'), policyno)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_SEARCH_RESULT_1'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/BTN_NEXT'))

WebUI.delay(8)

}else if (biztype == '2') {

WebUI.callTestCase(findTestCase("Screen/Web/Login/Logout"), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login DOC Syariah'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.callTestCase(findTestCase("Screen/Web/Home/Select Cancellation Policy"), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/TXT_INPUT'), policyno)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/LST_SEARCH_RESULT_1'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/BTN_NEXT'))

WebUI.delay(8)

}

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_UPLOAD'))

WebUI.delay(5)

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_EFFECTIVE_DATE'))

WebUI.delay(2)

CustomKeywords.'ifCondition.CancelEffectiveDate.input'()

WebUI.delay(5)

CustomKeywords.'ifCondition.CancellationType.CType'()

//WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/EXP_CANCELLATION_TYPE'))

//WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/LST_INTERNAL_REQUEST'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/TXT_REMARKS'), 'remarks')

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/TXT_ACCOUNT NUMBER'), '88888888888')

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/TXT_ACCOUNT NAME'), 'pak tarno ganteng')

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/TXT_BANK NAME'), 'Bank Bank tut')

WebUI.delay(1)

def isNeedHardCopy = findTestData("Cancellation/Policy for Cancel").getValue(9, 1)
println(isNeedHardCopy)

if (isNeedHardCopy == '0'){
	
	WebUI.delay(5)

	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_SUBMIT'))
	
	WebUI.delay(3)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CANCEL_YES'))
	
	WebUI.delay(20)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CANCEL_X'))
	
	WebUI.delay(20)
	
	//'CHECK STATUS POLICY'
	//CustomKeywords.'executeQuery.Validation.checkCancelPolicyStatus'()
	
	WebUI.delay(5)	
}else if (isNeedHardCopy == '1'){
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_DELIVERY_ADDRESS'))
	
	WebUI.delay(5)
	
	CustomKeywords.'ifCondition.CancelDeliverTo.deliverto'()
	
	/*WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/Delivery Address/EXP_DELIVERY_ADDRESS'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/Delivery Address/LST_DELIVERY_HOME'))
	
	WebUI.delay(1)*/
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/Delivery Address/BTN_X'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_SUBMIT'))
	
	WebUI.delay(3)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CANCEL_YES'))
	
	WebUI.delay(20)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CANCEL_X'))
	
	WebUI.delay(20)
	
	
}

/*WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_DELIVERY_ADDRESS'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/Delivery Address/EXP_DELIVERY_ADDRESS'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/Delivery Address/LST_DELIVERY_HOME'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/Delivery Address/BTN_X'))

WebUI.delay(1)*/

/*WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_SUBMIT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CANCEL_YES'))

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Cancelation Policy/Cancelation Summary/BTN_CANCEL_X'))

WebUI.delay(20)*/

//'CHECK STATUS POLICY'
//CustomKeywords.'executeQuery.Validation.checkCancelPolicyStatus'()

//WebUI.delay(5)

