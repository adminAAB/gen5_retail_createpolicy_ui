import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

WebUI.maximizeWindow()

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/BTN_SORT_ORDER_NO'))

WebUI.delay(7)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/BTN_SORT_ORDER_NO'))

WebUI.delay(5)

//println ('CHASSIS YANG AKAN DI APPROVE'+orderno)

/*def query = 'SELECT GLOBALVARIABLE_2 FROM LITT.DBO.GLOBALVARIABLE_BTE'
CustomKeywords.'executeQuery.DemoMySql.connectDB'('172.16.94.74', 'AAB', 'sa', 'Password95')
ResultSet orderno = CustomKeywords.'executeQuery.DemoMySql.executeQuery'(query)*/

def endorsno = findTestData("Cancellation/Approve Cancel").getValue(3, 1)

println(endorsno)

def orderno = findTestData("Cancellation/Approve Cancel").getValue(12, 1)

println (orderno)

def orderstatus = findTestData("Cancellation/Approve Cancel").getValue(5, 1)

println(orderstatus)

if(orderstatus == '94' ){

println ('CHASSIS YANG AKAN DI APPROVE : '+ orderno)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/LST_ORDER_TOP_1',[('orderno'):orderno]))

println ('CHASIS YG sudah di approve : '+ orderno)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/BTN_NEXT'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/TXT_REMARKS'), 'APPROVE REMARKS')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT_YES'))

WebUI.delay(30)

}else if (orderstatus == '93'){
println ('CHASSIS YANG AKAN DI APPROVE : '+ orderno)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/LST_ORDER_TOP_1',[('orderno'):orderno]))

println ('CHASIS YG sudah di approve : '+ orderno)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/BTN_NEXT'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/TXT_REMARKS'), 'APPROVE REMARKS')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT_YES'))

WebUI.delay(30)
}else if (orderstatus == '92'){

println ('CHASSIS YANG AKAN DI APPROVE : '+ orderno)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/LST_ORDER_TOP_1',[('orderno'):orderno]))

println ('CHASIS YG sudah di approve : '+ orderno)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/BTN_NEXT'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/TXT_REMARKS'), 'APPROVE REMARKS')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT_YES'))

WebUI.delay(30)
}else if (orderstatus == '91'){
println ('CHASSIS YANG AKAN DI APPROVE : '+ orderno)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/LST_ORDER_TOP_1',[('orderno'):orderno]))

println ('CHASIS YG sudah di approve : '+ orderno)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/BTN_NEXT'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/TXT_REMARKS'), 'APPROVE REMARKS')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Approve Policy/Approval Detail/BTN_SUBMIT_YES'))

WebUI.delay(30)
}
//UNTUK VALIDASI YG LIMIT SA, JADI LANGSUNG TUTUP BROWSER
 else if (endorsno == '1'){
	 println(orderno)
	 println(endorsno)
	 WebUI.delay(2)
	 WebUI.closeBrowser()
}

