import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'executeQuery.GetIP.IPLocal'()

def datanocover = ''

if (GlobalVariable.vmip == '172.16.94.191') {
	datanocover = 'NoCover3'
}else if (GlobalVariable.vmip == '172.16.94.192') {
	datanocover = 'NoCover4'
}else if (GlobalVariable.vmip == '172.16.94.193') {
	datanocover = 'NoCover5'
}else if (GlobalVariable.vmip == '172.16.94.194') {
	datanocover = 'NoCover6'
}else if (GlobalVariable.vmip == '172.16.92.22') {
	datanocover = 'NoCover'
}else if (GlobalVariable.vmip == '10.1.2.128') {
	datanocover = 'NoCover'
}else {
	datanocover = 'NoCover'
}

WebUI.delay(1)

def chasisnumber = (findTestData(GlobalVariable.vmparam).getValue(10, 1)) as String
def countnocover = findTestData(datanocover).getRowNumbers() as Integer
def x = 1 as Integer
def nsbte = 'NSBTE'

WebUI.delay(1)

while (x <= countnocover){
def cn = (findTestData(datanocover).getValue(3, x)).toUpperCase() as String
	if (cn != '' && cn != null) {
		if (chasisnumber.contains(nsbte)) {
				if ((findTestData(datanocover).getValue(3, x)).toUpperCase() == 'JOK BELAKANG'){
					WebUI.delay(3)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/EXP_PART_NAME'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/TXT_SEARCH_PART'), findTestData(datanocover).getValue(3, x))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/LST_SEARCH_RESULT_1'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/EXP_DAMAGE_CATEGORY'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/LST_DAMAGE_RUSAK'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/TXT_DESCRIPTION'), 'deskripsi')
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/TXT_QUANTITY'), '1')
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/Part/BTN_SAVE'))
					WebUI.delay(1)
				}
				else if ((findTestData(datanocover).getValue(3, x)).toUpperCase() == 'ALARM'){
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/BTN_ACCESSORIES'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_ACC_NAME'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_SEARCH_ACC'), findTestData(datanocover).getValue(3, x))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_SEARCH_RESULT_1'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_DAMAGE_CATEGORY'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_DAMAGE_RUSAK'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_DESCRIPTION'), 'deskripsi')
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_QUANTITY'), '1')
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_SAVE'))
					WebUI.delay(1)
				}
				else if ((findTestData(datanocover).getValue(3, x)).toUpperCase() == 'TV'){
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/BTN_ACCESSORIES'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_ACC_NAME'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_SEARCH_ACC'), findTestData(datanocover).getValue(3, x))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_SEARCH_RESULT_1'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_DAMAGE_CATEGORY'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_DAMAGE_RUSAK'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_DESCRIPTION'), 'deskripsi')
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_QUANTITY'), '1')
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_SAVE'))
					WebUI.delay(1)
				}
				else if ((findTestData(datanocover).getValue(3, x)).toUpperCase() == 'COVER JOK KULIT'){
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/BTN_ACCESSORIES'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_ACC_NAME'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_SEARCH_ACC'), findTestData(datanocover).getValue(3, x))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_SEARCH_RESULT_1'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_DAMAGE_CATEGORY'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_DAMAGE_RUSAK'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_DESCRIPTION'), 'deskripsi')
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_QUANTITY'), '1')
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_SAVE'))
					WebUI.delay(1)
				}
				else {
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_ADD'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_ACC_NAME'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_SEARCH_ACC'), findTestData(datanocover).getValue(3, x))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_SEARCH_RESULT_1'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/EXP_DAMAGE_CATEGORY'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/LST_DAMAGE_RUSAK'))
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_DESCRIPTION'), 'deskripsi')
					WebUI.delay(1)
					WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/TXT_QUANTITY'), '1')
					WebUI.delay(1)
					WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/ACCESSORIES/BTN_SAVE'))
					WebUI.delay(1)
				}
		}
	}
x = x + 1
}

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/No Cover/BTN_CLOSE'))

WebUI.delay(1)

