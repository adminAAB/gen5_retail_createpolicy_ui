import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

CustomKeywords.'executeQuery.GetIP.IPLocal'()

def customertype = findTestData(GlobalVariable.vmparam).getValue(54, 1) as String
println (customertype)
WebUI.delay(2)

if (customertype == 'NEW PERSONAL') {
	WebUI.delay(1)
    WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/1.1. New Personal Customer'), [:], FailureHandling.STOP_ON_FAILURE)
}
else if (customertype == 'EXISTING PERSONAL') {
	WebUI.delay(1)
	WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/1.1. Existing Personal Customer'), [:], FailureHandling.STOP_ON_FAILURE)
}
else if (customertype == 'NEW COMPANY') {
	WebUI.delay(1)
	WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/1.1. New Company Customer'), [:], FailureHandling.STOP_ON_FAILURE)
}
else if (customertype == 'EXISTING COMPANY') {
	WebUI.delay(1)
	WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/1.1. Existing Company Customer'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(2)


