import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'executeQuery.GetIP.IPLocal'()

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_EXISTING_CUSTOMER'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Existing Customer/TXT_SEARCH_BY'), findTestData(
        GlobalVariable.vmparam).getValue(3, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Existing Customer/BTN_SEARCH'))

WebUI.delay(20)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Existing Customer/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Existing Customer/BTN_SELECT'))

WebUI.delay(15)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_NEXT'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/BTN_NEXT'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_NEXT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/Confirmation/BTN_NO'))

