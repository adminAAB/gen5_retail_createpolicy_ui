import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After
import org.stringtemplate.v4.compiler.CodeGenerator.conditional_return

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.helper.screenshot.WebUIScreenCaptor
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain

import internal.GlobalVariable as GlobalVariable

def companyname = findTestData("CustomerCompany").getValue(3,1) as String
def companygroup = findTestData("CustomerCompany").getValue(4,1) as String
def companytype = findTestData("CustomerCompany").getValue(5,1) as String
def npwpname = findTestData("CustomerCompany").getValue(6,1) as String
def npwpno = findTestData("CustomerCompany").getValue(7,1) as String
def npwpdate = findTestData("CustomerCompany").getValue(8,1) as String
def npwpaddress = findTestData("CustomerCompany").getValue(9,1) as String
def siupno = findTestData("CustomerCompany").getValue(10,1) as String
def pkpno = findTestData("CustomerCompany").getValue(11,1) as String
def pkpdate = findTestData("CustomerCompany").getValue(12,1) as String
def address = findTestData("CustomerCompany").getValue(13,1) as String
def postalcode = findTestData("CustomerCompany").getValue(14,1) as String
def officenumber = findTestData("CustomerCompany").getValue(15,1) as String
def ext = findTestData("CustomerCompany").getValue(16,1) as String
def picname = findTestData("CustomerCompany").getValue(17,1) as String
def email = findTestData("CustomerCompany").getValue(18,1) as String
def mobilenumber1 = findTestData("CustomerCompany").getValue(19,1) as String
def mobilenumber2 = findTestData("CustomerCompany").getValue(20,1) as String


WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/RDB_COMPANY'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/TXT_COMPANY_NAME'), companyname)

WebUI.delay(1)

if(companygroup != ''){
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/EXP_COMPANY_GROUP'))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/TXT_COMPANY_GROUP_SEARCH'), companygroup)
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/LST_COMPANY_GROUP_RESULT_1'))
}

WebUI.delay(1)

if (companytype != ''){
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/EXP_COMPANY_TYPE'))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/TXT_COMPANY_TYPE_SEARCH'), companytype)
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/LST_COMPANY_TYPE_RESULT_1'))
}

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/TXT_NPWP_NAME'), npwpname)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/TXT_NPWP_NUMBER'), npwpno)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_NPWP_DATE'))

WebUI.delay(1)

///// SET NPWP Date
WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_CAL_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_CAL_2'))

String cmonthNPWP

String cdayNPWP

def dayNPWP = npwpdate.split('/')[0]

def monthNPWP = npwpdate.split('/')[1]

def yearNPWP = ((npwpdate.split('/')[2]) as Integer)

WebUI.delay(1)

//convert month
if (monthNPWP == '01') {
	cmonthNPWP = 'Jan'
} else if (monthNPWP == '02') {
	cmonthNPWP = 'Feb'
} else if (monthNPWP == '03') {
	cmonthNPWP = 'Mar'
} else if (monthNPWP == '04') {
	cmonthNPWP = 'Apr'
} else if (monthNPWP == '05') {
	cmonthNPWP = 'May'
} else if (monthNPWP == '06') {
	cmonthNPWP = 'Jun'
} else if (monthNPWP == '07') {
	cmonthNPWP = 'Jul'
} else if (monthNPWP == '08') {
	cmonthNPWP = 'Aug'
} else if (monthNPWP == '09') {
	cmonthNPWP = 'Sep'
} else if (monthNPWP == '10') {
	cmonthNPWP = 'Oct'
} else if (monthNPWP == '11') {
	cmonthNPWP = 'Nov'
} else if (monthNPWP == '12') {
	cmonthNPWP = 'Dec'
} else {
	println('Month not found')
}

WebUI.delay(1)

//convert day
if (dayNPWP == '01') {
	cdayNPWP = '1'
} else if (dayNPWP == '02') {
	cdayNPWP = '2'
} else if (dayNPWP == '03') {
	cdayNPWP = '3'
} else if (dayNPWP == '04') {
	cdayNPWP = '4'
} else if (dayNPWP == '05') {
	cdayNPWP = '5'
} else if (dayNPWP == '06') {
	cdayNPWP = '6'
} else if (dayNPWP == '07') {
	cdayNPWP = '7'
} else if (dayNPWP == '08') {
	cdayNPWP = '8'
} else if (dayNPWP == '09') {
	cdayNPWP = '9'
} else {
	cdayNPWP = dayNPWP
}

while (WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_YEAR', [('yearNPWP') : yearNPWP]),
	2, FailureHandling.OPTIONAL) == false) {
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_CAL_NEXT'))
}

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_YEAR', [('yearNPWP') : yearNPWP]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_MONTH', [('monthNPWP') : cmonthNPWP]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_DAY', [('dayNPWP') : cdayNPWP]))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/TXT_NPWP_ADDRESS'), npwpaddress)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_CHOOSE_IMAGE'))

WebUI.delay(8)

CustomKeywords.'runEXE.UploadFilePath.Upload'()

WebUI.delay(20)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Company/BTN_NEXT'))

WebUI.delay(3)

if(siupno != ''){
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/TXT_SIUP_NUMBER'), siupno)
}

WebUI.delay(1)

if(pkpno != ''){
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/TXT_PKP_NUMBER'), pkpno)
}

WebUI.delay(1)

///// SET PKP Date

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_PKP_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_CAL_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_CAL_2'))

String cmonthPKP

String cdayPKP

def dayPKP = pkpdate.split('/')[0]

def monthPKP = pkpdate.split('/')[1]

def yearPKP = ((pkpdate.split('/')[2]) as Integer)

WebUI.delay(1)

//convert month
if (monthPKP == '01') {
	cmonthPKP = 'Jan'
} else if (monthPKP == '02') {
	cmonthPKP = 'Feb'
} else if (monthPKP == '03') {
	cmonthPKP = 'Mar'
} else if (monthPKP == '04') {
	cmonthPKP = 'Apr'
} else if (monthPKP == '05') {
	cmonthPKP = 'May'
} else if (monthPKP == '06') {
	cmonthPKP = 'Jun'
} else if (monthPKP == '07') {
	cmonthPKP = 'Jul'
} else if (monthPKP == '08') {
	cmonthPKP = 'Aug'
} else if (monthPKP == '09') {
	cmonthPKP = 'Sep'
} else if (monthPKP == '10') {
	cmonthPKP = 'Oct'
} else if (monthPKP == '11') {
	cmonthPKP = 'Nov'
} else if (monthPKP == '12') {
	cmonthPKP = 'Dec'
} else {
	println('Month not found')
}

WebUI.delay(1)

//convert day
if (dayPKP == '01') {
	cdayPKP = '1'
} else if (dayPKP == '02') {
	cdayPKP = '2'
} else if (dayPKP == '03') {
	cdayPKP = '3'
} else if (dayPKP == '04') {
	cdayPKP = '4'
} else if (dayPKP == '05') {
	cdayPKP = '5'
} else if (dayPKP == '06') {
	cdayPKP = '6'
} else if (dayPKP == '07') {
	cdayPKP = '7'
} else if (dayPKP == '08') {
	cdayPKP = '8'
} else if (dayPKP == '09') {
	cdayPKP = '9'
} else {
	cdayPKP = dayPKP
}

while (WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_YEAR', [('yearPKP') : yearPKP]),
	2, FailureHandling.OPTIONAL) == false) {
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_CAL_NEXT'))
}

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_YEAR', [('yearPKP') : yearPKP]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_MONTH', [('monthPKP') : cmonthPKP]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_DAY', [('dayPKP') : cdayPKP]))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/TXT_ADDRESS'), address)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/Postal Code/TXT_SEARCH_INPUT'), postalcode)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/Postal Code/BTN_SEARCH'))

WebUI.delay(12)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/Postal Code/LST_POSTAL_CODE_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/Postal Code/BTN_SELECT'))

WebUI.delay(1)

if (officenumber != ''){
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/TXT_OFFICE_NUMBER'), officenumber)
}

if (ext != ''){
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/TXT_EXTENSION'), ext)
}

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Company/BTN_NEXT'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/TXT_PIC_NAME'), picname)

WebUI.delay(1)

if (email != ''){
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/TXT_EMAIL'), email)
}

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/TXT_MOBILE_NUMBER_1'), mobilenumber1)

WebUI.delay(1)

if (mobilenumber2 != ''){
	WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/TXT_MOBILE_NUMBER_2'), mobilenumber2)
}

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/BTN_NEXT'))

WebUI.delay(15)

if (WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/LST_CUSTOMER_1'), 10, FailureHandling.OPTIONAL) == true) {
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/LST_CUSTOMER_1'))
	WebUI.delay(2)
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/LST_SELECT'))
	WebUI.delay(2)
}
else {
	if (WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/BTN_CREATE_YES'), 10, FailureHandling.OPTIONAL) == true){
		WebUI.delay(2)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/BTN_CREATE_YES'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/BTN_CREATE_OK'))
	}else {
		WebUI.delay(2)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/BTN_UPDATE_YES'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Company/BTN_CREATE_OK'))
	}
}
WebUI.delay(3)






























