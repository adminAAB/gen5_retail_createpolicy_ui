import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser.func_scope as func_scope
import org.junit.After as After
import com.google.common.base.Equivalence.Equals as Equals
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.TestObjectXpath as TestObjectXpath
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import io.cucumber.tagexpressions.TagExpressionParser.True as True
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import groovy.time.TimeCategory

CustomKeywords.'executeQuery.GetIP.IPLocal'()

def source = findTestData(GlobalVariable.vmparam).getValue(7, 1).toUpperCase()

def term = findTestData(GlobalVariable.vmparam).getValue(39, 1)

def segment = findTestData(GlobalVariable.vmparam).getValue(55, 1).toUpperCase()

def marketing = findTestData(GlobalVariable.vmparam).getValue(38, 1).toUpperCase()

String productcode = (findTestData(GlobalVariable.vmparam).getValue(4, 1)) as String

def periodfrom = findTestData(GlobalVariable.vmparam).getValue(5, 1)

def cbte = ((findTestData(GlobalVariable.vmparam).getValue(58,1)) as String)

def cancel = 'CANCEL'

if (cbte.contains(cancel)) {
	def paramperiodfrom = ("UPDATE GLOBALVARIABLE_BTE SET GLOBALVARIABLE_2 = '"+periodfrom+"'")
	CustomKeywords.'executeQuery.DemoMySql.execute'(paramperiodfrom)
}

WebUI.delay(1)

CustomKeywords

def periodto = findTestData(GlobalVariable.vmparam).getValue(6, 1)

def policyperiod = findTestData(GlobalVariable.vmparam).getValue(56, 1).toUpperCase()

def y = 'Y'

def m = 'M'

WebUI.delay(1)

CustomKeywords.'ifCondition.PolicyPeriod.Policy'()

WebUI.delay(1)

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/BTN_SEARCH_PRODUCT'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/Product Search By/TXT_SEARCH_BY_INPUT'), findTestData(GlobalVariable.vmparam).getValue(
        4, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/Product Search By/BTN_SEARCH'))

WebUI.delay(7)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/Product Search By/LST_SEARCH_BY_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/Product Search By/BTN_SELECT'))

WebUI.delay(8)

'Policy holder'
CustomKeywords.'ifCondition.PolicyHolder.PHolder'()

'input financial institution if pay_to_party = 2'
CustomKeywords.'ifCondition.FinInstitution.check'()

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/EXP_SEGMENT'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/LST_SEGMENT', [('source') : source]))

WebUI.delay(1)

if (periodfrom != '') {
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_PERIOD_FROM'))
	WebUI.delay(1)
	CustomKeywords.'ifCondition.PolicyPeriod.PolicyPeriodFrom'()
	WebUI.delay(1)
}else {
	WebUI.delay(1)
}

if (periodto != '') {
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_PERIOD_TO'))
	WebUI.delay(1)
	CustomKeywords.'ifCondition.PolicyPeriod.PolicyPeriodTo'()
	WebUI.delay(1)
}else {
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_PERIOD_TO'))
	WebUI.delay(1)
	CustomKeywords.'ifCondition.PolicyPeriod.PolicyPeriodToAUTO'()
	WebUI.delay(1)
}


WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 1/BTN_NEXT'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/TXT_HEADER'), 3)

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/EXP_TERMS'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/LST_TERMS', [('terms') : term]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_FIND_OFFICER'))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Account Officer/TXT_SEARCH_BY_INPUT'), marketing)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Account Officer/BTN_SEARCH'))

WebUI.delay(15)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Account Officer/LST_SEARCH_RESULT_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Account Officer/BTN_SELECT'))

WebUI.delay(5)

///// PRODUCT LEXUS INPUT DEALER
if (segment == 'LEXUS') {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_DEALER'))

    WebUI.delay(2)

    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/TXT_SEARCH_INPUT'), '119264249')

    WebUI.delay(2)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SEARCH'))

    WebUI.delay(7)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/LST_SEARCH_RESULT_1'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find Dealer/BTN_SELECT'))

    WebUI.delay(2)
}

WebUI.delay(1)

///// CASH DEALER
CustomKeywords.'ifCondition.CashDealer.variant'()

WebUI.delay(1)

///// PRODUCT TITAN INPUT ID AGENT
if (segment == 'TITAN') {
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_SEARCH_ID_AGENT'))
	
		WebUI.delay(2)
	
		WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/TXT_SEARCH_INPUT'), 'LASM0796')
	
		WebUI.delay(2)
	
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SEARCH'))
	
		WebUI.delay(15)
	
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/LST_SEARCH_RESULT_1'))
	
		WebUI.delay(1)
	
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/Find ID Agent/BTN_SELECT'))
	
		WebUI.delay(10)
}

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/BTN_NEXT'))

WebUI.delay(8)

WebUI.scrollToElement(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/TXT_HEADER'), 3)

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/EXP_ADDRESS_POLICY'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/LST_ADDRESS_CUST_HOME'))

WebUI.delay(1)

println (productcode)

println (source)

CustomKeywords.'executeQuery.Validation.cekEPolicy'(productcode,source)

println (GlobalVariable.isNeedHardCopy)

if (GlobalVariable.isNeedHardCopy == '0') {
	def info = findTestData(GlobalVariable.vmparam).getValue(58, 1)
	def email_deliver
	def emaideliverCustomer = 'email_deliver:Customer Email Address'
	def emaildeliverPholder = 'email_deliver:Policy Holder Email Address'
	def emaildeliverBoth = 'email_deliver:Both'

WebUI.delay(1)

	if (info.contains(emaideliverCustomer)){
		email_deliver = 'Customer Email Address'
	}else if (info.contains(emaildeliverPholder)) {
		email_deliver = 'Policy Holder Email Address'
	}else if (info.contains(emaildeliverBoth)){
		email_deliver = 'Both'
	}
	WebUI.delay(2)
	
	if  (email_deliver == 'Customer Email Address' ) {
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
		
		WebUI.delay (1)
		
	}
		
	else if (email_deliver == 'Policy Holder Email Address') {
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
		
		WebUI.delay (1)
		
	}
	
	else if (email_deliver == 'Both') {
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
		
	}
}

else {
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/EXP_DELIVER_TO'))
	
	WebUI.delay(1)
	def deliver
	def info = findTestData(GlobalVariable.vmparam).getValue(58, 1) as String
	def deliverCustomer = 'Deliver:Customer'
	def deliverBranch = 'Deliver:Branch'
	def deliverGardaCenter = 'Deliver:Garda Center'
	def deliverOthers = 'Deliver:-Others'
	
	WebUI.delay(1)
	
	if (info.contains(deliverCustomer)){
		deliver = 'Customer'
	}else if (info.contains(deliverGardaCenter)) {
		deliver = 'Garda Center'
	} else if (info.contains(deliverOthers)){
		deliver = 'Others'
	} else if (info.contains(deliverBranch)){
		deliver ='Branch'
	}
	
	if (deliver == 'Customer') {
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/LST_DELIVER_TO_CUST',[('deliver'):deliver]))
	
	WebUI.delay(5)
	
	}else if (deliver == 'Branch'){
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/LST_DELIVER_TO_CUST',[('deliver'):deliver]))
	
	WebUI.delay(5)
	}else if (deliver == 'Garda Center') {
	
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_DELIVER_TO_GC'))
	
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_UP_NAME'), 'GC Sulai')
		
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_MOBILE_NO'), '089547487')
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_DELIVER_ADDRESS'))
		
		WebUI.delay(1)
		
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_SEARCH_INPUT'), 'Pluit Junction')
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_SEARCH_RESULT_1'))
		
		WebUI.delay(1)
	}else if (deliver == 'Others') {
	
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_UP_NAME'), 'Sulai Others')
		
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_MOBILE_NO'), '089547487')
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_SEARCH_POSTAL_CODE'))
		
		WebUI.delay(3)
		
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_INPUT_POST_CODE'), '45115')
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_SEARCH_PARAM'))
		
		WebUI.delay(3)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_RESULT_POST_CODE'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_SELECT'))
		
		WebUI.delay(3)
		
		WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_DELIVER_ADDRESS'), 'delivery address other')
		
	}
	
	def email_deliver
	def emaideliverCustomer = 'email_deliver:Customer Email Address'
	def emaildeliverPholder = 'email_deliver:Policy Holder Email Address'
	def emaildeliverBoth = 'email_deliver:Both'

WebUI.delay(1)

	if (info.contains(emaideliverCustomer)){
		email_deliver = 'Customer Email Address'
	}else if (info.contains(emaildeliverPholder)) {
		email_deliver = 'Policy Holder Email Address'
	}else if (info.contains(emaildeliverBoth)){
		email_deliver = 'Both'
}
	
	WebUI.delay(2)
	
	WebUI.scrollToElement(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'), 3, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(2)
	
	if  (email_deliver == 'Customer Email Address' ) {
		
			GlobalVariable.Email = 'Customer Email Address'
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
		
		WebUI.delay (1)
		
	}
		
	else if (email_deliver == 'Policy Holder Email Address') {
		
		GlobalVariable.Email = 'Policy Holder Email Address'
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
		
		WebUI.delay (1)
		
	}
	
	else if (email_deliver == 'Both') {
		
			GlobalVariable.Email = 'Both'
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
		
		WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
		
		
	}
}

//validasi email pholder kosong
WebUI.delay (2)
WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/BTN_NEXT'))

/*WebUI.delay(3)
boolean exist = WebUI.verifyElementVisible(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 4/LBL_Pholder_Email_Validation'))

	//WebUI.verifyElementText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/LBL_Pholder_Email_Validation'),
	//'Policy holder email is empty, please edit in Maintenance Customer Info first ')

	
	//WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/LBL_Pholder_Email_Validation'), 3)

println (exist)

if (exist == true){
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 4/BTN_Pholder_Email_Validation_OK'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 2/BTN_BACK'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 2/BTN_BACK'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/BTN_POLICY_HOLDER'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_EXISTING'))
	
	WebUI.delay(1)
	
	WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/TXT_SEARCH_BY_INPUT'),'163546163')
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SEARCH'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/LST_SEARCH_RESULT_1'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 1/CIF/EXISTING/BTN_SELECT'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/BTN_NEXT'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/BTN_NEXT'))
	
	//tambah validasi lagi
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/EXP_ADDRESS_POLICY'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/LST_ADDRESS_CUST_HOME'))
	
	WebUI.delay(1)
	
	if (GlobalVariable.isNeedHardCopy == '0') {
		def info = findTestData(GlobalVariable.vmparam).getValue(58, 1)
		def email_deliver
		def emaideliverCustomer = 'email_deliver:Customer Email Address'
		def emaildeliverPholder = 'email_deliver:Policy Holder Email Address'
		def emaildeliverBoth = 'email_deliver:Both'
	
	WebUI.delay(1)
	
		if (info.contains(emaideliverCustomer)){
			email_deliver = 'Customer Email Address'
		}else if (info.contains(emaildeliverPholder)) {
			email_deliver = 'Policy Holder Email Address'
		}else if (info.contains(emaildeliverBoth)){
			email_deliver = 'Both'
		}
		WebUI.delay(2)
		
		if  (email_deliver == 'Customer Email Address' ) {
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
			
			WebUI.delay (1)
			
		}
			
		else if (email_deliver == 'Policy Holder Email Address') {
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
			
			WebUI.delay (1)
			
		}
		
		else if (email_deliver == 'Both') {
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
			
		}
	}
	
	else {
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/EXP_DELIVER_TO'))
		
		WebUI.delay(1)
		def deliver
		def info = findTestData(GlobalVariable.vmparam).getValue(58, 1) as String
		def deliverCustomer = 'Deliver:Customer'
		def deliverBranch = 'Deliver:Branch'
		def deliverGardaCenter = 'Deliver:Garda Center'
		def deliverOthers = 'Deliver:-Others'
		
		WebUI.delay(1)
		
		if (info.contains(deliverCustomer)){
			deliver = 'Customer'
		}else if (info.contains(deliverGardaCenter)) {
			deliver = 'Garda Center'
		} else if (info.contains(deliverOthers)){
			deliver = 'Others'
		} else if (info.contains(deliverBranch)){
			deliver ='Branch'
		}
		
		if (deliver == 'Customer') {
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/LST_DELIVER_TO_CUST',[('deliver'):deliver]))
		
		WebUI.delay(5)
		
		}else if (deliver == 'Branch'){
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/LST_DELIVER_TO_CUST',[('deliver'):deliver]))
		
		WebUI.delay(5)
		}else if (deliver == 'Garda Center') {
		
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_DELIVER_TO_GC'))
		
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_UP_NAME'), 'GC Sulai')
			
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_MOBILE_NO'), '089547487')
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_DELIVER_ADDRESS'))
			
			WebUI.delay(1)
			
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_SEARCH_INPUT'), 'Pluit Junction')
			
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_SEARCH_RESULT_1'))
			
			WebUI.delay(1)
		}else if (deliver == 'Others') {
		
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_UP_NAME'), 'Sulai Others')
			
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_MOBILE_NO'), '089547487')
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_SEARCH_POSTAL_CODE'))
			
			WebUI.delay(3)
			
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_INPUT_POST_CODE'), '45115')
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_SEARCH_PARAM'))
			
			WebUI.delay(3)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_RESULT_POST_CODE'))
			
			WebUI.delay(1)
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/BTN_SELECT'))
			
			WebUI.delay(3)
			
			WebUI.setText(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/TXT_DELIVER_ADDRESS'), 'delivery address other')
			
		}
		
		def email_deliver
		def emaideliverCustomer = 'email_deliver:Customer Email Address'
		def emaildeliverPholder = 'email_deliver:Policy Holder Email Address'
		def emaildeliverBoth = 'email_deliver:Both'
	
	WebUI.delay(1)
	
		if (info.contains(emaideliverCustomer)){
			email_deliver = 'Customer Email Address'
		}else if (info.contains(emaildeliverPholder)) {
			email_deliver = 'Policy Holder Email Address'
		}else if (info.contains(emaildeliverBoth)){
			email_deliver = 'Both'
	}
		
		WebUI.delay(2)
		
		WebUI.scrollToElement(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'), 3, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.delay(2)
		
		if  (email_deliver == 'Customer Email Address' ) {
			
				GlobalVariable.Email = 'Customer Email Address'
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
			
			WebUI.delay (1)
			
		}
			
		else if (email_deliver == 'Policy Holder Email Address') {
			
			GlobalVariable.Email = 'Policy Holder Email Address'
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
			
			WebUI.delay (1)
			
		}
		
		else if (email_deliver == 'Both') {
			
				GlobalVariable.Email = 'Both'
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/EXP_EMAIL_DELIVER_TO'))
			
			WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Policy Info Page 3/LST_EMAIL_DELIVER_TO',[('emaildeliver'):email_deliver]))
			
			WebUI.delay (3)
		}
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/BTN_NEXT'))
	}
}
else {
	 
	 WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 3/BTN_NEXT'))
 }		
*/

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 2/TXT_HEADER'), 3)

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/EXP_BILLING_ADDRESS'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/LST_BILLING_CUST_HOME'))

WebUI.delay(1)
/*
if (segment == 'CASHDEALER') {
	
	'CHECK IF VANUMBER EXISTS'
	def rowcountVA = findTestData("Validation/VirtualAccountNumber").getRowNumbers()
	
	WebUI.delay(3)
	
	if (rowcountVA == 1) {
		
		def vanumber = findTestData("Validation/VirtualAccountNumber").getValue(1, 1)
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_SEARCH_VA_NUMBER'))
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/Find VANumber/EXP_SEARCH_BY'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/Find VANumber/LST_SEARCH_BY_VANUMBER'))
		
		WebUI.delay(1)
		
		WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/Find VANumber/TXT_SEARCH_BY'), vanumber)
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/Find VANumber/BTN_SEARCH'))
		
		WebUI.delay(5)
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/Find VANumber/LST_SEARCH_RESULT_1'))
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/Find VANumber/BTN_SELECT'))
		
		WebUI.delay(3)
	}
	
}
*/

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_CHOOSE_IMAGE'))

WebUI.delay(8)

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(20)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_NEXT'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_CONFIRM_YES'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Policy Info Page 4/BTN_CONFIRM_OK'))

WebUI.delay(8)

