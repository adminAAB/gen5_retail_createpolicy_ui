import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'executeQuery.GetIP.IPLocal'()

WebUI.delay(2)

def datacacat = ''

if (GlobalVariable.vmip == '172.16.94.191') {
	datacacat = 'CacatSemula3'
}else if (GlobalVariable.vmip == '172.16.94.192') {
	datacacat = 'CacatSemula4'
}else if (GlobalVariable.vmip == '172.16.94.193') {
	datacacat = 'CacatSemula5'
}else if (GlobalVariable.vmip == '172.16.94.194') {
	datacacat = 'CacatSemula6'
}else if (GlobalVariable.vmip == '172.16.92.22') {
	datacacat = 'CacatSemula'
}else if (GlobalVariable.vmip == '10.1.2.128') {
	datacacat = 'CacatSemula'
}else {
	datacacat = 'CacatSemula'
}

def countoridefect = findTestData(datacacat).getRowNumbers() as Integer
def x = 1 as Integer

WebUI.delay(1)

while (x <= countoridefect){
def cs = (findTestData(datacacat).getValue(3, x)).toUpperCase() as String
	if (cs != '') {
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/BTN_ADD'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/EXP_NAME'))
			WebUI.delay(1)
			WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/TXT_SEARCH_NAME'), findTestData(datacacat).getValue(3, x))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/LST_BUMPER_DEPAN'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/EXP_DAMAGE_CATEGORY'))
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/LST_DAMAGE_PENYOK'))
			WebUI.delay(1)
			WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/TXT_DESCRIPTION'), 'deskripsi')
			WebUI.delay(1)
			WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/BTN_SAVE'))
			WebUI.delay(1)
	}
x = x + 1
}

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/Original Defect/BTN_CLOSE'))

WebUI.delay(1)

