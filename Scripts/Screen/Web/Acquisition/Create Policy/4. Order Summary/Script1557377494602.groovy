import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/EXP_ORDER_SUMMARY'))

/*WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Object Repository/Web/Acquisition/Create Policy/Order Summary/LBL_ITEM_SCROLL',[('item'):'Email Deliver To'],10))
*/
WebUI.delay(20)

CustomKeywords.'executeQuery.Validation.checkOrderSummary'()

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT_YES'))

WebUI.delay(15)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Order Summary/BTN_SUBMIT_OK'))

WebUI.delay(10)

CustomKeywords.'executeQuery.Validation.getOrderNo'()

WebUI.delay(5)

