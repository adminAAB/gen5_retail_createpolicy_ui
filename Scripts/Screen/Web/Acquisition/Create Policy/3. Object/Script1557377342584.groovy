import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser.func_scope as func_scope
import org.eclipse.persistence.internal.sessions.DirectCollectionChangeRecord.NULL as NULL
import org.junit.After as After
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.google.common.base.Equivalence.Equals as Equals
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.TestObjectXpath as TestObjectXpath
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'executeQuery.GetIP.IPLocal'()

def usage = findTestData(GlobalVariable.vmparam).getValue(11, 1)

def vyear = findTestData(GlobalVariable.vmparam).getValue(9, 1)

def region = ((findTestData(GlobalVariable.vmparam).getValue(12, 1)) as String)

def chasisnumbercek = ((findTestData(GlobalVariable.vmparam).getValue(10, 1)) as String)

def chasisnumber = ((findTestData(GlobalVariable.vmparam).getValue(1, 1)) as String)

def chasisno = ''

def nsbte = 'NSBTE'

def cbte = ((findTestData(GlobalVariable.vmparam).getValue(58,1)) as String)

def cancel = 'CANCEL'

WebUI.maximizeWindow()

WebUI.delay(1)

if (chasisnumbercek.contains(nsbte)) {
    chasisno = chasisnumbercek
}
else {
    chasisno = chasisnumber
}

WebUI.delay(1)

	def updateparam = ("UPDATE globalvariable_bte set globalvariable_1 = '"+chasisnumber+"'")
	WebUI.delay(1)
	CustomKeywords.'executeQuery.DemoMySql.execute'(updateparam)
	WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Web/Acquisition/Create Policy/Object Page 1/TXT_HEADER'), 3)

WebUI.delay(8)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/BTN_ADD_OBJECT'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/BTN_FIND_VEHICLE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/Find Vehicle/TXT_SEARCH_BY_INPUT'), 
    findTestData(GlobalVariable.vmparam).getValue(8, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/Find Vehicle/BTN_SEARCH'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/Find Vehicle/LST_SEARCH_RESULT_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/Find Vehicle/BTN_SELECT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/EXP_YEAR'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/LST_YEAR', [('year') : vyear]), 
    FailureHandling.OPTIONAL)

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_ENGINE_NO'), chasisno)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_CHASSIS_NO'), chasisno)

GlobalVariable.chassis = chasisno
println ('CIMPAN CHASSISNO'+GlobalVariable.chassis)

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/TXT_COLOR_STNK'), 'black')

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 1/BTN_NEXT'))

WebUI.delay(10)

WebUI.scrollToElement(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/TXT_HEADER'), 3)

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/EXP_USAGE'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/LST_USAGE', [('usage') : usage.toUpperCase()]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/EXP_REGION'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/TXT_REGION_SEARCH'), region)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/LST_REGION_RESULT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/BTN_CHOOSE_IMAGE'))

WebUI.delay(10)

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(10)

'NO COVER'
def datanocover = ''

if (GlobalVariable.vmip == '172.16.94.191') {
    datanocover = 'NoCover3'
} else if (GlobalVariable.vmip == '172.16.94.192') {
    datanocover = 'NoCover4'
} else if (GlobalVariable.vmip == '172.16.94.193') {
    datanocover = 'NoCover5'
} else if (GlobalVariable.vmip == '172.16.94.194') {
    datanocover = 'NoCover6'
} else if (GlobalVariable.vmip == '172.16.92.22') {
    datanocover = 'NoCover'
} else {
    datanocover = 'NoCover'
}

def nocovercount = findTestData(datanocover).getRowNumbers()

if (nocovercount > 0) {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/BTN_NO_COVER'))

    WebUI.delay(1)

    WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/3.1. No Cover'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(2)

'ORIGINAL DEFECT'
def datacacat = ''

if (GlobalVariable.vmip == '172.16.94.191') {
    datacacat = 'CacatSemula3'
} else if (GlobalVariable.vmip == '172.16.94.192') {
    datacacat = 'CacatSemula4'
} else if (GlobalVariable.vmip == '172.16.94.193') {
    datacacat = 'CacatSemula5'
} else if (GlobalVariable.vmip == '172.16.94.194') {
    datacacat = 'CacatSemula6'
} else if (GlobalVariable.vmip == '172.16.92.22') {
    datacacat = 'CacatSemula'
} else {
    datacacat = 'CacatSemula'
}

def oridefectcount = findTestData(datacacat).getRowNumbers()

if (oridefectcount > 0) {
    WebUI.delay(1)

    def oridefectdesc = findTestData(datacacat).getValue(3, 1)

    WebUI.delay(1)

    if (oridefectdesc == '') {
        WebUI.delay(1)
    } else {
        WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/BTN_ORIGINAL_DEFECT'))

        WebUI.delay(1)

        WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/3.1. Original Defect'), [:], FailureHandling.STOP_ON_FAILURE)
    }
}

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 2/BTN_NEXT'))

WebUI.delay(5)

'Kendaraan'
WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/LST_KENDARAAN'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_EDIT'))

WebUI.delay(5)

'KENDARAAN'
CustomKeywords.'ifCondition.Kendaraan.InterestCoverage'()

WebUI.delay(5)

'ACCESSORIES'
CustomKeywords.'ifCondition.Accessories.InterestAccessories'()

WebUI.delay(5)

'PA DRIVER'
CustomKeywords.'ifCondition.PADriver.InterestPAD'()

WebUI.delay(5)

'PA PASSENGER'
CustomKeywords.'ifCondition.PAPassenger.InterestPAP'()

WebUI.delay(5)

'TPL'
CustomKeywords.'ifCondition.TPL.InterestTPL'()

WebUI.delay(5)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Object Detail Page 3/BTN_SAVE_YES'))

WebUI.delay(10)

'CLAUSE'
def dataclause = ''

if (GlobalVariable.vmip == '172.16.94.191') {
    dataclause = 'Clause3'
} else if (GlobalVariable.vmip == '172.16.94.192') {
    dataclause = 'Clause4'
} else if (GlobalVariable.vmip == '172.16.94.193') {
    dataclause = 'Clause5'
} else if (GlobalVariable.vmip == '172.16.94.194') {
    dataclause = 'Clause6'
} else if (GlobalVariable.vmip == '172.16.92.22') {
    dataclause = 'Clause'
} else {
    dataclause = 'Clause'
}

def clausecount = findTestData(dataclause).getRowNumbers()

if (clausecount > 0) {
    def clausedesc = findTestData(dataclause).getValue(3, 1)

    if (clausedesc == '') {
        WebUI.delay(1)
    } else {
        WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/BTN_CLAUSE'))

        WebUI.delay(1)

        WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Create Policy/3.1. Clause'), [:], FailureHandling.STOP_ON_FAILURE)
    }
}

WebUI.delay(10)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/BTN_NEXT'))

WebUI.delay(10)

