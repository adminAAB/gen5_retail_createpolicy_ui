import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def idtype = ((findTestData('CustomerPersonal').getValue(4, 1)) as String)

def religion = ((findTestData('CustomerPersonal').getValue(8, 1)) as String)

def gender = ((findTestData('CustomerPersonal').getValue(7, 1)) as String)

def marital = ((findTestData('CustomerPersonal').getValue(9, 1)) as String)

def homenumber = ((findTestData('CustomerPersonal').getValue(12, 1)) as String)

def mobilenumber2 = ((findTestData('CustomerPersonal').getValue(14, 1)) as String)

def mobilenumber3 = ((findTestData('CustomerPersonal').getValue(15, 1)) as String)

def mobilenumber4 = ((findTestData('CustomerPersonal').getValue(16, 1)) as String)

def email = ((findTestData('CustomerPersonal').getValue(17, 1)) as String)

def facebook = ((findTestData('CustomerPersonal').getValue(18, 1)) as String)

def twitter = ((findTestData('CustomerPersonal').getValue(19, 1)) as String)

def instagram = ((findTestData('CustomerPersonal').getValue(20, 1)) as String)

def jobtitle = ((findTestData('CustomerPersonal').getValue(21, 1)) as String)

def companyname = ((findTestData('CustomerPersonal').getValue(22, 1)) as String)

def ext = ((findTestData('CustomerPersonal').getValue(26, 1)) as String)

def income = ((findTestData('CustomerPersonal').getValue(27, 1)) as Integer)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/TXT_CUSTOMER_NAME'), findTestData('CustomerPersonal').getValue(
        3, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/EXP_ID_TYPE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/LST_ID_TYPE', [('idtype') : idtype]))

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/TXT_ID_NUMBER'), findTestData('CustomerPersonal').getValue(
        5, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_BIRTHDATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_CAL_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_CAL_2'))

///// SET BIRTHDAY
String BirthDate = findTestData('CustomerPersonal').getValue(6, 1)

String cmonthBD

String cdayBD

def dayBD = BirthDate.split('/')[0]

def monthBD = BirthDate.split('/')[1]

def yearBD = ((BirthDate.split('/')[2]) as Integer)

WebUI.delay(1)

//convert month
if (monthBD == '01') {
    cmonthBD = 'Jan'
} else if (monthBD == '02') {
    cmonthBD = 'Feb'
} else if (monthBD == '03') {
    cmonthBD = 'Mar'
} else if (monthBD == '04') {
    cmonthBD = 'Apr'
} else if (monthBD == '05') {
    cmonthBD = 'May'
} else if (monthBD == '06') {
    cmonthBD = 'Jun'
} else if (monthBD == '07') {
    cmonthBD = 'Jul'
} else if (monthBD == '08') {
    cmonthBD = 'Aug'
} else if (monthBD == '09') {
    cmonthBD = 'Sep'
} else if (monthBD == '10') {
    cmonthBD = 'Oct'
} else if (monthBD == '11') {
    cmonthBD = 'Nov'
} else if (monthBD == '12') {
    cmonthBD = 'Dec'
} else {
    println('Month not found')
}

WebUI.delay(1)

//convert day
if (dayBD == '01') {
    cdayBD = '1'
} else if (dayBD == '02') {
    cdayBD = '2'
} else if (dayBD == '03') {
    cdayBD = '3'
} else if (dayBD == '04') {
    cdayBD = '4'
} else if (dayBD == '05') {
    cdayBD = '5'
} else if (dayBD == '06') {
    cdayBD = '6'
} else if (dayBD == '07') {
    cdayBD = '7'
} else if (dayBD == '08') {
    cdayBD = '8'
} else if (dayBD == '09') {
    cdayBD = '9'
} else {
    cdayBD = dayBD
}

while (WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_YEAR', [('yearBD') : yearBD]), 
    2, FailureHandling.OPTIONAL) == false) {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_CAL_BACK'))
}

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_YEAR', [('yearBD') : yearBD]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_MONTH', [('monthBD') : cmonthBD]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_DAY', [('dayBD') : cdayBD]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/EXP_GENDER'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/LST_GENDER', [('gender') : gender]))

WebUI.delay(1)

if (religion != '') {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/EXP_RELIGION'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/LST_RELIGION', [('religion') : religion]))
}

WebUI.delay(1)

if (marital != '') {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/EXP_MARITAL'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/LST_MARITAL', [('marital') : marital]))
}

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_CHOOSE_IMAGE'))

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(15)

//CustomKeywords.'runEXE.UploadFilePath.Upload'()

//WebUI.delay(20)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 1/Personal/BTN_NEXT'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_ADDRESS'), findTestData('CustomerPersonal').getValue(
        10, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/BTN_POSTAL_CODE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/Postal Code/TXT_SEARCH_INPUT'), findTestData('CustomerPersonal').getValue(
        11, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/Postal Code/BTN_SEARCH'))

WebUI.delay(12)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/Postal Code/LST_POSTAL_CODE_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/Postal Code/BTN_SELECT'))

WebUI.delay(1)

if (homenumber != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_HOME_NUMBER'), homenumber)

    WebUI.delay(1)
}

WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_MOBILE_NUMBER_1'), findTestData('CustomerPersonal').getValue(
        13, 1))

WebUI.delay(1)

/////CEK MOBILE NUMBER
if (mobilenumber2 != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_MOBILE_NUMBER_2'), mobilenumber2)
}

if (mobilenumber3 != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_MOBILE_NUMBER_3'), mobilenumber3)
}

if (mobilenumber4 != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_MOBILE_NUMBER_4'), mobilenumber4)
}

WebUI.delay(1)

if (email != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/TXT_EMAIL'), email)
}

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 2/Personal/BTN_NEXT'))

WebUI.delay(3)

/////CEK SOCIAL MEDIA
if (facebook != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_FACEBOOK'), facebook)
}

if (twitter != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_TWITTER'), twitter)
}

if (instagram != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_INSTAGRAM'), instagram)
}

WebUI.delay(1)

/////CEK JOB TITLE
if (jobtitle != '') {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/EXP_JOB_TITLE'))

    WebUI.delay(1)

    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_JOB_SEARCH'), jobtitle)

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/LST_JOB_SEARCH_RESULT'))
}

WebUI.delay(1)

/////CEK COMPANY
if (companyname != '') {
    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_COMPANY_NAME'), findTestData('CustomerPersonal').getValue(
            22, 1))

    WebUI.delay(1)

    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_OFFICE_ADDRESS'), findTestData('CustomerPersonal').getValue(
            23, 1))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_OFFICE_POSTAL_CODE'))

    WebUI.delay(1)

    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/Office Postal Code/TXT_SEARCH_INPUT'), findTestData(
            'CustomerPersonal').getValue(24, 1))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/Office Postal Code/BTN_SEARCH'))

    WebUI.delay(12)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/Office Postal Code/LST_POSTAL_CODE_1'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/Office Postal Code/BTN_SELECT'))

    WebUI.delay(1)

    WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_OFFICE_NUMBER'), findTestData('CustomerPersonal').getValue(
            25, 1))
	
	WebUI.delay(1)
	
    /////EXTENSION
    if (ext != '') {
        WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/TXT_EXTENSION'), ext)
    }
    
    WebUI.delay(3)
}

/////GAJI
if (income != '') {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/EXP_INCOME'))

    if (income < 5000000) {
        WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/LST_INCOME_UNDER5mio'))
    } else if ((income >= 5000000) && (income <= 15000000)) {
        WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/LST_INCOME_5to15mio'))
    } else {
        WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/LST_INCOME_ABOVE15mio'))
    }
}

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_NEXT'))

WebUI.delay(10)

/////CHECK BIRTHDATE EXIST (PERSONAL)
if (WebUI.verifyElementPresent(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_CREATE_YES'), 2, FailureHandling.OPTIONAL) == 
false) {
    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_CREATE'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_CREATE_YES'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_CREATE_OK'))
} else {
    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_CREATE_YES'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Web/Acquisition/Create Policy/Customer Info Page 3/Personal/BTN_CREATE_OK'))
}

WebUI.delay(1)

