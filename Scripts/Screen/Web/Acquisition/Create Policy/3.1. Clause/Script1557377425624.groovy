import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//omvk1, omvF1, omv30, omv99, OMVS1 -- OBJECT CLAUSE

CustomKeywords.'executeQuery.GetIP.IPLocal'()

def dataclause = ''

if (GlobalVariable.vmip == '172.16.94.191') {
	dataclause = 'Clause3'
}else if (GlobalVariable.vmip == '172.16.94.192') {
	dataclause = 'Clause4'
}else if (GlobalVariable.vmip == '172.16.94.193') {
	dataclause = 'Clause5'
}else if (GlobalVariable.vmip == '172.16.94.194') {
	dataclause = 'Clause6'
}else if (GlobalVariable.vmip == '172.16.92.22') {
	dataclause = 'Clause'
}else if (GlobalVariable.vmip == '10.1.2.128') {
	dataclause = 'Clause'
}else {
	dataclause = 'Clause'
}

WebUI.delay(3)

def countclause = findTestData(dataclause).getRowNumbers()
def x = 1 as Integer

WebUI.delay(1)

while (x <= countclause){
def c = (findTestData(dataclause).getValue(3, x)).toUpperCase() as String
	if (c != '') {
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_ADD_OBJECT_CLAUSE'))
		WebUI.delay(1)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/EXP_OBJECT_CLAUSE'))
		WebUI.delay(1)
		WebUI.setText(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/TXT_SEARCH_OBJECT_CLAUSE'), c)
		WebUI.delay(1)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/LST_SEARCH_RESULT_1'))
		WebUI.delay(1)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/EXP_OBJECT'))
		WebUI.delay(1)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/LST_OBJECT_1'))
		WebUI.delay(1)
		WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_SAVE_OBJECT_CLAUSE'))
		WebUI.delay(1)
	}
x = x + 1
}

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Create Policy/Object Page 1/Clause/BTN_CLOSE'))

WebUI.delay(1)