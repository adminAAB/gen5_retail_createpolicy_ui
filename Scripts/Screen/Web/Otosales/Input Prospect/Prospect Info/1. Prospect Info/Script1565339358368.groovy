import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.lang.reflect.Array as Array
import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser.func_scope as func_scope
import org.junit.After as After
import com.google.common.base.Equivalence.Equals as Equals
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.TestObjectXpath as TestObjectXpath
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import io.cucumber.tagexpressions.TagExpressionParser.True as True
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import groovy.time.TimeCategory as TimeCategory

def prospectname = findTestData('Otosales/OtosalesParam').getValue(3, 1)

def prospectphone1 = findTestData('Otosales/OtosalesParam').getValue(4, 1)

def prospectphone2 = findTestData('Otosales/OtosalesParam').getValue(5, 1)

def prospectemail = findTestData('Otosales/OtosalesParam').getValue(6, 1)

def dealer = findTestData('Otosales/OtosalesParam').getValue(7, 1)

def salesman = findTestData('Otosales/OtosalesParam').getValue(8, 1)

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Otosales/Input Prospect/Prospect Info/TXT_PROSPECT_NAME'), prospectname)

WebUI.delay(2)

WebUI.setText(findTestObject('Web/Otosales/Input Prospect/Prospect Info/TXT_PROSPECT_NO_1'), prospectphone1)

WebUI.setText(findTestObject('Web/Otosales/Input Prospect/Prospect Info/TXT_PROSPECT_NO_2'), prospectphone2)

WebUI.setText(findTestObject('Web/Otosales/Input Prospect/Prospect Info/TXT_PROSPECT_EMAIL'), prospectemail)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/TXT_DEALER_NAME'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/TXT_GENERAL_SEARCH'), dealer)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/LST_GENERAL_SEARCH_RESULT'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/TXT_SALESMAN_DEALER_NAME'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/TXT_GENERAL_SEARCH'), salesman)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/LST_GENERAL_SEARCH_RESULT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Otosales/Input Prospect/Prospect Info/BTN_UPLOAD_KTP'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_CHOOSE'))

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_UPLOAD_STNK'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_CHOOSE'))

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_UPLOAD_SPPAKB'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_CHOOSE'))

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_UPLOAD_BSTB'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_CHOOSE'))

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(20)

/*
WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_UPLOAD_BUKTI_BAYAR'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_CHOOSE'))

CustomKeywords.'runEXE.UploadFile.Upload'()

WebUI.delay(8)
*/
WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_NEXT'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Prospect Info/BTN_NEXT_YES'))

WebUI.delay(8)

