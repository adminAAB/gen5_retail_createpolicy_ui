import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def vehicle = findTestData("Otosales/OtosalesParam").getValue(10, 1)
def usage = findTestData("Otosales/OtosalesParam").getValue(11, 1)
def region = findTestData("Otosales/OtosalesParam").getValue(12, 1)
def regno = findTestData("Otosales/OtosalesParam").getValue(13, 1)
def chassisno = findTestData("Otosales/OtosalesParam").getValue(14, 1)


WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_VEHICLE_DETAIL'))

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_GENERAL_SEARCH'), vehicle)

WebUI.delay(2)

WebUI.click(findTestObject('Web/Otosales/Input Prospect/Vehicle Info/LST_VEHICLE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_USAGE'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_GENERAL_SEARCH'), usage)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/LST_USAGE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_REGION'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_GENERAL_SEARCH'), region)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/LST_REGION'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_REGISTRATION_NO'), regno)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_CHASSIS_NO'), chassisno)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/TXT_ENGINE_NO'), chassisno)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Vehicle Info/BTN_NEXT'))

WebUI.delay(5)
































//