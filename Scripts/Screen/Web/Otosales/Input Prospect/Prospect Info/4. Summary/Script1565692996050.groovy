import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def prospectname = findTestData('Otosales/OtosalesParam').getValue(3, 1)

def prospectphone = findTestData('Otosales/OtosalesParam').getValue(4, 1)

def email = findTestData('Otosales/OtosalesParam').getValue(6, 1)

def vehicle = findTestData("Otosales/OtosalesParam").getValue(10, 1)

def basiccover = findTestData("Otosales/OtosalesParam").getValue(17, 1)

def suminsured = findTestData('Otosales/OtosalesParam').getValue(18, 1)

def accsi = '10,000,000'

def totalpremi = findTestData('Otosales/OtosalesParam').getValue(36, 1) as String

def salesman = findTestData('Otosales/OtosalesParam').getValue(8, 1)

def adminfee = '50,000'

def nettpremi = findTestData("Otosales/OtosalesParam").getValue(92, 1).replace(',00','').replace('.',',')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_PROSPECT_NAME'), prospectname)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_PHONE_NO_1'), prospectphone)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_EMAIL'), email)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_VEHICLE'), vehicle)

WebUI.delay(1)

//WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_COVERAGE'), basiccover)

WebUI.delay(1)
/*
def query = "SELECT REPLACE((CONVERT(VARCHAR(30),(CAST((CAST(REPLACE('"+totalpremi+"',',','') AS INT) + CAST(REPLACE('"+adminfee+"',',','') AS INT))AS MONEY)),1)),'.00','') AS NETTPREMI"

WebUI.delay(1)

CustomKeywords.'executeQuery.DemoMySql.connectDB'('172.16.94.48', 'LITT', 'sa', 'Password95')

WebUI.delay(1)

def nettpremi = CustomKeywords.'executeQuery.DemoMySql.executeQuery'(query)

println(nettpremi)
*/
WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_TOTAL_PREMI'), nettpremi)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_TSI_CASCO'), suminsured)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_TSI_ACC'), accsi)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LBL_SALESMAN_NAME'), salesman)

WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/LNK_QUOTATION_DETAILS'))

WebUI.delay(1)

//WebUI.callTestCase(findTestCase('Screen/Web/Otosales/Input Prospect/Prospect Info/4.1. Quotation Detail'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

