import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def producttype = findTestData("Otosales/OtosalesParam").getValue(15, 1)
def productcode = findTestData("Otosales/OtosalesParam").getValue(16, 1)
def basiccover = findTestData("Otosales/OtosalesParam").getValue(17, 1)
def suminsured = findTestData("Otosales/OtosalesParam").getValue(18, 1)
def srccpremi = findTestData("Otosales/OtosalesParam").getValue(29, 1)
def fldpremi = findTestData("Otosales/OtosalesParam").getValue(30, 1)
def etvpremi = findTestData("Otosales/OtosalesParam").getValue(31, 1)
def tspremi = findTestData("Otosales/OtosalesParam").getValue(27, 1)
def accpremi = findTestData("Otosales/OtosalesParam").getValue(32, 1)
def padpremi = findTestData("Otosales/OtosalesParam").getValue(33, 1)
def pappremi = findTestData("Otosales/OtosalesParam").getValue(34, 1)
def tplpremi = findTestData("Otosales/OtosalesParam").getValue(35, 1)
def totalpremi = findTestData("Otosales/OtosalesParam").getValue(36, 1)
def tplsi = '10,000,000'
def padsi = '10,000,000'
def papsi = '10,000,000'
def accsi = '10000000'


WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_PRODUCT_TYPE'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_GENERAL_SEARCH'), producttype)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LST_PRODUCT_TYPE'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_PRODUCT_CODE'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_GENERAL_SEARCH'), productcode)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LST_PRODUCT_CODE'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_BASIC_COVER'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_GENERAL_SEARCH'), basiccover)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LST_BASIC_COVER'))

WebUI.delay(13)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_SUM_INSURED'), 'value', suminsured, 3)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/CHK_TPL - ischeckTPL',[('ischeckTPL'):'0']))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_TPL_SI'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_GENERAL_SEARCH'), tplsi)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LST_TPL - tplsi',[('tplsi'):tplsi]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/BTN_GENERAL_OK'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/CHK_TS - ischeckTS',[('ischeckTS'):'0']))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/BTN_GENERAL_OK'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/CHK_PAD - ischeckPAD',[('ischeckPAD'):'0']))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_PAD_SI'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_GENERAL_SEARCH'), padsi)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LST_PAD_SI - padsi',[('padsi'):padsi]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/BTN_GENERAL_OK'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/CHK_PAP - ischeckPAP',[('ischeckPAP'):'0']))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_PAP_SI'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_GENERAL_SEARCH'), papsi)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LST_PAP',[('papsi'):papsi]))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/BTN_GENERAL_OK'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/CHK_ACC'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/TXT_ACC_SI'), accsi)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/BTN_GENERAL_OK'))

WebUI.delay(30)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_TPL_PREMI'),tplpremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_SRCC_PREMI'), srccpremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_FLD_PREMI'), fldpremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_ETV_PREMI'), etvpremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_TS_PREMI'), tspremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_PAD_PREMI'), padpremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_PAP_PREMI'), pappremi)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/LBL_ACC_PREMI'), accpremi)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Input Prospect/Coverage/BTN_NEXT'))

WebUI.delay(10)

















//