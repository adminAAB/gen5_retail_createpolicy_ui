import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testdata.DBData

def prospectname = findTestData("Otosales/OtosalesParam").getValue(3, 1)
def prospectphone1 = findTestData("Otosales/OtosalesParam").getValue(4,1)
def prospectemail = findTestData("Otosales/OtosalesParam").getValue(6,1)
def perihal = findTestData("Otosales/OtosalesParam").getValue(21,1)
def jenispertanggungan = findTestData("Otosales/OtosalesParam").getValue(22,1)
def kendaraan = findTestData("Otosales/OtosalesParam").getValue(23,1)
def platno = findTestData("Otosales/OtosalesParam").getValue(24,1)
def tahun = findTestData("Otosales/OtosalesParam").getValue(25,1)
def totalpremi = findTestData("Otosales/OtosalesParam").getValue(36,1)
def usage = findTestData("Otosales/OtosalesParam").getValue(11,1)
def vcode = findTestData("Otosales/OtosalesParam").getValue(9,1)

GlobalVariable.nosurat = WebUI.getText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_NO_SURAT'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_PROSPECT_NAME'), prospectname)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_PHONE_NO_1'), prospectphone1)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_EMAIL'), prospectemail)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_PERIHAL'), perihal)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_JENIS_PERTANGGUNGAN'), jenispertanggungan)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_KENDARAAN'), kendaraan)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_PLAT_NO'), platno)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_TAHUN'), tahun)

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_USAGE'), usage)

WebUI.delay(1)

//WebUI.verifyElementText(findTestObject('Object Repository/Web/Otosales/Input Prospect/Summary/Quotation Details/LBL_TOTAL_PREMI'), totalpremi)

WebUI.delay(1)

//perhitungan
'KENDARAAN'
CustomKeywords.'executeQuery.Validation.checkKendaraan'()

WebUI.delay(1)

'ACCESSORIES'
CustomKeywords.'executeQuery.Validation.checkAccessories'()

WebUI.delay(1)



WebUI.delay(1)





























//