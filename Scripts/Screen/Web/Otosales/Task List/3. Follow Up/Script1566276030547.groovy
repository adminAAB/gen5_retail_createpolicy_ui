import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement

WebUI.delay(1)

WebUI.click(findTestObject('Web/Otosales/Task List/Follow Up/EXP_FOLLOW_UP_REASON'))

WebUI.delay(3)

WebUI.setText(findTestObject('Web/Otosales/Task List/Follow Up/TXT_FOLLOW_UP_REASON'), 'send to sa')

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Web/Otosales/Task List/Follow Up/TXT_FOLLOW_UP_REASON'), Keys.chord(Keys.ARROW_DOWN,Keys.ENTER))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Task List/Follow Up/TXT_RECEIVER'), 'RECEIVER')

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Otosales/Task List/Follow Up/TXT_REMARKS'), 'REMARKS FOLLOW UP')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Task List/Follow Up/BTN_SAVE'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Otosales/Task List/Follow Up/BTN_OK'))

WebUI.delay(80)






















//