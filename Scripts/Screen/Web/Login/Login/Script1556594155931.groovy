import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def option
def userid
def password

WebUI.openBrowser('https://gen5-qc.asuransiastra.com/retail')

WebUI.delay(1)

WebUI.setText(findTestObject('Web/Login/TXT_USERID'), userid)

WebUI.setText(findTestObject('Web/Login/TXT_PASSWORD'), password)

WebUI.click(findTestObject('Object Repository/Web/Login/EXP_LOGIN_TYPE'))

WebUI.click(findTestObject('Object Repository/Web/Login/LST_LOGIN_TYPE',[('option') : option]))

WebUI.click(findTestObject('Web/Login/BTN_SUBMIT'))
