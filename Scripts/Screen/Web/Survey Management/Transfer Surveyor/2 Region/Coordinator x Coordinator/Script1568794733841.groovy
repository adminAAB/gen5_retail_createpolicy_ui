import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.time.TimeCategory as TimeCategory

WebUI.delay(1)

CustomKeywords.'kTransferSurveyor.convertdate.convertday'()

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 1']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 2']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 2']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Coordinator BTE 1 -> Coordinator BTE 2
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAJ']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 2']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(5)

///// Coordinator 1 BTE -> Coordinator 2 BTE
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AEK']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(5)

///// SAVE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

///// CHECK DB
CustomKeywords.'transferSurveyor.checktransfer.CoorxCoor2R'()

WebUI.delay(3)

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/LST_EFFECTIVE_DATE', [('effdate') : GlobalVariable.tomorrowconvert]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))

WebUI.delay(5)
