import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.time.TimeCategory as TimeCategory

WebUI.delay(1)

CustomKeywords.'kTransferSurveyor.convertdate.convertday'()

WebUI.delay(3)

///// REGION BTE COORDINATOR 1

///// BLOCK 1
'BLOCK 1'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 1']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Surveyor AAM -> COORDINATOR 1 BTE 1
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAM']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator AAJ -> Regional BTE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AAJ']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Regional WHY -> Inactive
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'WHY']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_REGION', 
        [('unaregion') : 'BTE 1']), 3)

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

CustomKeywords.'transferSurveyor.checktransfer.block1bte1coor'()

WebUI.delay(3)

///// Delete 
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/LST_EFFECTIVE_DATE', [('effdate') : GlobalVariable.tomorrowconvert]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))

WebUI.delay(5)

///// BLOCK 2
'BLOCK 2'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 1']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Region WHY -> COORDINATOR 1 BTE 1
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_REGIONAL_NAME', [('regname') : 'WHY']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator AAJ -> Surveyor BTE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AAJ']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Surveyor']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Surveyor AAM -> Regional BTE
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAM']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

CustomKeywords.'transferSurveyor.checktransfer.block2bte1coor'()

WebUI.delay(3)

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))
	
WebUI.delay(1)
	
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))
	
WebUI.delay(5)
	
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))
	
WebUI.delay(5)
/*
///// BLOCK 3
'BLOCK 3'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 1']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Surveyor AAM -> Regional Coordinator & Coordinator
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAM']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator & Coordinator']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

////// Regional WHY -> Surveyor
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_REGIONAL_NAME', [('regname') : 'WHY']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Surveyor']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator AAJ -> Inactive
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAJ']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

CustomKeywords.'transferSurveyor.checktransfer.block3bte1coor'()

WebUI.delay(3)
	
///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))
		
WebUI.delay(1)
		
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))
		
WebUI.delay(5)
		
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))
		
WebUI.delay(5)

///// BLOCK 4
'BLOCK 4'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 1']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Regional WHY -> Regional Coordinator & Coordinator
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_REGIONAL_NAME', [('regname') : 'WHY']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator & Coordinator']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Surveyor AAM -> Inactive
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAM']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator AAJ -> Inactive
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AAJ']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

	///// CHECK DB

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))
		
WebUI.delay(1)
		
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))
		
WebUI.delay(5)
		
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))
		
WebUI.delay(5)
/*
///// BLOCK 5
'BLOCK 5'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 1']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 1']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Coordinator AAJ -> Regional Coordinator & Coordinator
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AAJ']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator & Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 1']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 1']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Regional WHY -> Inactive
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'WHY']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

	///// CHECK DB

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))
		
WebUI.delay(1)
		
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))
		
WebUI.delay(5)
		
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))
		
WebUI.delay(5)
*/







