import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.time.TimeCategory as TimeCategory

WebUI.delay(1)

CustomKeywords.'kTransferSurveyor.convertdate.convertday'()

WebUI.delay(3)

///// REGION BTE 1 2 COORDINATOR 1 2

///// BLOCK 1
'BLOCK 1'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 2']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 2']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Regional BTE WHY -> Regional BTE 2
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_REGIONAL_NAME', [('regname') : 'WHY']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Regional 2 UA AED -> Coordinator 1 BTE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AED']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator UA AFK -> Surveyor BTE 2
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AFK']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Surveyor']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 2']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Surveyor BTE 2 AEK -> Regional BTE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AEK']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

///// CHECK DB
CustomKeywords.'transferSurveyor.checktransfer.block1bte12'()

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/LST_EFFECTIVE_DATE', [('effdate') : tomorrowconvert]))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))

WebUI.delay(5)

///// BLOCK 2
'BLOCK 2'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 2']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 2']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Coordinator BTE AFK -> Coordinator BTE 2
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AFK']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 2']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Surveyor BTE 2 AEK -> Coordinator BTE
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AEK']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator BTE 2 UA AEF -> Regional BTE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AEF']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Surveyor BTE AGU -> Surveyor BTE 2
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AGU']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Surveyor']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 2']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Regional BTE UA WHY -> Inactive
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'WHY']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

///// CHECK DB
CustomKeywords.'transferSurveyor.checktransfer.block2bte12'()

///// DELETE
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))

WebUI.delay(5)

///// BLOCK 3
'BLOCK 3'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 2']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 2']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Surveyor BTE AGU -> Regional Coordinator & Coordinator BTE 2
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AGU']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator & Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 2']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Regional BTE 2 UA AED -> Inactive
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AED']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator BTE 2 UA AEF -> Inactive
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AEF']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

///// CHECK DB

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))

WebUI.delay(5)

///// BLOCK 4
'BLOCK 4'
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_ADD'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE']))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_REGIONAL', [('regional') : 'BTE 2']))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/EXP_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_COORDINATOR', [('coordinator') : 'COORDINATOR 1 BTE 2']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SEARCH'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_EFFECTIVE_DATE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_DAY', [('day') : GlobalVariable.ctomorrowday]))

///// Coordinator BTE AFK -> Regional Coordinator & Coordinator BTE 2
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/ORI_COORDINATOR_NAME', [('coorname') : 'AFK']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Regional Coordinator & Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE 2']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE 2']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Regional BTE 2 UA AED -> Inactive
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AED']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Inactive']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

///// Coordinator BTE 2 UA AEF -> Coordinator BTE
WebUI.scrollToElement(findTestObject('Web/Survey Management/Transfer Surveyor/Add/TXT_HEADER'), 3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/LST_UNASSIGNED_NAME', [('unassign') : 'AEF']))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_TRANSFER'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_TITLE'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_TITLE', [('title') : 'Coordinator']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_REGIONAL'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_REGIONAL', [('destreg') : 'BTE']))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/EXP_DEST_COORDINATOR'))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/LST_DEST_COORDINATOR', [('destcoor') : 'COORDINATOR 1 BTE']))

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/Transfer/BTN_EXECUTE'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_SAVE_YES'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/Add/BTN_OK'))

WebUI.delay(5)

///// CHECK DB

///// Delete
WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE'))

WebUI.delay(1)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_YES'))

WebUI.delay(5)

WebUI.click(findTestObject('Web/Survey Management/Transfer Surveyor/BTN_DELETE_OK'))

WebUI.delay(5)













