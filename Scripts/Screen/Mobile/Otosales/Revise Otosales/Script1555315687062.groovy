import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.delay(2)

Mobile.startApplication('D:\\Apk\\OtoSales.6.2.6.QC.apk', false)

Mobile.delay(20)

Mobile.setText(findTestObject('Object Repository/Mobile/Otosales/Login/TXT_USERNAME'), 'yui', 3)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Object Repository/Mobile/Otosales/Login/TXT_PASSWORD'), 'P@ssw0rd', 3)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Login/BTN_LOGIN'), 3)

Mobile.delay(30)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Side Menu/LNK_ORDER_APPROVAL'), 3)

Mobile.delay(8)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Additional Order/LBL_APPROVAL_LIST_1'), 3)

Mobile.delay(60)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Additional Order/BTN_REVISE'), 3)

Mobile.delay(2)

Mobile.setText(findTestObject('Object Repository/Mobile/Otosales/Additional Order/TXT_REMARKS'), 'Remarks', 3)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Additional Order/BTN_SEND'), 3)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Additional Order/BTN_CONFIRM_REVISE'), 3)

Mobile.delay(80)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Home/BTN_SIDE_MENU'), 3)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Side Menu/LNK_LOGOUT'), 3)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosales/Side Menu/BTN_LOGOUT_YES'), 3)

Mobile.delay(3)

Mobile.closeApplication()

