import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def chassisno = findTestData("ChassisNeedSurvey/MasterID_NSBTE").getValue(1, 1)

def brand = 'AUDI'

def year = '2016'

def model = 'A 4'

def series = 'ALL NEW 2.0 TFSI'

Mobile.delay(1)

Mobile.setText(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/TXT_NAME'), 'NSBTE', 3)

Mobile.delay(1)

Mobile.hideKeyboard()

Mobile.delay(1)

Mobile.setText(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/TXT_PHONE_NUMBER'), '081298986868', 3)

Mobile.delay(1)

Mobile.hideKeyboard()

Mobile.delay(1)

Mobile.setText(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/TXT_SURVEY_LOCATION'), 'SURVEY LOCATION', 3)

Mobile.delay(1)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/TXT_CHASSIS_NO'), chassisno, 3)

Mobile.delay(1)

Mobile.hideKeyboard()

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/EXP_BRAND'), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/LST_BRAND',[('brand'):brand]), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/EXP_YEAR'), 3)

Mobile.delay(1)

//SWIPE YEAR
device_Heighta = Mobile.getDeviceHeight()

device_Widtha = Mobile.getDeviceWidth()

int startXa = device_Widtha * 0.75

int startYa = device_Heighta * 0.40

int endXa = device_Widtha * 0.75

int endYa = device_Heighta * 0.20

for (i = 0 ; i < 5 ; i++) {
	Mobile.delay(1)
	
	Mobile.swipe(startXa, startYa, endXa, endYa)
	
	Mobile.delay(1)
}

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/LST_YEAR',[('year'):year]), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/EXP_MODEL'), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/LST_MODEL',[('model'):model]), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/EXP_SERIES'), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Object Info/LST_SERIES',[('series'):series]), 3)

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/TAB_POLICY_INFO'), 3)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/TAB_COLLECT_PHOTO'), 3)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_ADD_VEHICLE_1'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_CAPTURE_PHOTO'), 3)

Mobile.delay(5)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_USE_PHOTO'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_ADD_CHASSIS_1'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_CAPTURE_PHOTO'), 3)

Mobile.delay(5)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_USE_PHOTO'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_ADD_DOC_1'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_CAPTURE_PHOTO'), 3)

Mobile.delay(5)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_USE_PHOTO'), 3)

Mobile.delay(3)

for (i = 0 ; i < 3 ; i++) {
	Mobile.delay(1)
	
	Mobile.swipe(startXa, startYa, endXa, endYa)
	
	Mobile.delay(1)
}

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_ADD_CHECKLIST_SURVEY_1'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_CAPTURE_PHOTO'), 3)

Mobile.delay(5)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_USE_PHOTO'), 3)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Acquisition Survey/Collect Photo/BTN_SAVE'), 3)

Mobile.delay(15)

CustomKeywords.'executeQuery.UpdateDataFiles.increaseNeedSurveyID'()

Mobile.delay(3)