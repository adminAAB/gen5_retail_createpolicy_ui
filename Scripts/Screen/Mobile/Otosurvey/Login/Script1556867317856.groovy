import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('E:\\Otosurvey.apk', false)

Mobile.delay(3)

Mobile.setText(findTestObject('Object Repository/Mobile/Otosurvey/Login/TXT_USERNAME'), 'ing', 3)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Object Repository/Mobile/Otosurvey/Login/TXT_PASSWORD'), 'R4hasia', 3)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/Mobile/Otosurvey/Login/BTN_LOGIN'), 3)

Mobile.delay(15)

