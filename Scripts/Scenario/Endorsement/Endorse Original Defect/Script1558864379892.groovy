import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login DOC Konvensional'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Home/Select Endorsement Policy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/1.B. Select Policy No For Endorsement 0 Need Survey'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/2. Endorsement Detail'), [('endorsetype') : 'Customer Request'
        , ('detailinfo') : 'Original Defect'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/3.0. Upload Endorse File'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Policy Info Page 1/BTN_NEXT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Policy Info Page 2/BTN_NEXT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Policy Info Page 3/BTN_NEXT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Policy Info Page 4/BTN_NEXT'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Policy Info Page 4/BTN_SAVE_ORDER_YES'))

WebUI.delay(8)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Policy Info Page 4/BTN_SAVE_ORDER_OK'))

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/4. Object Info'), [('objectno') : '1'], FailureHandling.STOP_ON_FAILURE)

'Object Detail Page 1'
WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 1/BTN_NEXT'))

WebUI.delay(10)

'Object Detail Page 2'
WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Endorsement Policy/4.B. Original Defect'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 2/BTN_NEXT'))

WebUI.delay(10)

'Object Detail Page 3'
WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 3/BTN_SAVE'))

WebUI.delay(2)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/Object Detail Page 3/BTN_SAVE_YES'))

WebUI.delay(5)

'Object Info Page 1'
WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Object Info/BTN_NEXT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Endorsement Summary/BTN_SUBMIT'))

WebUI.delay(3)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Endorsement Summary/BTN_SUBMIT_YES'))

WebUI.delay(15)

WebUI.click(findTestObject('Web/Acquisition/Endorsement Policy/Endorsement Summary/BTN_SUBMIT_OK'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Screen/Web/Login/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.closeBrowser()

