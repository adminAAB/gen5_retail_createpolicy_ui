import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login YUL Konvensional'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Home/Select Virtual Account Number'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Validation/Virtual Account Number/1. Generate New Virtual Account Number'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

def insertdatapolicy = 'insert into litt.dbo.dataaab2000 (Tipe,Customer,ProductCode,PolicyPeriodFrom,PolicyPeriodTo,source,VehicleCode,Year,ChasisNo,Usage,Wilayah,COMPeriodFrom,COMPeriodTo,TLOPeriodFrom,TLOPeriodTo,ETVPeriodFrom,ETVPeriodTo,FLDPeriodFrom,FLDPeriodTo,PADPeriodFrom,PADPeriodTo,PADSI,PAPPeriodFrom,PAPPeriodTo,PAPSI,PAPRate,TPLPeriodFrom,TPLPeriodTo,TPLSI,isRun,SRCCPeriodFrom,SRCCPeriodTo,SRCCSI,TSPeriodFrom,TSPeriodTo,TSSI,Marketing,Term,TLOTSPeriodFrom,TLOTSPeriodTo,Acc1,Acc1Price,Acc1Qty,Acc1SI,Acc2,Acc2Price,Acc2Qty,Acc2SI,ACCTSI,Region,AREA,CustomerType,Segment,PolicyPeriod,SurveyType) values (\'COM\',\'163484533\',\'GDN52\',\'01/12/2018\',\'01/05/2019\',\'P1C100\',\'V04959\',\'2016\',\'BTE\',\'Pribadi\',\'Jakarta\',\'01/12/2018\',\'01/05/2019\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'0\',\'\',\'\',\'\',\'\',\'\',\'\',\'YUI\',\'Full Payment\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'EXISTING PERSONAL\',\'CASHDEALER\',\'6M\',\'SKIP SURVEY\')'

WebUI.callTestCase(findTestCase('Scenario/New Policy/Config/Existing Personal Customer/CASHDEALER/6M/Skip Survey/Update Parameter'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

new executeQuery.DemoMySql().execute(insertdatapolicy)

WebUI.callTestCase(findTestCase('Scenario/New Policy/Flow/Comprehensive/COM'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Validation/Virtual Account Number/1.1. Check VANumber X Order'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

