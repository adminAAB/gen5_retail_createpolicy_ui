import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Screen/Web/Login/Login DOC Konvensional'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(100)

WebUI.callTestCase(findTestCase('Screen/Web/Home/Select Tasklist'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(180)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Task List/Task List SA'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Task List/Customer Info/Personal - Next'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Task List/Policy Info/Policy Info - Next'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Acquisition/Task List/Object Info/Check Detail Object'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Web/Login/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

